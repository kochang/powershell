﻿
function Add-PowershellAsContextMenuItem
{
    if(-not (Test-Path HKCR:))
    {    
        New-PSDrive -PSProvider registry -Root HKEY_CLASSES_ROOT -Name HKCR
    }

    $powershell = "powershell"    
    $runas = "runas"
    $command = "command"
    $value = "$PSHOME\powershell.exe -NoExit -Command ""Set-Location '%V'"""
    $adminValue = "$PSHOME\powershell.exe -NoExit -NoProfile -Command ""Set-Location '%V'"""
    $contextName = "Open &Powershell Here"
    $contextAdminName = "Open Powershell Here As Administrator"

    "HKCR:\Directory\shell", "HKCR:\Directory\background\shell", "HKCR:\Drive\shell" `
    | foreach {

        if(Test-Path $_)
        {        
            $powershellPath= Join-Path $_ $powershell
            if(-not (Test-Path $powershellPath))
            {            
                New-Item -Path $_ -Name $powershell | Out-Null
                Set-Item -Path $powershellPath -Value $contextName
        
                New-Item -Path $powershellPath -Name $command | Out-Null
                Set-Item -Path (Join-Path $powershellPath $command) -Value $value
            }

            
            $powershellPath = Join-Path $_ $runas
            if(-not (Test-Path $powershellPath))
            {
                New-Item -Path $_ -Name $runas | Out-Null
                Set-Item -Path $powershellPath -Value $contextAdminName

                New-Item -Path $powershellPath -Name $command | Out-Null    
                $commandPath = (Join-Path $powershellPath $command)         
                Set-Item -Path $commandPath -Value $value
                Set-ItemProperty -Path $commandPath -Name "HasLUAShield" -Value '' | Out-Null
            }
        }
    }    
}



function Remove-PowershellAsContextMenuItem
{
    [CmdletBinding(SupportsShouldProcess=$true)]
    param()
    "HKCR:\Directory\shell", "HKCR:\Directory\background\shell", "HKCR:\Drive\shell" `
    | foreach {        
        $powershellPath = Join-Path $_ "powershell"
        if(Test-Path $powershellPath)
        {
            rm $powershellPath -Recurse -Confirm
        }

        
        $powershellPath = Join-Path $_ "runas"
        if(Test-Path $powershellPath)
        {
            rm $powershellPath -Recurse -Confirm
        }
    }
}


