﻿function Confirm-Commits
{
    [CmdletBinding()]
    param(
    [Parameter(Mandatory=$true, ValueFromPipeline=$true)]
    [string[]] $Path
    )
    BEGIN
    {
        $processPath = @()
    }

    PROCESS
    {        
        $where = $Path | Where-Object { $_ -match "^ServerRoles"}
        if($where)
        {
            $processPath += Resolve-Path $where
        }
    }

    END
    {
        $processed = @{
            "Tests" = @();
            "Source" = @();
        }

        $seen = @{}
        $last = @()
        foreach ($p in $processPath)
        {
            if($p -notlike "*.ps1")
            {
                continue
            }

            if($seen.ContainsKey($p.Path))
            {
                continue
            }        
            $seen[$p.Path] = $null

            if($p -like "*.Tests.ps1")
            {
                $processed["Tests"] += $p.Path
                $location = Resolve-Path "$p/../../"
                $sourceName = (Split-Path -Leaf $p) -replace '\.Tests\.', '.'
                $source = (Join-Path $location $sourceName)
            
                if(Test-Path $source)
                {
                    $processed["Source"] += $source
                    $seen[$source] = $null
                    Write-Host $source
                    Write-Host $p
                }
            }
            else
            {
                $location = Join-Path (Split-Path $p -Parent) "Tests"
                $testName = (Split-Path $p -Leaf) -replace '\.', '.Tests.'
                $test = (Join-Path $location $testName)
                $processed["Source"] +=$p.Path

                if(Test-Path $test)
                {            
                     $processed["Tests"] += $test
                     $seen[$test] = $null
                     Write-Host $p
                     Write-Host $test
                }
            
            }
        }

        $r = Invoke-Pester $processed["Tests"] -CodeCoverage $processed["Source"] -PassThru 
        $failed = $r.FailedCount
        $analyzed = $r.CodeCoverage.NumberOfCommandsAnalyzed
        $executed = $r.CodeCoverage.NumberOfCommandsExecuted
   

        if($analyzed -gt 0)
        {
            $colour = "Yellow"
            Write-Host -Foreground $colour "==============================" 
        
            $codeCoverage = ($executed / $analyzed)

            if($failed)
            {
                Write-Warning "`a$failed test(s) failed"
            }
            else 
            {
                Write-Host "All $($r.TotalCount) test(s) passed"  -Foreground $colour
            }

            if($codeCoverage -ne 1)
            {
                Write-Warning "`aCode coverage is $($codeCoverage.ToString(`"0.00%`"))"
                $r.CodeCoverage.MissedCommands
            }
            else 
            {
                Write-Host "Hooray to 100% code coverage!"  -Foreground $colour
            }
        
            Write-Host -Foreground $colour "==============================" 
        }
    }
}

#& ./Set-ModulePath.ps1
#Import-Module Test-Utilities -Force
#git diff origin/dev --name-only | Confirm-Commits
