﻿$here = Split-Path -Parent $MyInvocation.MyCommand.Path
$sut = (Split-Path -Leaf $MyInvocation.MyCommand.Path).Replace(".Tests.", ".")
. "$here\$sut"



function NotPesterBeKnownHashObjectFailureMessage
{    
    param(
    $Value,
    $ExpectedValue
    )
    
    return PesterBeKnownHashObject @PSBoundParameters -Message -Not
}

function PesterBeKnownHashObjectFailureMessage
{    
    param(
    $Value,
    $ExpectedValue
    )
    
    return PesterBeKnownHashObject @PSBoundParameters -Message
}
function PesterBeKnownHashObject
{
    param(
    $Value,
    $ExpectedValue,
    [switch]$Message,
    [switch]$Not
    )

    if($Not)
    {
        if($Message)
        {
            return "Hash is the same, but it shouldn't be"
        }
        return $false
    }


    if($ExpectedValue.Count -ne $Value.Count)
    {
        if($Message)
        {
            return "Expected: {$($ExpectedValue.Count)} items, but it was {$($Value.Count)}"
        }
        return $false
    }
    foreach($e in $ExpectedValue.Keys)
    {
        if(-not $Value.ContainsKey($e))
        {
            if($Message)
            {
                return "Expected: {$e} as a key but it does not exist"
            }
            return $false
        }

        if($Value[$e] -ne $ExpectedValue[$e])
        {
            if($Message)
            {
                return "Expected: value {$($ExpectedValue[$e])} for key {$e} but was {$($Value[$e])}"
            }
            return $false
        }
    }


    return $true

}



Describe $sut {
    It "asserts many values" {
        
        $result = GetAHashObject 
        $result["A"] | should be 1
        $result["B"] | should be 2
        $result["C"] | should be 3
        $result["D"] | should be 4
    }

    It "asserts with custom should"{
        GetAHashObject | Should BeKnownHashObject @{
            "A" = 1;
            "B" = 2;
            "C" = 3;
            "D" = 4;
        }
    }

    
    It "should fail at unmatched number of items"{
        GetAHashObject | Should BeKnownHashObject @{
            "A" = 1;
            "B" = 2;
            "C" = 3;
        }
    }

    It "should fail at missing key"{
        GetAHashObject | Should BeKnownHashObject @{
            "A" = 1;
            "B" = 2;
            "C" = 3;
            "E" = 4;
        }
    }

    
    It "should fail at mismatching value"{
        GetAHashObject | Should BeKnownHashObject @{
            "A" = 1;
            "B" = 2;
            "C" = 3;
            "D" = 99;
        }
    }


     
    It "should fail 'not'"{
        GetAHashObject | Should not BeKnownHashObject @{
            "A" = 1;
            "B" = 2;
            "C" = 3;
            "D" = 4;
        }
    }    
}

