﻿function Get-EnvironmentVariable
{
    param(
        [string]$Name
    )
    return (Get-Item Env:$Name).Value;
}

Export-ModuleMember -Function Get-EnvironmentVariable