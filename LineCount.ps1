Write-Host "LineCount completed in $((Measure-Command {
   Set-Variable -Name CSharp -Value (Get-ChildItem -include *.cs, *.aspx, *.asmx, *.ashx, *.ascx -exclude *generated* -recurse | select-string "^([\s\t]*)$" -notmatch).Count
   Set-Variable -Name Flex -Value (Get-ChildItem -include *.as, *.mxml -exclude *generated*.* -recurse | select-string "^([\s\t]*)$" -notmatch).Count 
   Set-Variable -Name header -Value (@("CSharp", "Flex", "Combined") -join ',')
   Set-Variable -Name value -Value (@($CSharp, $Flex, ($CSharp + $Flex)) -join ',') 

   [System.IO.File]::WriteAllLines((Join-Path (Get-Location) "LineCount.csv"), "$header`r`n$value", (New-Object System.Text.UTF8Encoding($false)))
}).TotalSeconds)"
