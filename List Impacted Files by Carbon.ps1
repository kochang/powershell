﻿$CarbonFunctions = @(
                            'Add-GroupMember',
                            'Add-IisDefaultDocument',
                            'Add-TrustedHost',
                            'Assert-AdminPrivilege',
                            'Assert-FirewallConfigurable',
                            'Assert-Service',
                            'Clear-DscLocalResourceCache',
                            'Clear-MofAuthoringMetadata',
                            'Clear-TrustedHost',
                            'Complete-Job',
                            'Compress-Item',
                            'ConvertFrom-Base64',
                            'Convert-SecureStringToString',
                            'ConvertTo-Base64',
                            'ConvertTo-ContainerInheritanceFlags',
                            'ConvertTo-InheritanceFlag',
                            'ConvertTo-PropagationFlag',
                            'ConvertTo-SecurityIdentifier',
                            'Convert-XmlFile',
                            'Copy-DscResource',
                            'Disable-FirewallStatefulFtp',
                            'Disable-IEEnhancedSecurityConfiguration',
                            'Disable-IisSecurityAuthentication',
                            'Disable-NtfsCompression',
                            'Enable-FirewallStatefulFtp',
                            'Enable-IEActivationPermission',
                            'Enable-IisDirectoryBrowsing',
                            'Enable-IisSecurityAuthentication',
                            'Enable-IisSsl',
                            'Enable-NtfsCompression',
                            'Expand-Item',
                            'Find-ADUser',
                            'Format-ADSearchFilterValue',
                            'Get-ADDomainController',
                            'Get-Certificate',
                            'Get-CertificateStore',
                            'Get-ComPermission',
                            'Get-ComSecurityDescriptor',
                            'Get-DscError',
                            'Get-DscWinEvent',
                            'Get-FileShare',
                            'Get-FileSharePermission',
                            'Get-FirewallRule',
                            'Get-Group',
                            'Get-HttpUrlAcl',
                            'Get-IisApplication',
                            'Get-IisAppPool',
                            'Get-IisConfigurationSection',
                            'Get-IisHttpHeader',
                            'Get-IisHttpRedirect',
                            'Get-IisMimeMap',
                            'Get-IisSecurityAuthentication',
                            'Get-IisVersion',
                            'Get-IisWebsite',
                            'Get-IPAddress',
                            'Get-Msi',
                            'Get-MsmqMessageQueue',
                            'Get-MsmqMessageQueuePath',
                            'Get-PathProvider',
                            'Get-PathToHostsFile',
                            'Get-PerformanceCounter',
                            'Get-Permission',
                            'Get-PowerShellModuleInstallPath',
                            'Get-PowershellPath',
                            'Get-Privilege',
                            'Get-ProgramInstallInfo',
                            'Get-RegistryKeyValue',
                            'Get-ScheduledTask',
                            'Get-ServiceAcl',
                            'Get-ServiceConfiguration',
                            'Get-ServicePermission',
                            'Get-ServiceSecurityDescriptor',
                            'Get-SslCertificateBinding',
                            'Get-TrustedHost',
                            'Get-User',
                            'Get-WindowsFeature',
                            'Get-WmiLocalUserAccount',
                            'Grant-ComPermission',
                            'Grant-HttpUrlPermission',
                            'Grant-MsmqMessageQueuePermission',
                            'Grant-Permission',
                            'Grant-Privilege',
                            'Grant-ServiceControlPermission',
                            'Grant-ServicePermission',
                            'Initialize-Lcm',
                            'Install-Certificate',
                            'Install-Directory',
                            'Install-FileShare',
                            'Install-Group',
                            'Install-IisApplication',
                            'Install-IisAppPool',
                            'Install-IisVirtualDirectory',
                            'Install-IisWebsite',
                            'Install-Junction',
                            'Install-Msi',
                            'Install-Msmq',
                            'Install-MsmqMessageQueue',
                            'Install-PerformanceCounter',
                            'Install-RegistryKey',
                            'Install-ScheduledTask',
                            'Install-Service',
                            'Install-User',
                            'Install-WindowsFeature',
                            'Invoke-AppCmd',
                            'Invoke-PowerShell',
                            'Join-IisVirtualPath',
                            'Lock-IisConfigurationSection',
                            'New-Credential',
                            'New-Junction',
                            'New-RsaKeyPair',
                            'New-TempDirectory',
                            'Protect-Acl',
                            'Protect-String',
                            'Read-File',
                            'Remove-DotNetAppSetting',
                            'Remove-EnvironmentVariable',
                            'Remove-GroupMember',
                            'Remove-HostsEntry',
                            'Remove-IisMimeMap',
                            'Remove-IniEntry',
                            'Remove-Junction',
                            'Remove-RegistryKeyValue',
                            'Remove-SslCertificateBinding',
                            'Reset-HostsFile',
                            'Reset-MsmqQueueManagerID',
                            'Resolve-FullPath',
                            'Resolve-Identity',
                            'Resolve-IdentityName',
                            'Resolve-NetPath',
                            'Resolve-PathCase',
                            'Resolve-RelativePath',
                            'Restart-RemoteService',
                            'Revoke-ComPermission',
                            'Revoke-HttpUrlPermission',
                            'Revoke-Permission',
                            'Revoke-Privilege',
                            'Revoke-ServicePermission',
                            'Set-DotNetAppSetting',
                            'Set-DotNetConnectionString',
                            'Set-EnvironmentVariable',
                            'Set-HostsEntry',
                            'Set-IisHttpHeader',
                            'Set-IisHttpRedirect',
                            'Set-IisMimeMap',
                            'Set-IisWebsiteID',
                            'Set-IisWebsiteSslCertificate',
                            'Set-IisWindowsAuthentication',
                            'Set-IniEntry',
                            'Set-RegistryKeyValue',
                            'Set-ServiceAcl',
                            'Set-SslCertificateBinding',
                            'Set-TrustedHost',
                            'Split-Ini',
                            'Start-DscPullConfiguration',
                            'Test-AdminPrivilege',
                            'Test-DotNet',
                            'Test-DscTargetResource',
                            'Test-FileShare',
                            'Test-FirewallStatefulFtp',
                            'Test-Group',
                            'Test-GroupMember',
                            'Test-Identity',
                            'Test-IisAppPool',
                            'Test-IisConfigurationSection',
                            'Test-IisSecurityAuthentication',
                            'Test-IisWebsite',
                            'Test-IPAddress',
                            'Test-MsmqMessageQueue',
                            'Test-NtfsCompression',
                            'Test-OSIs32Bit',
                            'Test-OSIs64Bit',
                            'Test-PathIsJunction',
                            'Test-PerformanceCounter',
                            'Test-PerformanceCounterCategory',
                            'Test-Permission',
                            'Test-PowerShellIs32Bit',
                            'Test-PowerShellIs64Bit',
                            'Test-Privilege',
                            'Test-RegistryKeyValue',
                            'Test-ScheduledTask',
                            'Test-Service',
                            'Test-SslCertificateBinding',
                            'Test-TypeDataMember',
                            'Test-UncPath',
                            'Test-User',
                            'Test-WindowsFeature',
                            'Test-ZipFile',
                            'Uninstall-Certificate',
                            'Uninstall-Directory',
                            'Uninstall-FileShare',
                            'Uninstall-Group',
                            'Uninstall-IisAppPool',
                            'Uninstall-IisWebsite',
                            'Uninstall-Junction',
                            'Uninstall-MsmqMessageQueue',
                            'Uninstall-PerformanceCounterCategory',
                            'Uninstall-ScheduledTask',
                            'Uninstall-Service',
                            'Uninstall-User',
                            'Uninstall-WindowsFeature',
                            'Unlock-IisConfigurationSection',
                            'Unprotect-String',
                            'Write-DscError',
                            'Write-File'
                        )

$Path = "C:\serraview\ProdOps"

$files = ls $Path -Recurse -File -Include "*.ps1", "*.psm1", "*.psd1", "*.ps1xml" | Where-Object {
     $_.FullName -notmatch "^$([Regex]::Escape('C:\serraview\ProdOps\ThirdPartyModules\Pester'))" `
     -and $_.FullName -notmatch "^$([Regex]::Escape('C:\serraview\ProdOps\ServerRoles\Common\PowerShell\OctoPosh'))" `
     -and $_.FullName -notmatch "^$([Regex]::Escape('C:\serraview\ProdOps\ServerRoles\Common\PowerShell\Modules'))" `
     -and $_.FullName -notmatch "^$([Regex]::Escape('C:\serraview\ProdOps\Deploy\Scripts\Modules'))" `
     -and $_.FullName -notmatch '\.Tests\.ps1$'  
     }



$Global:matches = @{}
$Global:files = @{}
foreach($f in $CarbonFunctions)
{   
    $skip = @()
    switch($f)
    {
        "ConvertTo-Base64" 
        { 
           $skip += "C:\serraview\ProdOps\ServerRoles\Common\PowerShell\JIraAPI.ps1"
        }
        "Get-Certificate"
        {
           $skip += "C:\serraview\ProdOps\ServerRoles\Common\PowerShell\Encryption.ps1"
        }
        "Get-IisApplication"
        {
           $skip += "C:\serraview\ProdOps\ServerRoles\Common\PowerShell\Library\IIS.ps1"
        }
        "Get-IPAddress"
        {
           $skip += "C:\serraview\ProdOps\Deploy\Scripts\Library\Common\Network.ps1", "C:\serraview\ProdOps\Deploy\Scripts\Library\IPAuthentication\Sync-SerraviewIPAuthentication.ps1", "C:\serraview\ProdOps\ServerRoles\App\Deploy\Core\Apply-DnsRecords.ps1", "C:\serraview\ProdOps\ServerRoles\App\Deploy\Core\Apply-IISBindings.ps1", "C:\serraview\ProdOps\ServerRoles\App\Deploy\Core\Apply-IPAuthentication.ps1", "C:\serraview\ProdOps\ServerRoles\App\Library\PublishEnvironmentDocumentation.ps1", "C:\serraview\ProdOps\ServerRoles\Common\PowerShell\Library\Instance.ps1", "C:\serraview\ProdOps\ServerRoles\Common\PowerShell\Library\Network.ps1", "C:\serraview\ProdOps\ServerRoles\Common\PowerShell\Sync-SerraviewIPAuthentication.ps1"
        }
        "Get-ScheduledTask"
        {
           $skip += "C:\serraview\ProdOps\ServerRoles\App\Library\PublishEnvironmentDocumentation.ps1", "C:\serraview\ProdOps\ServerRoles\Common\PowerShell\ScheduledTask.ps1"
        }
        "Get-User"
        {
           $skip += "C:\serraview\ProdOps\ServerRoles\Common\PowerShell\Wayfinder\WayfinderAPI.ps1", "C:\serraview\ProdOps\ServerRoles\Common\PowerShell\Authentication.ps1", "C:\serraview\ProdOps\ServerRoles\Common\PowerShell\EventTrigger.ps1", "C:\serraview\ProdOps\ServerRoles\Common\PowerShell\ScheduledTask.ps1", "C:\serraview\ProdOps\ServerRoles\Common\PowerShell\WindowsService.ps1"
        }
        "New-RsaKeyPair"
        {
           $skip += "C:\serraview\ProdOps\ServerRoles\Common\PowerShell\Encryption.ps1"
        }
        "Set-EnvironmentVariable"
        {
           $skip += (ls "C:\serraview\ProdOps\Configuration\" -Recurse -File | Select-Object -ExpandProperty FullName)
           $skip += (ls "C:\serraview\ProdOps\Tests\LaunchUpdate.Tests" -Recurse -File | Select-Object -ExpandProperty FullName) 
           $skip += "C:\serraview\ProdOps\LaunchUpdate.ps1", "C:\serraview\ProdOps\Tests\Modules\Test-Utilities\Functions\WriteTests.ps1", "C:\serraview\ProdOps\Tests\NotATest-Update.ps1"
        }
        "Test-ScheduleTask"
        {
           $skip += "C:\serraview\ProdOps\ServerRoles\Common\PowerShell\ScheduledTask.ps1", "C:\serraview\ProdOps\ServerRoles\Data\IntegrationTests\Test-PushBillingReport.ps1", "C:\serraview\ProdOps\ServerRoles\Data_US\IntegrationTests\Test-JpmcTasksExist.ps1", "C:\serraview\ProdOps\ServerRoles\Sync\Install\010 Create framework synchronisation task.ps1", "C:\serraview\ProdOps\Tests\NotATest-ScheduledTask.ps1" 
        }
        "Test-User"
        {
           $skip += "C:\serraview\ProdOps\ServerRoles\App\IntegrationTests\Test-SyncIPAuthentication.ps1", "C:\serraview\ProdOps\ServerRoles\App\Script\IpWhitelist.ps1", "C:\serraview\ProdOps\ServerRoles\Common\PowerShell\Wayfinder\WayfinderAPI.ps1"
        }
        "Unprotect-String"
        {
           $skip += "C:\serraview\ProdOps\ServerRoles\Common\PowerShell\Encryption.ps1"
        }
        "Protect-String"
        {
           $skip += "C:\serraview\ProdOps\ServerRoles\Common\PowerShell\Encryption.ps1"
        }

    }
    $result = $files | Where { $skip -notcontains $_.FullName } | Select-String -Pattern $f    
    if($result)
    {
        $Global:matches[$f] = $result

        foreach($r in $result)
        {
           if(-not $Global:files.ContainsKey($r.Path))
           {
              $Global:files[$r.Path] = @()
           }
           $Global:files[$r.Path] += (New-Object psobject -Property @{ 
              Function = $f
              LineNumber = $r.LineNumber
              Line = $r.Line
           })
        }

        "=== $f ==="
        ($result | Select-Object -Property Path, LineNumber, Line | Out-String)
    }
}



