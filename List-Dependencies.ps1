﻿$files = ls -file -Recurse -Include "*.ps1", "*.psm1", "*.psd1", "*.ps1xml"

$Global:tree = @{}
$count = 0
foreach($f in $files)
{
    $count++
    Write-Progress -Activity "search files ... $count / $($files.Length)" -PercentComplete ($count/$files.Length * 100)

    $result = $files | Select-String $f.Name
    $f.FullName
    
    $Global:tree[$f.FullName] = @{}    

    if($result)
    {
    
        foreach($r in $result)
        {
            $r.Path
            $r.LineNumber
            $r.Line
            $Global:tree[$f.FullName][$r.Path] = New-Object psobject -Property @{
                Line = $r.Line
                LineNumber = $r.LineNumber
            }
        }
        "`r`n"
    }
}


$tree.GetEnumerator() | Sort-Object -property @{Expression = {"$($_.Value.Keys.Count)"}} -Descending |  % { $ref = $_.Key; "[REF] $($tree[$ref].Count): $ref"; $tree[$ref].Keys.ForEach({ $_; "`t$($tree[$ref][$_].LineNumber): {$($tree[$ref][$_].Line)}" }); "`r`n" } | Set-Clipboard