$ErrorActionPreference = "Stop"
$DebugPreference = "Continue" 
$ProdOpPath = "C:\serraview\\ProdOps"


# Functions

function Push-ModulePathIfExistAndNotAdded
{
   [CmdletBinding()]
   param(
   [Parameter(Mandatory=$true)]
   [string]$Path
   )

   END{
      if((Test-Path $Path) -and $ENV:PSModulePath -NotMatch [Regex]::Escape($Path))
      {
         $ENV:PSModulePath += ";" + $Path
         Write-Debug "Path '$($Path)' added to module path."
      }
   }
}


function Push-Profile
{
   param(
      [string]$ToLocation = "$(Get-OneDrivePath)\Code\Powershell",
      [string]$Rename  
   )

   if(Test-Path $ToLocation)
   {
      $p = Get-Item $profile
      if($Rename)
      {
         $bakFile = Join-Path $ToLocation $Rename
      }
      else
      { 
         $bakFile = Join-Path $ToLocation $p.Name
      }

      if(!(Test-Path $bakFile) -or $p.LastWriteTimeUtc -gt (Get-Item $bakFile).LastWriteTimeUtc)
      {
         Copy $profile $bakFile
         Write-Debug "Profile pushed to '$bakFile'"
      }
   }
}

function Pull-Profile
{
   param( 
      [string]$FromLocation = "$(Get-OneDrivePath)\Code\Powershell"
   )

   if(Test-Path $FromLocation)
   {
      $p = Get-Item $profile
      $bakFile = "$($FromLocation)\$($p.Name)"

      if(!(Test-Path $bakFile) -or $p.LastWriteTimeUtc -ge (Get-Item $bakFile).LastWriteTimeUtc)
      {
         return;
      }

      Copy $bakFile $Profile
      Write-Debug "Profile pulled from '$($FromLocation)'"
   }
}

function Sync-Profile
{
   param(
      [string]$BackupLocation = "$(Get-OneDrivePath)\Code\Powershell"
   )

   # Sync to one drive
   Push-Profile $BackupLocation
   Pull-Profile $BackupLocation 
   #
   # Sync to Dropbox
   $dropbox = "D:\Dropbox\WindowsPowershell"
   Push-Profile $dropbox
   Pull-Profile $dropbox

   # Sync to VSCode
   $ProfileFolder = Split-Path -Parent $Profile
   Push-Profile $ProfileFolder -Rename "Microsoft.VSCode_profile.ps1"
   
}

function Get-OneDrivePath
{
   return (Get-ItemProperty -Path "hkcu:\Software\Microsoft\Onedrive" -Name UserFolder).UserFolder
}



function Get-IsWorkStation
{
   if($env:USERDOMAIN -eq "Melbourne")
   {
      return $true
   }
   return $false
}



function Set-EnvironmentPath
{
   param(
   [string]$Path,
   [string]$Message
   )

   if((Test-Path $Path) -and $ENV:Path -NotMatch [Regex]::Escape($Path))
   {
      $ENV:Path += ";" + $Path
      if($Message -ne [String]::Empty)
      {
         Write-Debug "'$($Message)' included."
      }
      else
      {
         Write-Debug "'$($Path)' included."
      }      
   }
}


function New-PSW()
{
   ii "C:\Windows\System32\WindowsPowerShell\v1.0\powershell.exe"
}

function Push-IfExistAndNotAtLocation([string] $path)
{ 
   if((Test-Path $path) -eq $false)
   {
      return
   }

   if($MyInvocation.MyCommand.Path -eq $path)
   {
      return
   }
   Push-Location $path
}




function Push-Desktop()
{ 
   Push-IfExistAndNotAtLocation("$Home\Desktop")
}



function Enter-ProdOps()
{
   cd $ProdOpPath
}




function Find-ChildProcesses
{
  [CmdletBinding()]
  Param(
     [int]$ID = $PID,
     
     [Object[]]$Property = @('ProcessID'),

     [switch]$Recurse
     )

 

  $children = @(Get-WmiObject -Class Win32_Process -Filter "ParentProcessID=$($ID)" | Select-Object -Property $Property)
  
  if($Recurse)
  {
    $children += @($children | Where-Object {$_.ProcessID -ne $null } | ForEach-Object {Find-ChildProcesses -ID $_.ProcessID})
  }
  

  return $children
}



function Stop-ChildProcesses
{
   param(
   [int]$ID = $PID
   )

   process
   {
      Find-ChildProcesses -Recurse -ID $ID | ForEach-Object { Stop-Process $_.ProcessID }
   }
}




# Set Path Variables
Set-EnvironmentPath "$(${env:ProgramFiles(x86)})\Microsoft VS Code" -Message "VS Code"
Set-EnvironmentPath "D:\Vim\vim74" -Message "Vim"
Set-EnvironmentPath "$($env:USERPROFILE)\AppData\Local\Programs\Git\bin" -Message "Git"
Set-EnvironmentPath "$($env:ProgramFiles)\TortoiseGit\bin" -Message "TortoiseGit"
Set-EnvironmentPath "$(${env:ProgramFiles(x86)})\Vim\vim74" -Message "Vim"



# Update Prompt

function Prompt{
    Write-Host ""
    Write-Host "[$(Get-Date -Format 'yyyy-MM-dd HH:mm')]"  -ForegroundColor Gray -BackgroundColor Black
    Write-Host "$(Get-Location)"  -ForegroundColor Green -BackgroundColor Black
    return "It's an honor to serve you, Ko: ";    
}

# Increase Buffer Size
$b = $host.ui.RawUI.BufferSize
$b.Height = 9999
$host.ui.RawUI.BufferSize = $b
Remove-Variable b


# Alias
New-Alias psw  New-PSW
New-Alias dt   Push-Desktop
New-Alias epo  Enter-ProdOps
New-Alias tg   TortoiseGitProc.exe
New-Alias ip   Invoke-Pester

Write-Debug "I wonder if I'm going run ModulePath and..."
if(Get-IsWorkStation)
{
   # Go to ProdOps
   epo
   .\Set-ModulePath.ps1
Write-Debug "I did "
}


# Import Modules
$DebugPreference = "Silent" 
Import-Module Pester 
$DebugPreference = "Continue" 


Sync-Profile
$ErrorActionPreference = "Continue"
