New-Module 1871 -ScriptBlock {

   $script:HomeTestBuild = "$Home\Desktop\Builds"
   $script:HomeBackupBuild = "$Home\Desktop\BackupBuilds"
   $script:DestTestBuild = "\\SVDEV01\Builds"

   function Rename-Stuff
   {
      param
      (
         [string]$Before,
         [string]$After
      )

      $esBefore = [Regex]::Escape($Before)
      s -Pattern $esBefore -Path $ProdOpsPath | Out-Null
      $LastSearchFiles.ForEach({ 
            $fileName = $_
            Write-Verbose "Updating file {$fileName}"
            (Get-Content $fileName) -replace $esBefore, $After | Out-File $fileName 
         }) 
   }

   function Start-JIRAIssue 
   {
      start "https://serraview.atlassian.net/browse/PRODOPS-1871"
   }

   function Invoke-Tests 
   {
      <#
      Import-Module Deploy -Force
      Invoke-PesterFailedOnly @('Tests\LaunchUpdate.Tests.ps1', 'ServerRoles\Common\Modules\*\Library', 'ServerRoles\Common\PowerShell\Library\Tests', 'ServerRoles\App', 'ServerRoles\PrimaryWeb', 'ServerRoles\Dev\', 'ServerRoles\ImportOrchestration', 'ServerRoles\FileSync').ForEach({ Join-Path $ProdOpsPath $_ })
#>

      Invoke-ProdOpsTests -PassThru -SkipCodeCoverage | Set-Variable TestResult -Scope Global

   }


   function Find-Reference 
   {
      # given a file info, get me the ps files that reference it
      param
      (
         [Parameter(Mandatory=$True, ValueFromPipeline=$true)]
         [System.IO.FileInfo]$ReferencedFile
      )
      
      if(Test-Path $ReferencedFile)
      {
         s -Path $ProdOpsPath -Pattern ([Regex]::Escape("$($ReferencedFile.Name)"))
      }
   }

   function Write-HostToDo
   {
      Write-Host @"


PRODOPS-1871 Move framework to modules - Common 

change all calls to Invoke-EmailReport to fit the new calls.
update old call to Email-Report.ps1 to function Invoke-EmailReports

TODO turn back tests
TODO pop back deployment
TODO do not check in build.ps1

TODO fix Orchestration Common.ps1 as it imports common very often
TODO Merge inf, staging to dev and then merge dev, same applies to prod in DataManagement 
TODO When going to dev, don't forget to merge DataManagement repo as well
TODO Tweak Import-module Common performance, might have to check before calling import
TODO compile a list of renamed commands
TODO find/identify functions with the same names


"@ 
   }

   function Get-TestCoverageInCommon
   {
      Invoke-PesterFailedOnly (ls "$ProdOpsPath\ServerRoles\Common\Modules\*\Library\" -Recurse -Include *.Tests.ps1) -CodeCoverage (ls "$ProdOpsPath\ServerRoles\Common\Modules\*\Library\" -Recurse -Include *.ps1 -Exclude *.Tests.ps1)

      Write-Host "`aServer role common has {$PesterCoverage}% coverage."
   }

   function Push-TestBuild
   {
      if(Copy-Item "$script:HomeTestBuild\ProdOps - dev\*" "$script:DestTestBuild\ProdOps - dev\" -Force -PassThru)
      {
         Copy-Item "$script:HomeTestBuild\DataManagement - 1871\*" "$script:DestTestBuild\DataManagement - au_staging\" -Force 
         Move-Item "$script:DestTestBuild\DataManagement - au_staging\DataManagement - 1871.zip" "$script:DestTestBuild\DataManagement - au_staging\DataManagement - au_staging.zip" -Force
         ResetServerRevNums
      }
   }

   function Backup-BeforeTestBuild
   {
      Get-ChildItem $script:HomeBackupBuild | Remove-Item -Force -Recurse
      Copy-Item "$script:DestTestBuild\ProdOps*" "$script:HomeBackupBuild" -Force -PassThru -Recurse
      Copy-Item "$script:DestTestBuild\DataManagement*" "$script:HomeBackupBuild" -Force -PassThru -Recurse
   }

   function Pop-TestBuild
   {
      if(Copy-Item "$script:HomeBackupBuild\ProdOps - dev\*" "$script:DestTestBuild\ProdOps - dev\" -Force -PassThru)
      {
         Copy-Item "$script:HomeBackupBuild\DataManagement - au_staging\*" "$script:DestTestBuild\DataManagement - au_staging\" -Force 
         ResetServerRevNums
      }
   }

   function Build
   {
      Write-Verbose "Build framework"
      Get-ChildItem $script:HomeTestBuild | Remove-Item -Force -Recurse
      & "$ProdOpsPath\Build\Build.ps1" -Branch dev -DMBranch 1871 -SignPSScript -SkipUnitTests
      & "$ProdOpsPath\Build\Build.ps1" -Branch dev -DMBranch 1871 -SignPSScript -BuildDataManagementOnly -SkipUnitTests
   }

   function ResetServerRevNums
   {
      $servers = @(Get-ChildItem "$ProdOpsPath\Configuration\Server\CHQ*" | Select-Object -ExpandProperty Name) -replace '.ps1', ''
      foreach ($s in $servers)
      {
         $path = "\\$s\C$\ProdOps\revNum", "\\$s\C$\ProdOps\DataManagement\revNum"
         foreach($p in $path)
         {
            if(Test-Path $p)
            {
               Remove-Item $p
               Write-Verbose "Removed {$p}"
            }
            else
            {
               Write-Verbose "`aFailed to locate RevNum at {$p}"
            }
         }
      }
   }

   function ServerStatus
   {
      $servers = @(Get-ChildItem "$ProdOpsPath\Configuration\Server\CHQ*" | Select-Object -ExpandProperty Name) -replace '.ps1', ''
      foreach ($s in $servers)
      {
         $path = "\\$s\C$\ProdOps\revNum", "\\$s\C$\ProdOps\DataManagement\revNum"
         foreach($p in $path)
         {
            if(Test-Path $p)
            {
               Write-Host "$p revNum is {$(Get-Content $p)}"
            }
            else
            {
               Write-Verbose "`aFailed to locate RevNum at {$p}"
            }
         }
      }

      Invoke-Command -ComputerName $servers -ScriptBlock { Write-Host -ForegroundColor Magenta "== Computer Name: {$($env:COMPUTERNAME)} =="; $env:PSModulePath -split ';'}
   }


   Export-ModuleMember -Function * -Alias *
}
