
New-Module 2035 -ScriptBlock {


   function Start-JIRAIssue 
   {
      start "https://serraview.atlassian.net/browse/PRODOPS-2035"
   }


   function Write-HostToDo
   {
      Write-Host @"

PRODOPS-2035 Improve reliability of framework / data management deploys

LaunchUpdate.ps1
TODO Find all source file copy and trap errors

Sync-Files.ps1 
TODO Add command info when failed 

"@ 
   }


   Export-ModuleMember -Function * -Alias *
}
