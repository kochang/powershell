﻿$script:CIMonitorRoot = Split-Path -Parent $MyInvocation.MyCommand.Path 
@('GoogleAPI', 'TimerEvent', 'CKLAudio').ForEach({ 
   Import-Module (Resolve-Path "$script:CIMonitorRoot\..\$_\$_.psd1").ProviderPath -Force
})



#dot sourcing functions
"$script:CIMonitorRoot\Functions\*.ps1" | Resolve-Path | ForEach-Object { . $_.ProviderPath }

$Script:InstanceID = New-Guid

Import-CIMonitorConfiguration
Set-StorePath (Get-GoogleAPICredStorePath)
