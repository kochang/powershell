function Get-Holidays
{
   $obj = Get-ObjectFromJsonFile "PublicHolidays.json"
   $holidays = @{}
   foreach($d in $obj.holidays)
   {
      try
      {
         $date = [DateTime]::ParseExact($d.Date,"yyyy-MM-dd", [CultureInfo]::CurrentCulture)
         $holidays[$date] = $d.Description
      }
      catch
      {
         return @{}
      }
   }

   return $holidays
}

function Test-IsHoliday
{
   [CmdletBinding()]
   param(
      [Parameter(Mandatory=$true)]
      [DateTime]$Today
   )

   $holidays = Get-Holidays
   return ((([System.DayOfWeek]::Saturday, [System.DayOfWeek]::Sunday) -contains $Today.DayOfWeek) -or $holidays.ContainsKey($Today))
}

function Test-IsLastWorkDayThisWeek 
{
   [CmdletBinding()]
   param(
      [Parameter(Mandatory=$true)]
      [DateTime]$Today
   )
      
   $holidays = Get-Holidays
   if(Test-IsHoliday -Today $Today)
   {
      return $false
   }
   
   if($Today.DayOFWeek -eq [System.DayOfWeek]::Friday)
   {
      return $true
   }
   
   $dayOfWeek = @{
      [System.DayOfWeek]::Monday = 4
      [System.DayOfWeek]::Tuesday = 3
      [System.DayOfWeek]::Wednesday = 2
      [System.DayOfWeek]::Thursday = 1
   }

   $lastDay = $Today.AddDays($dayOfWeek[$Today.DayOfWeek])
   while($holidays.ContainsKey($lastDay))
   { 
      $lastDay = $lastDay.AddDays(-1) 
   }

   if($lastDay -eq $Today.Date)
   {
      return $true
   }
   return $false
}
