﻿function Get-DateTimeStringFormat
{
   return "yyyy-MM-dd hh:mm:ss ddd"
}

function Get-AssetFile
{
    [CmdletBinding()]
    param(
    [Parameter(Mandatory=$true)]
    [string]$RequestingFile
    )

    $configDir = Get-ConfigDir -Name "Asset"

    $path = Join-Path $configDir $RequestingFile
    if(-not (Test-Path $path))
    {
        $path = Join-Path (Get-ConfigDir -Default) $RequestingFile
        if(-not (Test-Path $path))
        {
            return $null
        }
    }
    
    return $path
}

function Get-GoogleAPICredStorePath
{
   return (Get-ConfigDir -Name "GoogleAPI")
}

function Get-MuteJobs
{  
   return Get-HashObjectFromFile -FileName "MuteJobs.txt" 
}

function Get-FocusJobs
{  

    return Get-ArrayFromFile -FileName "FocusJobs.txt" 
}

function Get-DeveloperNicknames
{

    return Get-HashObjectFromFile -FileName "DeveloperNickname.txt" 
}
function Get-ArrayFromFile
{
    [CmdletBinding()]
    param(
    [string]$FileName)

    $file = Get-AssetFile -RequestingFile $FileName
    if(-not $file)
    {
        return @()
    }
    $obj = Get-Content $file
    return $obj
}

function Get-HashObjectFromFile
{
    [CmdletBinding()]
    param(
    [string]$FileName)

    $file = Get-AssetFile -RequestingFile $FileName
    if(-not $file)
    {
        return @{}
    }
    $obj = Get-Content $file -Raw | ConvertFrom-StringData
    return $obj
}

function Get-ObjectFromJsonFile
{
    [CmdletBinding()]
    param(
        [string]$FileName
    )

    $file = Get-AssetFile -RequestingFile $FileName
    $obj = Get-Content $file -Raw | ConvertFrom-Json

    return $obj    
}

function Import-CIMonitorConfiguration
{    
    param (
        [string]$Path = (Join-Path $script:CIMonitorRoot "Path.txt")
    )
    $Script:ConfigPaths = Get-Content $Path -Raw | ConvertFrom-StringData    
}

function Get-CIMonitorID
{
    return $Script:InstanceID 
}

function Get-ConfigDir
{
   param(
      [string]$Name,
      [switch]$Default
   )
   if($Script:ConfigPaths.ContainsKey($Name) -and (-not $Default))
   {
      $path = $Script:ConfigPaths[$Name] 
   }
   else 
   {
      $path = Join-Path $Script:CIMonitorRoot "DefaultConfigurations"
      if($Name)
      {
         $path = Join-Path $path $Name
      }
   }

   if(-not (Test-Path $path))
   {
      $null = New-Item -Type Directory -Path $path 
   }
   return $path
}

function Get-LogDir
{
    return Get-ConfigDir "Log"
}

function Get-TimeStampLogPath
{
    return Join-Path (Get-LogDir) "TimeStamp.$(Get-CIMonitorID).log"
}

function Get-BuildAnnoncementAlertPath
{
    return Get-AssetFile -RequestingFile "BuildAnnoncementAlert.mp3"
}

function Get-AnnoncementAlertPath
{
    return Get-AssetFile -RequestingFile "AnnoncementAlert.mp3"
}

function Get-BeerTimeAlertPath
{
    return Get-AssetFile -RequestingFile "BeerTime.mp3"
}

function Get-BirthdaySongPath
{
    return Get-AssetFile -RequestingFile "happy.birthday.mp3"
}

function Get-LunchAlertPath
{
    return Get-AssetFile -RequestingFile "LunchAlert.mp3"
}

function Get-LunchQuotes
{
    return Get-ObjectFromJsonFile -FileName "LunchQuotes.json"
}


function Get-BuildAllSucceedAlertPath
{
    return Get-AssetFile -RequestingFile "StageClear.mp3"
}

function Get-BrokenBuildAlertPath
{
    return Get-AssetFile -RequestingFile "BrokenBuild.mp3"
}


function Get-LogPath
{
    $callingCmd = (Get-Variable MyInvocation -Scope 1).Value.MyCommand.Name
    $logDir = Get-LogDir
    
    return Join-Path $logDir "$callingCmd.log"
}

function Remove-Logs
{
    $logDir = Get-LogDir

    Remove-Item $logDir\* -Force
}

function Start-ReadAllLogs
{
    param(
    [string]$ComputerName = $env:COMPUTERNAME
    )

    $logDir = Get-LogDir
    if(Test-Path $logDir)
    {
        $logs = Get-ChildItem $logDir
        $logs.ForEach(
        {            
            Start-Process powershell -ArgumentList "-NoProfile -NoExit -Command  & { Get-Content '$($_.FullName)' -Wait | foreach { if(`$_ -match '^\['){ Write-Host `$_ } else { Write-Host `$_ -ForegroundColor Red} } } "
        })
    }
    else
    {
        throw "can't find log directory {$logDir}"
    }
}

function Set-TimeStamp
{
    param(
        [datetime]$Value
    )
    $timestampPath = (Get-TimeStampLogPath)

    $data = $Value.ToUniversalTime().ToString("yyyy-MM-dd.HH:mm:ss")
    Add-Log -Message $data -LogPath $timestampPath -Replace
}

function Get-TimeStamp
{
    $timestampPath = (Get-TimeStampLogPath)

    $lastRan = $null
    if(Test-Path $timestampPath)
    {
        $lastRan = [datetime]::ParseExact((Get-Content $timestampPath), "yyyy-MM-dd.HH:mm:ss", [cultureinfo]::CurrentCulture)
        if($lastRan)
        {
            return $lastRan.ToLocalTime()
        }
    }

    return $lastRan
}




Export-ModuleMember -Function Import-CIMonitorConfiguration
Export-ModuleMember -Function Get-ConfigurationDirectory
Export-ModuleMember -Function Start-ReadAllLogs
Export-ModuleMember -Function Get-LogDir
Export-ModuleMember -Function Get-TimeStamp
Export-ModuleMember -Function Get-LunchQuotes
