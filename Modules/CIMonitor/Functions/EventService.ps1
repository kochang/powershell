﻿function Invoke-EventAnnoncements
{
   [CmdletBinding()]
   param(
      [DateTime]$Now,
      $Past,
      [int]$ToleranceInMins = 3
   )

   $today = $Now.Date 
   
   if($Past -gt $Now)
   {
     throw "Past cannot be greater than present in time"
   }

   $lunchTime = $today.AddHours(12) 
   if(Test-IsTime -Due $lunchTime @PSBoundParameters)
   {
      _Invoke-Mp3 -Path (Get-LunchAlertPath)

      $qs = Get-LunchQuotes
      if($qs -and $qs.Length -gt 1)
      {
         $quote = $qs | Get-Random
         _Invoke-Speak $quote.Quote 
         _Invoke-Speak $quote.By 
      }
   }    

   $beerTime = $today.AddHours(16).AddMinutes(00)

   $isLastDayOfTheWeek = Test-IsLastWorkDayThisWeek -Today $today
   if($isLastDayOfTheWeek -and (Test-IsTime -Due $beerTime @PSBoundParameters))
   {
      _Invoke-Speak "It is beer time, please proceed to Serraview fridge for a refreshing beverage." 
      _Invoke-Mp3 -Path (Get-BeerTimeAlertPath) 
   }

   $calendar = Get-CalendarEventsToday -CalendarID "wallie@serraview.com" -UserID "wallie@serraview.com"

   if($calendar.items)
   {
      foreach($e in $calendar.items)
      {
         if($e.start.dateTime)
         {
            $startTime = [DateTime]$e.start.dateTime
         }
         elseif($e.start.date)
         {
            $startTime = [DateTime]$e.start.date
         } 
         else
         {
            throw 'unrecongnised time'
         }

         $startTime = $startTime.AddMinutes(-$ToleranceInMins)

         if(Test-IsTime -Due $startTime @PSBoundParameters)
         {
            _Invoke-Mp3 (Get-AnnoncementAlertPath)
            $event = $e.summary
            $location = $e.location 

            $message = "An event is starting soon. " 
            if($event)
            {
               $message = "$event is starting soon. " 
            }

            if($location)
            {
               $message += "Please proceed to $location"
            }

            _Invoke-Speak $message 
         }
      }
   }
}

function Test-IsTime 
{
   param(
      [DateTime]$Due,
      [DateTime]$Now,
      $Past, 
      [int]$ToleranceInMins = 3
   )

   $result = ((($Past -and $Due -ge $Past) -or (-not $Past -and ($Now.Subtract($Due).TotalMinutes -le $ToleranceInMins))) -and $Due -le $Now)
   return $result
}

