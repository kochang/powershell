﻿function Get-RestService{
    param(
    [string]$url,
    [string]$query
    )

    if($query)
    {
        return "$url/api/json?$query"
    }
    return "$url/api/json"
}

function Get-Json{
    param(
    [string]$url,
    [string]$query
    )
    try
    {        
        $uri = (Get-RestService $url $query) 
        return Invoke-WebRequest -Uri $uri -UseBasicParsing | ConvertFrom-Json
    }
    catch
    {
        Write-Error -Exception $_.Exception -Message "Error processing {$uri}`n$_"
    }
}

<#
function Convert-TimeStamp
{
    param(
    $timestamp
    )
    
    $seconds = $timestamp / 1000
    $baseTime = ([DateTime]'1/1/1970').AddSeconds($seconds)
    $currentTime = [System.TimeZone]::CurrentTimeZone.ToLocalTime($baseTime)
    return $currentTime
}
#>


function Get-AllJobs 
{
    param(
    [string]$server
    )
    $root = Get-Json $server "tree=jobs[name,url]"
    return $root.jobs
}

function Get-Job
{
    param(
        $job
    )

    return Get-Json $job.url "tree=lastCompletedBuild[url,number],lastFailedBuild[number],displayName"
}


function Get-LastCompletedBuild
{
    param(
        $job
    )
    return Get-Json $job.lastCompletedBuild.url "tree=culprits[fullName],result,fullDisplayName,timestamp"
}

function Get-JobAnnoncement
{
    param(
        $job
    )
    [string]$message = ""
    
    $latestBuild = Get-LastCompletedBuild $job        
    #$latestBuildTime = Convert-TimeStamp $latestBuild.timestamp
    $success = $latestBuild.result -eq "SUCCESS"       
    $DevNames = Get-DeveloperNicknames 
    $suspectCount = $latestBuild.culprits.Count
    $suspects = ""
    for($i = 0; $i -lt $suspectCount; $i++)
    {
        $culprit =  $latestBuild.culprits[$i]
        $name = $culprit.fullName
        $isLast = $suspectCount -gt 1 -and $i -eq ($suspectCount -1)
        if($suspects -ne "" -and -not $isLast)
        {
            $suspects += ", "
        }
        if($isLast)
        {
            $suspects += " and "
        }
        if($null -ne $DevNames -and $DevNames.ContainsKey($name))
        {
            $suspects += $DevNames[$name]
            continue
        }
        $suspects += $name        
    }  

    if(-not $suspects)
    {
        $suspects = "someone"
    }
    $jobName = $job.displayName    
    <#  
    $duration = [DateTime]::Now.Subtract($latestBuildTime).TotalMinutes.ToString(0)          
    $someMinutes = "$duration minute"
    if($duration -gt 1)
    {
        $someMinutes += "s"
    }
    #>

    if($success)
    {
        if($job.lastFailedBuild)
        {
            [int]$lastBuildNum = $job.lastCompletedBuild.number 
            [int]$lastFailedBuildNum = $job.lastFailedBuild.number

            if(($lastBuildNum - $lastFailedBuildNum) -eq 1)
            {
                $message += "'$jobName' is fixed by "
                $message += $suspects
            }               
        }
    }
    else
    {  
        $message += "'$jobName' is broken by "
        $message += $suspects
    }

    if($message -eq "")
    {
        return $null
    }
    return New-Object psobject -Property @{
        Message = $message
        Succeed = $success
        JobName = $jobName
        }
}

function Get-BuildAnnoncements
{   
   [CmdletBinding()]
    param(
        [string]$Server = "https://build.dev.serraview.com",
        [string]$LogPath = (Get-LogPath),
        [switch]$FocusJobsOnly 
    )
    Start-Logging -LogPath $LogPath

    if($script:IsWorking)
    {
        Add-Log "Skipping call as function is still running" -LogPath $LogPath
        return 
    }

    $script:IsWorking = $true
    if($null -eq $script:SeenBuilds)
    {
        $script:SeenBuilds = @{}
    }
    
    $MuteJobs = Get-MuteJobs 
    $FocusJobs = Get-FocusJobs
    $jobs = @(Get-AllJobs $Server)
    $combinedResult = New-Object psobject -Property @{
        Messages = @()
        Results = @{$true = @(); $false = @()}
    }
    
    for ($i = 0; $i -lt  $jobs.Count; $i++)
    {

        $j = $jobs[$i]
        if($null -ne $MuteJobs -and $MuteJobs.ContainsKey($j.name) -and $MuteJobs[$j.name])
        {
           Add-Log "SKIP: Job {$($j.name)} is muted" -LogPath $LogPath
           continue # job has been muted
        }


        if($FocusJobsOnly) 
        {
           $match = $false
           ForEach ($f in $FocusJobs)
           {
              if($j.name -match $f)
              {
                 $match = $true 
              }
           } 
           if(-not $match)
           {
              Add-Log "SKIP: Job {$($j.name)} is not in focus list" -LogPath $LogPath
              continue # job is not in focus list, skipping it!
           }
        }
                


        $prog = (($i+1) / $jobs.Count) * 100
        Write-Progress -Activity "Loading Jenkins Jobs" -Status "$($prog.ToString("0.00"))% complete" -PercentComplete $prog
        
        
        $job = Get-Job $j
        if(-not $job.lastCompletedBuild)
        {
            Add-Log "SKIP: Job {$($j.name)} does not have completed builds" -LogPath $LogPath
            continue # this job hasn't got a completed build, nothing to do here
        }    

        if($script:SeenBuilds.ContainsKey($job.displayName) -and $script:SeenBuilds[$job.displayName] -eq $job.lastCompletedBuild.url)
        {
           continue # this job has been seen/reported, never mind
        } 

        $script:SeenBuilds[$job.displayName] = $job.lastCompletedBuild.url        
        $result = Get-JobAnnoncement $job
        if($result)
        {
            $combinedResult.Messages += $result.Message
            Add-Log -LogPath $LogPath -Message $result.Message
            $combinedResult.Results[$result.Succeed] += $result.JobName
            
            # track build results module wide
            if($script:BuildResults -eq $null)
            {
                $script:BuildResults = @{}
            }

            if(-not $script:BuildResults.ContainsKey($result.Succeed))
            {        
                $script:BuildResults[$result.Succeed] = @{}
            }
    
            $script:BuildResults[$result.Succeed][$result.JobName] = $null
        }
    }
   
    $script:IsWorking = $false
    Stop-Logging -LogPath $LogPath
    return $combinedResult
}

function Reset-BuildAnnoncements
{
    param(
    [string]$LogPath = (Get-LogPath)
    )
    Start-Logging -LogPath $LogPath 

    $script:SeenBuilds = $null
    $script:IsWorking = $null    
    $script:LogStartTimes = $null
    $script:BuildResults = $null    

    Stop-Logging -LogPath $LogPath 
}

function Invoke-SpeakBuildAnnoncements
{
    [CmdletBinding()]
    param(
        [switch]$SkipSpeakFirstTime,
        [string]$LogPath = (Get-LogPath),
        [string]$OutputFile,
        [switch]$FocusJobsOnly
    )    
    Start-Logging -LogPath $LogPath -ClearLog

    $firstTime = (-not $script:SeenBuilds -or $script:SeenBuilds.Count -eq 0)
    $results = Get-BuildAnnoncements -LogPath $LogPath -FocusJobsOnly:$FocusJobsOnly
    
    if(-not $results.Messages)
    {
        Stop-Logging -LogPath $LogPath
        return 
    }
     
    if($SkipSpeakFirstTime -and $firstTime)
    {
        Stop-Logging -LogPath $LogPath
        return
    }

    if($results.Results[$false].Count -gt 0)
    {
        _Invoke-Mp3 -Path (Get-BrokenBuildAlertPath)      
    }
    else
    {            
        _Invoke-Mp3 -Path (Get-BuildAnnoncementAlertPath)
    }    

    foreach ($a in $results.Messages)
    {
        _Invoke-Speak $a -OutputFile $OutputFile
    }
    
    if($script:BuildResults -and $script:SeenBuilds -and $script:BuildResults[$false].Count -eq 0 -and $script:SeenBuilds.Count -gt 0)
    {
        _Invoke-Mp3 -Path (Get-BuildAllSucceedAlertPath)
    }
    
    Stop-Logging -LogPath $LogPath
}


function Invoke-CheckBuilds
{
    param(
        [string]$LogPath = (Join-Path (Get-LogDir) "CIMonitor.log")
    )
    try
    {
        $lastRan = (Get-TimeStamp)
        # update timestamp

        $now = [DateTime]::Now
        Set-TimeStamp -Value $now

        try
        {
           Invoke-EventAnnoncements -Now $now -Past $lastRan -ToleranceInMins 3 -ErrorAction Stop
        }
        catch
        {   
           if($_.Exception -match "awaiting user interaction")
           {
              _Invoke-Speak "I seem to have trouble authenticating to Google.  Please tend to my request to connect wallie@serraview.com via Internet Explorer in a few minutes."
           }
           Write-DefaultErrorLog -Error $_ -Message "Error occurred in {Invoke-EventAnnoncements}"           
        }

        try
        {
           Invoke-SpeakBuildAnnoncements -SkipSpeakFirstTime -FocusJobsOnly -ErrorAction Stop
        }
        catch
        {
           Write-DefaultErrorLog -Error $_ -Message "Error occurred in {Invoke-SpeakBuildAnnoncements}"
        }


    }
    catch
    {
        Write-DefaultErrorLog -Error $_
    }
}

function Write-DefaultErrorLog
{
   param(
      [String] $Message,
      [System.Management.Automation.ErrorRecord] $Error,
      [String]$LogPath = (Join-Path (Get-LogDir) "CIMonitor.log")

   ) 
   Write-Information "Error written to {$LogPath}" 
   $logMessage = ""

   if($Message)
   {
      $logMessage += "[$([DateTime]::Now.ToString((Get-DateTimeStringFormat)))] $Message"
   }
   if($Error)
   {
      if($logMessage.Length -gt 0)
      {
         $logMessage += "`r`n"
      }
      $logMessage += "[$([DateTime]::Now.ToString((Get-DateTimeStringFormat)))] $(Out-String -InputObject $Error)" 
   }

   Add-Log -Message $logMessage -LogPath $LogPath
   try
   {
      Send-Email -EmailBody $logMessage -From 'wallie@serraview.com' -To 'ko-lin.chang@serraview.com' -Subject "CIMonitor error occurred at {$([DateTime]::Now)} on {$env:COMPUTERNAME}" -UserID 'wallie@serraview.com'
   }
   catch
   {
   }
}


function Start-CheckingCIBuilds
{ 
    $ID = "CheckCIBuildTimer"
    New-TimerEvent -IntervalInMilliseconds ([TimeSpan]::FromMinutes(1).TotalMilliseconds) -ID $ID -Action { Invoke-CheckBuilds }

    Start-TimerEvent -ID $ID
}

function Stop-CheckingCIBuilds
{
    Stop-TimerEvent -ID "CheckCIBuildTimer" | Out-Null
    if(Test-Path (Get-TimeStampLogPath))
    {
       Move-Item (Get-TimeStampLogPath) "$(Get-TimeStampLogPath).done"
    }
}

function logging
{
    param(
        [string]$CallingFunction,
        [string]$LogPath,
        [switch]$Start,
        [switch]$End,
        [switch]$ClearLog
    ) 
    $name = $CallingFunction
    if($ClearLog -and (Test-Path $LogPath))
    {
       Clear-Content $LogPath
    }

    if($null -eq $script:LogStartTimes)
    {
        $script:LogStartTimes = @{}
    }
    if($Start)
    {
        $startTime =  [DateTime]::Now 
        $script:LogStartTimes[$name] = $startTime
        Add-Log "[$($startTime.ToString((Get-DateTimeStringFormat)))] START of $name" -LogPath $LogPath
    }
    if($End)
    {
        $endTime = [DateTime]::Now
        if($script:LogStartTimes.ContainsKey($name))
        {
            [DateTime]$startTime = $script:LogStartTimes[$name]            
            $took = $endTime - $startTime
            Add-Log "[$($endTime.ToString((Get-DateTimeStringFormat)))] END of $($name): took $($took.TotalSeconds) second(s)" -LogPath $LogPath
        }
        else
        {
            Add-Log "[$endTime] END of $name" -LogPath $LogPath
        }
    }
}

function Start-Logging
{
    param(
        [ScriptBlock]$CallingFunction, 
        [string]$LogPath,
        [switch]$ClearLog
    )
    
    logging -CallingFunction ((Get-Variable MyInvocation -Scope 1).Value.MyCommand.Name) -Start -LogPath $LogPath -ClearLog:$ClearLog
}

function Stop-Logging
{

    param(
        [ScriptBlock]$CallingFunction, 
        [string]$LogPath
    )

    logging  -CallingFunction ((Get-Variable MyInvocation -Scope 1).Value.MyCommand.Name) -End -LogPath $LogPath
}

function _Invoke-Mp3
{
    [CmdletBinding()]
    param(
       [Parameter(Mandatory=$true)]    
       [string] $Path,
       [string]$LogPath = (Get-LogPath)
    )
    $now = [DateTime]::Now
    $result = Invoke-Mp3 -Path $Path
    if($result)
    {
       Add-Log "[$($now.ToString((Get-DateTimeStringFormat)))] Playing Mp3: {$Path}" -LogPath $LogPath
       return 
    }
    Add-Log "[$($now.ToString((Get-DateTimeStringFormat)))] Cannot locate Mp3: {$Path}" -LogPath $LogPath
}


Export-ModuleMember -Function Invoke-SpeakBuildAnnoncements, Get-BuildAnnoncements, Reset-BuildAnnoncements, Start-CheckingCIBuilds, Stop-CheckingCIBuilds, Invoke-CheckBuilds
