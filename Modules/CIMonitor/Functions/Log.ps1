﻿function Add-Log 
{
    [CmdletBinding()]
    param(
    [string]$Message,
    [Parameter(Mandatory=$true)]
    [string]$LogPath,
    [switch]$Replace
    )
    $logDir = Split-Path $LogPath
    if(-not (Test-Path $logDir))
    {
        mkdir $logDir
    }
    
    if($Replace)
    {
        $Message > $LogPath
    }
    else
    {
        $Message >> $LogPath
    }
}

Export-ModuleMember -Function Add-Log
