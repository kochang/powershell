﻿function _Invoke-Speak
{
   param(
      [Parameter(ValueFromPipeline=$true)]
      [string]$message,
      [string]$OutputFile,
      [string]$LogPath = (Get-LogPath)
   )

   $time = [DateTime]::Now
   if(Invoke-Speak -Message $message -OutputFile $OutputFile)
   {
      $logMessage = "[$($time.ToString((Get-DateTimeStringFormat)))][$((Get-Volume).ToString("0.000"))][$(if(Get-Mute){ "Muted" } else { "OK" })] $message" 
      Add-Log $logMessage $LogPath
      try
      {
         Send-Email -EmailBody $logMessage -From 'wallie@serraview.com' -To 'ko-lin.chang@serraview.com' -Subject "CIMonitor spoke at {$([DateTime]::Now)} on {$env:COMPUTERNAME}" -UserID 'wallie@serraview.com'
      }
      catch
      {
      }
   }
}

