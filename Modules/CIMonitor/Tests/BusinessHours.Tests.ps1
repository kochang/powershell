$moduleName = "CIMonitor"
$here = Split-Path -Parent $MyInvocation.MyCommand.Path
$modulePath = (Resolve-Path "$here\..\$moduleName.psd1").ProviderPath
Import-Module $modulePath  -Force

InModuleScope $moduleName { 
    $script:DayOfWeek = @{
        [System.DayOfWeek]::Monday = 1
        [System.DayOfWeek]::Tuesday = 2
        [System.DayOfWeek]::Wednesday = 3
        [System.DayOfWeek]::Thursday = 4
        [System.DayOfWeek]::Friday = 5
        [System.DayOfWeek]::Saturday = 6
        [System.DayOfWeek]::Sunday = 7
    }

   Describe "Get-Holidays" {
      
      Mock Get-AssetFile {
         return "$TestDrive\PublicHolidays.json"
      }

      It "get holiday hash object" {
@"
{
   "Holidays": [
      { 
         "Description": "New Year's Day",
         "Date": "2016-01-01"
      },
      { 
         "Description": "New Year's Day",
         "Date": "2017-01-01"
      },
      { 
         "Description": "New Year's Day (additional day)",
         "Date": "2017-01-02"
      },
      { 
         "Description": "Australia Day",
         "Date": "2016-01-26"
      },
      { 
         "Description": "Australia Day",
         "Date": "2017-01-26"
      },
      { 
         "Description": "Labour Day",
         "Date": "2016-03-14"
      },
      { 
         "Description": "Labour Day",
         "Date": "2017-03-13"
      },
      { 
         "Description": "Good Friday",
         "Date": "2016-03-25"
      },
      { 
         "Description": "Good Friday",
         "Date": "2017-04-14"
      },
      { 
         "Description": "Saturday before Easter Sunday",
         "Date": "2016-03-26"
      },
      { 
         "Description": "Saturday before Easter Sunday",
         "Date": "2017-04-15"
      },
      { 
         "Description": "Easter Sunday",
         "Date": "2016-03-27"
      },
      { 
         "Description": "Easter Sunday",
         "Date": "2017-04-16"
      },
      { 
         "Description": "Easter Monday",
         "Date": "2016-03-28"
      },
      { 
         "Description": "Easter Monday",
         "Date": "2017-04-17"
      },
      { 
         "Description": "ANZAC Day",
         "Date": "2016-04-25"
      },
      { 
         "Description": "ANZAC Day",
         "Date": "2017-04-25"
      },
      { 
         "Description": "Queen's Birthday",
         "Date": "2016-06-13"
      },
      { 
         "Description": "Queen's Birthday",
         "Date": "2017-06-12"
      },
      { 
         "Description": "Friday before the AFL Grand Final",
         "Date": "2016-09-30"
      },
      { 
         "Description": "Friday before the AFL Grand Final",
         "Date": "2017-09-29"
      },
      { 
         "Description": "Melbourne Cup",
         "Date": "2016-11-01"
      },
      { 
         "Description": "Melbourne Cup",
         "Date": "2017-11-07"
      },
      { 
         "Description": "Christmas Day",
         "Date": "2016-12-27"
      },
      { 
         "Description": "Christmas Day",
         "Date": "2017-12-25"
      },
      { 
         "Description": "Boxing Day",
         "Date": "2016-12-26"
      }, 
      { 
         "Description": "Boxing Day",
         "Date": "2017-12-26"
      }
   ]
}
"@ >"$TestDrive\PublicHolidays.json"

         $result = Get-Holidays 
         $result.Count | should beGreaterThan 0

      }

      It "return empty hash when error occurred" {
@"
{
    "holidays": [
        {
            "Description": "Some holiday",
            "Date": "2016-12-1x2"
        }
    ]
}
"@ > "$TestDrive\PublicHolidays.json"

         $result = Get-Holidays
         $result.Count | should be 0
      }

   }

   Describe "Test-IsLastWorkDayThisWeek" {
      Mock Get-Holidays {
            return @{}
      }

      It "Weekend is not last work day this week" {
         $today = [DateTime]::Today
         $sunday = $today.AddDays($DayOfWeek[[System.DayOfWeek]::Sunday] - $DayOfWeek[$today.DayOfWeek])

         Test-IsLastWorkDayThisWeek -Today $sunday | should be $false         
         Test-IsLastWorkDayThisWeek -Today $sunday.AddDays(-1) | should be $false
         Test-IsLastWorkDayThisWeek -Today $sunday.AddDays(-2) | should be $true
      }


      It "Friday is generally last work day this week" {      
         $today = [DateTime]::Today
         $friday = $today.AddDays($DayOfWeek[[System.DayOfWeek]::Friday] - $DayOfWeek[$today.DayOfWeek])

         Test-IsLastWorkDayThisWeek -Today $friday | should be $true
         Test-IsLastWorkDayThisWeek -Today $friday.AddDays(-1) | should be $false
         Test-IsLastWorkDayThisWeek -Today $friday.AddDays(-2) | should be $false
         Test-IsLastWorkDayThisWeek -Today $friday.AddDays(-3) | should be $false
         Test-IsLastWorkDayThisWeek -Today $friday.AddDays(-4) | should be $false
         Test-IsLastWorkDayThisWeek -Today $friday.AddDays(-5) | should be $false
      }

      It "Friday is generally last work day this week, even when monday is holiday" {      
       Mock Get-Holidays {
                return @{ 
                    $script:holiday = "Some holiday" 
                }
         }
         $today = [DateTime]::Today
         $friday = $today.AddDays($DayOfWeek[[System.DayOfWeek]::Friday] - $DayOfWeek[$today.DayOfWeek])
         $monday = $today.AddDays($DayOfWeek[[System.DayOfWeek]::Monday] - $DayOfWeek[$today.DayOfWeek])         
         $script:holiday = $monday

         Test-IsLastWorkDayThisWeek -Today $friday | should be $true
         Test-IsLastWorkDayThisWeek -Today $friday.AddDays(-1) | should be $false
         Test-IsLastWorkDayThisWeek -Today $friday.AddDays(-2) | should be $false
         Test-IsLastWorkDayThisWeek -Today $friday.AddDays(-3) | should be $false
         Test-IsLastWorkDayThisWeek -Today $friday.AddDays(-4) | should be $false
         Test-IsLastWorkDayThisWeek -Today $friday.AddDays(-5) | should be $false
         Test-IsHoliday -Today $friday.AddDays(-5) | should be $true
         Test-IsHoliday -Today $friday.AddDays(-4) | should be $true
      }

      It "Friday is generally last work day this week, even when tuesday is holiday" {      
       Mock Get-Holidays {
                return @{ 
                    $script:holiday = "Some holiday" 
                }
         }
         $today = [DateTime]::Today
         $friday = $today.AddDays($DayOfWeek[[System.DayOfWeek]::Friday] - $DayOfWeek[$today.DayOfWeek])
         $tuesday = $today.AddDays($DayOfWeek[[System.DayOfWeek]::Tuesday] - $DayOfWeek[$today.DayOfWeek])         
         $script:holiday = $tuesday

         Test-IsLastWorkDayThisWeek -Today $friday | should be $true
         Test-IsLastWorkDayThisWeek -Today $friday.AddDays(-1) | should be $false
         Test-IsLastWorkDayThisWeek -Today $friday.AddDays(-2) | should be $false
         Test-IsLastWorkDayThisWeek -Today $friday.AddDays(-3) | should be $false
         Test-IsLastWorkDayThisWeek -Today $friday.AddDays(-4) | should be $false
         Test-IsLastWorkDayThisWeek -Today $friday.AddDays(-5) | should be $false
         Test-IsHoliday -Today $friday.AddDays(-4) | should be $false
         Test-IsHoliday -Today $friday.AddDays(-3) | should be $true
      }

      It "Friday is public holiday so it is not last work day this week" {
         Mock Get-Holidays {
                return @{ $script:holiday = "Some holiday" }
         }
         $today = [DateTime]::Today         
         $friday = $today.AddDays($DayOfWeek[[System.DayOfWeek]::Friday] - $DayOfWeek[$today.DayOfWeek])         
         $script:holiday = $friday

         Test-IsLastWorkDayThisWeek -Today $friday | should be $false
         Test-IsLastWorkDayThisWeek -Today $friday.AddDays(-1) | should be $true
      }

      It "Friday and Thursday are public holidays so they are not last work day this week" {
       Mock Get-Holidays {
                return @{ 
                    $script:holiday = "Some holiday" 
                    $script:holiday.AddDays(1) = "Some other holiday"
                }
         }
         $today = [DateTime]::Today         
         $thursday = $today.AddDays($DayOfWeek[[System.DayOfWeek]::Thursday] - $DayOfWeek[$today.DayOfWeek])         
         $script:holiday = $thursday

         Test-IsLastWorkDayThisWeek -Today $thursday | should be $false         
         Test-IsLastWorkDayThisWeek -Today $thursday.AddDays(-1) | should be $true
      }

      It "Friday, Thursday, Wednesday are public holidays so they are not last work day this week" {
       Mock Get-Holidays {
                return @{ 
                    $script:holiday = "Some holiday" 
                    $script:holiday.AddDays(1) = "Some other holiday"
                    $script:holiday.AddDays(2) = "Some other holiday"
                }
         }
         $today = [DateTime]::Today         
         $wednesday = $today.AddDays($DayOfWeek[[System.DayOfWeek]::Wednesday] - $DayOfWeek[$today.DayOfWeek])         
         $script:holiday = $wednesday

         Test-IsLastWorkDayThisWeek -Today $wednesday | should be $false             
         Test-IsLastWorkDayThisWeek -Today $wednesday.AddDays(-1) | should be $true
      }

      It "Friday, Thursday, Wednesday, Tuesday are public holidays so they are not last work day this week" {
       Mock Get-Holidays {
                return @{ 
                    $script:holiday = "Some holiday" 
                    $script:holiday.AddDays(1) = "Some other holiday"
                    $script:holiday.AddDays(2) = "Some other holiday"
                    $script:holiday.AddDays(3) = "Some other holiday"
                }
         }
         $today = [DateTime]::Today         
         $tuesday = $today.AddDays($DayOfWeek[[System.DayOfWeek]::Tuesday] - $DayOfWeek[$today.DayOfWeek])         
         $script:holiday = $tuesday

         Test-IsLastWorkDayThisWeek -Today $tuesday | should be $false             
         Test-IsLastWorkDayThisWeek -Today $tuesday.AddDays(-1) | should be $true
      }

      It "Friday, Thursday, Wednesday, Tuesday, Monday are public holidays so they are not last work day this week" {
       Mock Get-Holidays {
                return @{ 
                    $script:holiday = "Some holiday" 
                    $script:holiday.AddDays(1) = "Some other holiday"
                    $script:holiday.AddDays(2) = "Some other holiday"
                    $script:holiday.AddDays(3) = "Some other holiday"
                    $script:holiday.AddDays(4) = "Some other holiday"
                }
         }
         $today = [DateTime]::Today         
         $monday = $today.AddDays($DayOfWeek[[System.DayOfWeek]::Monday] - $DayOfWeek[$today.DayOfWeek])         
         $script:holiday = $monday
         
         Test-IsLastWorkDayThisWeek -Today $monday.AddDays(8) | should be $false 
         Test-IsLastWorkDayThisWeek -Today $monday.AddDays(7) | should be $false 
         Test-IsLastWorkDayThisWeek -Today $monday.AddDays(6) | should be $false 
         Test-IsLastWorkDayThisWeek -Today $monday.AddDays(5) | should be $false 
         Test-IsLastWorkDayThisWeek -Today $monday.AddDays(3) | should be $false 
         Test-IsLastWorkDayThisWeek -Today $monday.AddDays(2) | should be $false 
         Test-IsLastWorkDayThisWeek -Today $monday.AddDays(1) | should be $false 
         Test-IsLastWorkDayThisWeek -Today $monday | should be $false             
         Test-IsLastWorkDayThisWeek -Today $monday.AddDays(-1) | should be $false 
         Test-IsLastWorkDayThisWeek -Today $monday.AddDays(-2) | should be $false          
         Test-IsLastWorkDayThisWeek -Today $monday.AddDays(-3) | should be $true
      }

      It "Thursday is last day of work when Friday, Wednesday are public holidays" {
       Mock Get-Holidays {
                return @{ 
                    $script:holiday = "Some holiday" 
                    $script:holiday.AddDays(1) = "Some other holiday"
                    $script:holiday.AddDays(3) = "Some other holiday"
                }
         }
         $today = [DateTime]::Today         
         $tuesday = $today.AddDays($DayOfWeek[[System.DayOfWeek]::Tuesday] - $DayOfWeek[$today.DayOfWeek])         
         $script:holiday = $tuesday

         Test-IsLastWorkDayThisWeek -Today $tuesday | should be $false             
         Test-IsHoliday -Today $tuesday.AddDays(1) | should be $true
         Test-IsLastWorkDayThisWeek -Today $tuesday.AddDays(2) | should be $true
         Test-IsHoliday -Today $tuesday.AddDays(2) | should be $false
         Test-IsHoliday -Today $tuesday.AddDays(3) | should be $true
      }
   }
}
