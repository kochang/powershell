﻿$moduleName = "CIMonitor"
$here = Split-Path -Parent $MyInvocation.MyCommand.Path
$modulePath = (Resolve-Path "$here\..\$moduleName.psd1").ProviderPath
Import-Module $modulePath  -Force

InModuleScope CIMonitor{

<#

function Get-TimeStamp
{
    $timestampPath = (Get-TimeStampLogPath)

    $lastRan = $null
    if(Test-Path $timestampPath)
    {
        $lastRan = [datetime](Get-Content $timestampPath -Raw)
    }

    return $lastRan
}

function Get-TimeStampLocalTime
{
    $time = Get-TimeStamp
    if($time)
    {
        return $time.ToLocalTime()
    }
    return $time
}


#>

   Describe "Get-BuildAnnoncementAlertPath" {
      It "returns build annoncement alert path" {

         Mock Get-AssetFile { return Join-Path $TestDrive $RequestingFile }

         (Get-BuildAnnoncementAlertPath) | should be "$TestDrive\BuildAnnoncementAlert.mp3" 
      }
   }

   Describe "Get-AnnoncementAlertPath" {
      It "returns annoncement alert path" {

         Mock Get-AssetFile { return Join-Path $TestDrive $RequestingFile }

         (Get-AnnoncementAlertPath) | should be "$TestDrive\AnnoncementAlert.mp3" 
      }
   }

   Describe "Get-BeerTimeAlertPath" {
      It "returns beer time alert path" {

         Mock Get-AssetFile { return Join-Path $TestDrive $RequestingFile }

         (Get-BeerTimeAlertPath) | should be "$TestDrive\BeerTime.mp3" 
      }
   }
   Describe "Get-BrokenBuildAlertPath" {
      It "returns broken build song" {

         Mock Get-AssetFile { return Join-Path $TestDrive $RequestingFile }

         (Get-BrokenBuildAlertPath) | should be "$TestDrive\BrokenBuild.mp3" 
      }
   }

   Describe "Get-BuildAllSucceedAlertPath" {
      It "returns build all passed song" {

         Mock Get-AssetFile { return Join-Path $TestDrive $RequestingFile }

         (Get-BuildAllSucceedAlertPath) | should be "$TestDrive\StageClear.mp3" 
      }
   }

   Describe "Get-BirthdaySongPath" {
      It "returns birthday song path" {

         Mock Get-AssetFile { return Join-Path $TestDrive $RequestingFile }

         (Get-BirthdaySongPath) | should be "$TestDrive\happy.birthday.mp3" 
      }
   }

   Describe "Get-ArrayFromFile" {
      It "Get-ArrayFromFile" {
         $filePath = (Join-Path $TestDrive "arrayfile.txt")
         @"
Arrr
Brrr
Crrr
"@ > $filePath
         Mock Get-AssetFile { return $filePath }
         $result = Get-ArrayFromFile -FileName (Split-Path $filePath -Leaf)
         $result[0] | should be "Arrr"
         $result[1] | should be "Brrr"
         $result[2] | should be "Crrr"
         $result.Count | should be 3

      }
      It "returns empty array when failed to location file" {
         Mock Get-AssetFile { return $null } 
         $result = Get-ArrayFromFile -FileName 'huh?'
         $result.Count | should be 0

      }
   }
    Describe "Get-DateTimeStringFormat" {
         It "yyyy-MM-dd hh:mm:ss ddd" {
            (Get-DateTimeStringFormat) | should be "yyyy-MM-dd hh:mm:ss ddd"
         }
    }

    Describe "Get-TimeStamp"{
        Context "Get-TimeStamp" {
            It "gets last ran timestamp of the pooling function" {
                $now = [Datetime]::Now
                $now.ToUniversalTime().ToString("yyyy-MM-dd.HH:mm:ss") > (Join-Path $TestDrive "timestamp.log")

                Mock Get-TimeStampLogPath { return Join-Path $TestDrive "timestamp.log"}

                [Math]::Abs((New-TimeSpan -Start $now -End (Get-TimeStamp)).TotalSeconds) | should beLessThan 1
            }
        }

        Context "Get-TimeStamp" {
            It "gets last ran timestamp of the pooling function" {                
                Mock Get-TimeStampLogPath { return Join-Path $TestDrive "timestamp.log"}

                Get-TimeStamp | should be $null
            }
        }
    }

    Describe "Set-TimeStamp" {
        
        Mock Get-TimeStampLogPath { return Join-Path $TestDrive "timestamp.log"}
        
        It "sets timestamp the first time" {                    
            $now = [Datetime]::Now
            Set-TimeStamp $now
            
            [Math]::Abs((New-TimeSpan -Start $now -End (Get-TimeStamp)).TotalSeconds) | should beLessThan 1
        }

        It "sets timestamp again" {              
            $now = [Datetime]::Now
            Set-TimeStamp $now
            
            [Math]::Abs((New-TimeSpan -Start $now -End (Get-TimeStamp)).TotalSeconds) | should beLessThan 1
        }
    }


    Describe "Get-ObjectFromJsonFile" {        

        It "parse object from json" {      
            Set-Content -Path (Join-Path $TestDrive "LunchQuotes.json") -Value @"
[
    {
        "Quote": "Note on a door: Out to lunch; if not back by five, out for dinner also.", 
        "By": "Unknown"
    },
    {
        "Quote": "When people you greatly admire appear to be thinking deep thoughts, they probably are thinking about lunch.", 
        "By": "Douglas Adams"
    }
]
"@
            Mock Get-AssetFile { return (Join-Path $TestDrive "LunchQuotes.json")} 
            
            $result = Get-ObjectFromJsonFile "LunchQuotes.json" 
            $result.Length | should be 2
            $result[0].Quote | should be "Note on a door: Out to lunch; if not back by five, out for dinner also."
            $result[0].By | should be "Unknown"
            $result[1].Quote | should be "When people you greatly admire appear to be thinking deep thoughts, they probably are thinking about lunch."
            $result[1].By | should be "Douglas Adams"                       
        }
    }

    Describe "Get-CIMonitorID" {
        It "gets instance id" {
            Get-CIMonitorID | should match '^[\w]{8}-[\w]{4}-[\w]{4}-[\w]{4}-[\w]{12}$'
        }
    }

    Describe "Get-TimeStampLogPath" {

        Mock Get-LogDir { return $TestDrive }
        Mock Get-CIMonitorID { return "someInstanceID"}

        It "return timestamp log file path" {
        # Join-Path (Get-LogDir) "$(Get-CIMonitorID).timestamp.log"
            Get-TimeStampLogPath | should be (Join-Path $TestDrive "TimeStamp.someInstanceID.log")    
        }
    }

    Describe "Get-LunchAlertPath" {

        It "gets lunch song path" {
            Get-LunchAlertPath | should be (Join-Path $Script:CIMonitorRoot "DefaultConfigurations\LunchAlert.mp3")
        }
    }

    Describe "Get-LunchQuotes" {

        It "gets lunch quote" {
            $result = Get-LunchQuotes 

            $result.Length | should be 2
            $result[0].Quote | should be "It is more fun to talk with someone who doesn't use long, difficult words but rather short, easy words like ""What about lunch?"""
            $result[0].By | should be "Winnie the Pooh"        
            $result[1].Quote | should be "When people you greatly admire appear to be thinking deep thoughts, they probably are thinking about lunch."
            $result[1].By | should be "Douglas Adams"
        }

    }


    Describe "Configurations"{
        
        Context "Import-CIMonitorConfiguration"{
            It "loads path config file"{
                Import-CIMonitorConfiguration
                $Script:ConfigPaths["Asset"] | should be "C:\CIMonitor\Asset"
                $Script:ConfigPaths["Log"] | should be "C:\CIMonitor\Log"
                $Script:ConfigPaths["GoogleAPI"] | should be "C:\CIMonitor\GoogleAPICredStore"
                $Script:ConfigPaths.Count | should be 3
            }

            It "loads specified file"{
                $testPath = (Join-Path $TestDrive "Path.txt")
                $logPath = (Join-Path $TestDrive "Log")
                $assetPath = (Join-Path $TestDrive "Asset")
                $somethingElse = (Join-Path $TestDrive "somethingElse")
                "Log = $($logPath.Replace("\", "\\"))" >> $testPath
                "Asset = $($assetPath.Replace("\", "\\"))" >> $testPath
                "SomethingElse = $($somethingElse.Replace("\", "\\"))" >> $testPath

                Import-CIMonitorConfiguration -Path $testPath
                $Script:ConfigPaths["Asset"] | should be $assetPath
                $Script:ConfigPaths["Log"] | should be $logPath
                $Script:ConfigPaths["SomethingElse"] | should be $somethingElse
                $Script:ConfigPaths.Count | should be 3
            }
        }

        Context "Get-AssetFile"{
            It "loads asset from default if configured path does not exist"{            
                $configFile = Join-Path $TestDrive "Path.txt"
                "Log = somestuff" >> $configFile
                Import-CIMonitorConfiguration -Path $configFile
                $result = Get-AssetFile -RequestingFile "BrokenBuild.mp3" 
                $result | should exist
                $result | should be "$(Join-Path $script:CIMonitorRoot "DefaultConfigurations\BrokenBuild.mp3")"
            }
            
            It "loads asset from Path.txt"{
                $assetDir = (mkdir (Join-Path $TestDrive "Asset")).FullName           
                $configFile = Join-Path $TestDrive "Path.txt"   
                "Asset = $($assetDir.Replace("\", "\\"))" >> $configFile
                "Whatsup!" >> (Join-Path $assetDir "HelloThere.txt")                
                Import-CIMonitorConfiguration -Path $configFile
                $result = Get-AssetFile -RequestingFile "HelloThere.txt" 
                $result | should exist

                Get-Content -Path $result -Raw | should be "Whatsup!`r`n"
            }

            It "default to module directory if configured asset directory doesn't have it"{
                $result = Get-AssetFile -RequestingFile "BrokenBuild.mp3"
                $result | should exist
            }  
            
            It "returns null when cannot find file"{
                $result = Get-AssetFile -RequestingFile "shouldntexist"
                $result | should be $null
                
            }          
        }

        Context "Get-ConfigDir" {
            It "gets Path.txt configured path"{
                Import-CIMonitorConfiguration

                $result = Get-ConfigDir "Asset"
                $result | should be "C:\CIMonitor\Asset"
            }

            It "uses current location if not found by name"{
                $result = Get-ConfigDir "SomethingElse"
                $result | should be "$(Join-Path "$script:CIMonitorRoot\DefaultConfigurations" "SomethingElse")"
            }
        }

        Context "Get-LogDir" {
            It "gets log dir"{
                $result = Get-LogDir
                $result | should be "C:\CIMonitor\Log"
            }
        }

        Context "Get-LogPath"{
            It "get log path by calling function name"{
                $pathtxt = Join-Path $TestDrive "Path.txt"
                $logPath = Join-Path $TestDrive "Log"
                "Log = $($logPath.Replace("\", "\\"))" >> $pathtxt
                Import-CIMonitorConfiguration -Path $pathtxt

                $result = Get-LogPath
                #well, this is not a function calling so it will be null therefore the .log
                $logPath | should exist
                $result | should be (Join-Path $logPath ".log") 
            }
        }

        Context "Get-HashObjectFromFile"{
            It "convert string data from file"{
                $file = Join-Path $TestDrive "Test.data"
                "A = 1" >> $file
                "B = 2" >> $file
                Mock Get-AssetFile { return $file }

                $result = Get-HashObjectFromFile "Test.data"
                $result.ContainsKey("A") | should be $true
                $result.ContainsKey("B") | should be $true
                $result.Keys.Count | should be 2
                $result["A"] | should be 1
                $result["B"] | should be 2
            }

            It "turns empty hash when failed to locate file"{
                
                $result = Get-HashObjectFromFile "Another.test"
                $result.Keys.Count | should be 0
            }
        }

        Context "Get-ArrayFromFile" {
           It "retrieve lines from file as an array" {
              $file = Join-Path $TestDrive "Test.data"
              "This is the first line" >> $file
              "This is the second line" >> $file

              Mock Get-AssetFile { return $file }

              $result = Get-ArrayFromFile "Test.data"
              $result.Count | should be 2
              $result[0] | should be "This is the first line"
              $result[1] | should be "This is the second line"
           }
        }

        Context "Get-FocusJobs"{
            It "load focus jobs"{
                $result = Get-FocusJobs 
                $result.Count | should be 50   
                $result[0] | should be "^Dashboards[\w\W]+develop"
            }
        }

        
        Context "Get-MuteJobs"{
            It "load mute jobs"{
                $result = Get-MuteJobs 
                $result | should be $null
            }
        }

        Context "Get-DeveloperNicknames"{
            It "can get nickname"{
                $result = Get-DeveloperNicknames
                $result.Count | should beGreaterThan 1
                $result.ContainsKey("Ko-Lin Chang") | should be $true
                $result["Ko-Lin Chang"] | should be "Kohh"
            }
        }

        Context "Remove-Log"{
            $logPath = (mkdir (Join-Path $TestDrive "Log")).FullName
            "Hello" >> (Join-Path $logPath "First.log")
            "How's going?" >> (Join-Path $logPath "Second.log")
            Mock Get-LogDir { return $logPath }

            It "will remove logs"{
                Remove-Logs

                $logPath | should exist
                (Get-ChildItem $logPath -Recurse).Count | should be 0
            }

        }

        Context "Start-ReadingAllLogs"{     
            $logPath = (mkdir (Join-Path $TestDrive "Log")).FullName
            "Hello" >> (Join-Path $logPath "First.log")
            "How's going?" >> (Join-Path $logPath "Second.log")
            Mock Start-Process {}
            Mock Get-LogDir { return $logPath }
            It "can launch process to read logs"{
                Start-ReadAllLogs
                Assert-MockCalled Start-Process -Times 2
            }

            It "can launch process on another computer"{
                Start-ReadAllLogs -ComputerName $env:COMPUTERNAME #it's actually the same pc 
                Assert-MockCalled Start-Process -Times 2
            }

            It "will throw error when log dir can't be found"{
                $notFoundPath = Join-Path $TestDrive "$(New-Guid)"
                Mock Get-LogDir {return $notFoundPath }

                { Start-ReadAllLogs } | should throw "can't find log directory {$notFoundPath}"
            }
        }
    }        
}
