﻿$moduleName = "CIMonitor"
$here = Split-Path -Parent $MyInvocation.MyCommand.Path
$modulePath = (Resolve-Path "$here\..\$moduleName.psd1").ProviderPath
Import-Module $modulePath  -Force

InModuleScope CIMonitor {
   Set-StorePath (Join-Path (Get-ConfigDir -Default) "GoogleAPICredStore")
   Describe "Test-IsTime" {
      It "IsTime" {
         $now = [DateTime]::Now
         Test-IsTime -Now $now -Due $now.AddMinutes(1) | should be $false
         Test-IsTime -Now $now -Due $now.AddMinutes(2) | should be $false
         Test-IsTime -Now $now -Due $now.AddMinutes(3) | should be $false
         Test-IsTime -Now $now -Due $now.AddMinutes(4) | should be $false


         Test-IsTime -Now $now -Due $now.AddMinutes(1) -Tolerance 0 | should be $false


         Test-IsTime -Now $now -Due $now.AddMinutes(-1) | should be $true
         Test-IsTime -Now $now -Due $now.AddMinutes(-2) | should be $true
         Test-IsTime -Now $now -Due $now.AddMinutes(-3) | should be $true
         Test-IsTime -Now $now -Due $now.AddMinutes(-4) | should be $false
         Test-IsTime -Now $now -Due $now.AddMinutes(-3) -Tolerance 4 | should be $true
         Test-IsTime -Now $now -Due $now.AddMinutes(-4) -Tolerance 4 | should be $true
         Test-IsTime -Now $now -Due $now.AddMinutes(-5) -Tolerance 4 | should be $false


         Test-IsTime -Now $now -Due $now.AddMinutes(-3) -Past $now.AddMinutes(-2) | should be $false
         Test-IsTime -Now $now -Due $now.AddMinutes(-1) -Past $now.AddMinutes(-2) | should be $true
         Test-IsTime -Now $now -Due $now.AddMinutes(-2) -Past $now.AddMinutes(-2) | should be $true
         Test-IsTime -Now $now -Due $now.AddMinutes(-2) -Past $now.AddMinutes(-3) | should be $true
         Test-IsTime -Now $now -Due $now.AddMinutes(-3) -Past $now.AddMinutes(-3) | should be $true
         Test-IsTime -Now $now -Due $now.AddMinutes(-4) -Past $now.AddMinutes(-3) | should be $false
         Test-IsTime -Now $now -Due $now.AddMinutes(-4) -Past $now.AddMinutes(-3) -Tolerance 4 | should be $false
      }
   }

   Describe "Invoke-EventAnnoncements" {
      $script:DayOfWeek = @{
        [System.DayOfWeek]::Monday = 1
        [System.DayOfWeek]::Tuesday = 2
        [System.DayOfWeek]::Wednesday = 3
        [System.DayOfWeek]::Thursday = 4
        [System.DayOfWeek]::Friday = 5
        [System.DayOfWeek]::Saturday = 6
        [System.DayOfWeek]::Sunday = 7
      }

     Mock _Invoke-Speak { }
     Mock _Invoke-Mp3 { }
     $script:startTime = [DateTime]::Now
     $script:endTime = [DateTime]::Now.AddMinutes(30)
     $script:startTimeStr = [System.Xml.XmlConvert]::ToString($script:startTime, [System.Xml.XmlDateTimeSerializationMode]::RoundtripKind)
     $script:endTimeStr = [System.Xml.XmlConvert]::ToString($script:endTime, [System.Xml.XmlDateTimeSerializationMode]::RoundtripKind)
     $script:summary = "test"
     $script:location = "Ko's desk"


     $script:Content = @"
{
 "kind": "calendar#events",
 "etag": "\"p3389jbs1kqfd40g\"",
 "summary": "wallie@serraview.com",
 "updated": "2017-02-20T08:51:56.322Z",
 "timeZone": "Australia/Sydney",
 "accessRole": "owner",
 "defaultReminders": [
  {
   "method": "popup",
   "minutes": 10
  }
 ],
 "nextSyncToken": "CNCZr4GmntICENCZr4GmntICGAU=",
 "items": [
  {
   "kind": "calendar#event",
   "etag": "\"2975161432084000\"",
   "id": "2i6pdl2js3snq0cvlka3cs274s",
   "status": "confirmed",
   "htmlLink": "https://www.google.com/calendar/event?eid=Mmk2cGRsMmpzM3NucTBjdmxrYTNjczI3NHMgd2FsbGllQHNlcnJhdmlldy5jb20",
   "created": "2017-02-20T08:49:25.000Z",
   "updated": "2017-02-20T08:51:56.042Z",
   "summary": "$script:summary",
   "location": "$script:location",
   "creator": {
    "email": "kochang.chu@gmail.com",
    "displayName": "Ko-Lin Chang"
   },
   "organizer": {
    "email": "kochang.chu@gmail.com",
    "displayName": "Ko-Lin Chang"
   },
   "start": {
    "dateTime": "$($script:startTimeStr)"
   },
   "end": {
    "dateTime": "$($script:endTimeStr)"
   },
   "iCalUID": "2i6pdl2js3snq0cvlka3cs274s@google.com",
   "sequence": 0,
   "attendees": [
    {
     "email": "kochang.chu@gmail.com",
     "displayName": "Ko-Lin Chang",
     "organizer": true,
     "responseStatus": "accepted"
    },
    {
     "email": "wallie@serraview.com",
     "displayName": "mr wallie",
     "self": true,
     "responseStatus": "needsAction"
    }
   ],
   "reminders": {
    "useDefault": true
   }
  }
 ]
} 
"@ 
        Mock Get-CalendarEventsToday {
              return $script:Content | ConvertFrom-Json
        }

     It "should annonce calendar event before it starts" {

        $now = $script:startTime.AddMinutes(-3)
        $past = $script:startTime.AddMinutes(-4)
        Invoke-EventAnnoncements -Now $now -Past $past -ToleranceInMins 3
        Assert-VerifiableMocks
        Assert-MockCalled _Invoke-Speak -Scope It -Exactly -Times 1 -ParameterFilter { $Message -eq "$script:summary is starting soon. Please proceed to $location" }
        Assert-MockCalled _Invoke-Mp3 -Scope It -Exactly -Times 1 -ParameterFilter { $Path -match "AnnoncementAlert.mp3$" }

     }

     It "should not anuonce calendar event in the past" {
        $now = $script:startTime.AddMinutes(-1)
        $past = $script:startTime.AddMinutes(-2)
        Invoke-EventAnnoncements -Now $now -Past $past -ToleranceInMins 3
        Assert-VerifiableMocks
        Assert-MockCalled _Invoke-Speak -Scope It -Exactly -Times 0 -ParameterFilter { $Message -eq "$script:summary is starting soon. Please proceed to $location" }
        Assert-MockCalled _Invoke-Mp3 -Scope It -Exactly -Times 0 -ParameterFilter { $Path -match "AnnoncementAlert.mp3$" }
     }

     It "should not anuonce calendar event in the future" {
        $now = $script:startTime.AddMinutes(-5)
        $past = $script:startTime.AddMinutes(-6)
        Invoke-EventAnnoncements -Now $now -Past $past -ToleranceInMins 3
        Assert-VerifiableMocks
        Assert-MockCalled _Invoke-Speak -Scope It -Exactly -Times 0 -ParameterFilter { $Message -eq "$script:summary is starting soon. Please proceed to $location" }
        Assert-MockCalled _Invoke-Mp3 -Scope It -Exactly -Times 0 -ParameterFilter { $Path -match "AnnoncementAlert.mp3$" }
     }
     
     It "should annonce lunch event" {        
             
         Invoke-EventAnnoncements -Now ([DateTime]::Today.AddHours(12).AddSeconds(44)) -Past ([DateTime]::Today.AddHours(11).AddMinutes(59).AddSeconds(33))

         Assert-VerifiableMocks
         Assert-MockCalled _Invoke-Speak -Exactly -Times 1 -ParameterFilter { @("When people you greatly admire appear to be thinking deep thoughts, they probably are thinking about lunch.", "It is more fun to talk with someone who doesn't use long, difficult words but rather short, easy words like ""What about lunch?""") -contains $Message } -Scope It
         Assert-MockCalled _Invoke-Speak -Exactly -Times 1 -ParameterFilter { @("Douglas Adams", "Winnie The Pooh") -contains $Message } -Scope It
     }
     
     It "should annonce lunch within tolerance" {    

         Invoke-EventAnnoncements -Now ([DateTime]::Today.AddHours(12).AddMinutes(2).AddSeconds(44))

         Assert-VerifiableMocks
         Assert-MockCalled _Invoke-Speak -Exactly -Times 1 -ParameterFilter { @("When people you greatly admire appear to be thinking deep thoughts, they probably are thinking about lunch.", "It is more fun to talk with someone who doesn't use long, difficult words but rather short, easy words like ""What about lunch?""") -contains $Message } -Scope It
         Assert-MockCalled _Invoke-Speak -Exactly -Times 1 -ParameterFilter { @("Douglas Adams", "Winnie The Pooh") -contains $Message } -Scope It
     }

     It "should not annonce lunch within tolerance" {    

         Invoke-EventAnnoncements -Now ([DateTime]::Today.AddHours(12).AddMinutes(3).AddSeconds(1))

         Assert-VerifiableMocks
         Assert-MockCalled _Invoke-Speak -Exactly -Times 0 -Scope It
     }

     

     It "cannot have past ahead of present" {
         { Invoke-EventAnnoncements -Now ([DateTime]::Now) -Past ([DateTime]::Now.AddMilliseconds(11)) } | should throw "Past cannot be greater than present in time"
     }

     It "will not annonce lunch time before due" {
         Invoke-EventAnnoncements -Now ([DateTime]::Today.AddHours(11).AddMinutes(59)) -Past ([DateTime]::Today.AddHours(11).AddMinutes(58).AddSeconds(33))

         Assert-VerifiableMocks
         Assert-MockCalled _Invoke-Speak -Exactly -Times 0 -Scope It
     }

     
     It "will not annonce lunch time after it's due" {
         Invoke-EventAnnoncements -Now ([DateTime]::Today.AddHours(12).AddMinutes(02)) -Past ([DateTime]::Today.AddHours(12).AddMinutes(00).AddSeconds(33))

         Assert-VerifiableMocks
         Assert-MockCalled _Invoke-Speak -Exactly -Times 0 -Scope It
     } 

     It "will annonce beer time when it's due" { 
         $today = [DateTime]::Today
         $tuesday = $today.AddDays($DayOfWeek[[System.DayOfWeek]::Tuesday] - $DayOfWeek[$today.DayOfWeek])         
         $friday = $today.AddDays($DayOfWeek[[System.DayOfWeek]::Friday] - $DayOfWeek[$today.DayOfWeek])         
         $script:holiday = $tuesday
         Mock Get-Holidays {
            return @{ 
              $script:holiday = "Some holiday" 
            }
         }

         Invoke-EventAnnoncements -Now $friday.AddHours(16).AddMinutes(00) 

         Assert-VerifiableMocks
         Assert-MockCalled _Invoke-Speak -Exactly -Times 1 -Scope It -ParameterFilter { $message -eq "It is beer time, please proceed to Serraview fridge for a refreshing beverage." }
         Assert-MockCalled _Invoke-Mp3 -Exactly -Times 1 -Scope It -ParameterFilter { $Path -match "BeerTime.mp3" }
     } 

     It "will annonce beer time after it's due" { 
         $today = [DateTime]::Today
         $tuesday = $today.AddDays($DayOfWeek[[System.DayOfWeek]::Tuesday] - $DayOfWeek[$today.DayOfWeek])         
         $friday = $today.AddDays($DayOfWeek[[System.DayOfWeek]::Friday] - $DayOfWeek[$today.DayOfWeek])         
         $script:holiday = $tuesday
         Mock Get-Holidays {
            return @{ 
              $script:holiday = "Some holiday" 
            }
         }

         Invoke-EventAnnoncements -Now $friday.AddHours(16).AddMinutes(01) 

         Assert-VerifiableMocks
         Assert-MockCalled _Invoke-Speak -Exactly -Times 1 -Scope It -ParameterFilter { $message -eq "It is beer time, please proceed to Serraview fridge for a refreshing beverage." }
         Assert-MockCalled _Invoke-Mp3 -Exactly -Times 1 -Scope It -ParameterFilter { $Path -match "BeerTime.mp3" }
     } 

     It "will not annonce beer time when it's already due" { 
         $today = [DateTime]::Today
         $tuesday = $today.AddDays($DayOfWeek[[System.DayOfWeek]::Tuesday] - $DayOfWeek[$today.DayOfWeek])         
         $friday = $today.AddDays($DayOfWeek[[System.DayOfWeek]::Friday] - $DayOfWeek[$today.DayOfWeek])         
         $script:holiday = $tuesday
         Mock Get-Holidays {
            return @{ 
              $script:holiday = "Some holiday" 
            }
         }

         Invoke-EventAnnoncements -Now $friday.AddHours(16).AddMinutes(03) -Past $friday.AddHours(16).AddMinutes(02) 

         Assert-VerifiableMocks
         Assert-MockCalled _Invoke-Speak -Exactly -Times 0 -Scope It -ParameterFilter { $message -eq "It is beer time, please proceed to Serraview fridge for a refreshing beverage." }
         Assert-MockCalled _Invoke-Mp3 -Exactly -Times 0 -Scope It -ParameterFilter { $Path -match "BeerTime.mp3" }
     } 

     It "will not annonce beer time before it's due" { 
         $today = [DateTime]::Today
         $tuesday = $today.AddDays($DayOfWeek[[System.DayOfWeek]::Tuesday] - $DayOfWeek[$today.DayOfWeek])         
         $friday = $today.AddDays($DayOfWeek[[System.DayOfWeek]::Friday] - $DayOfWeek[$today.DayOfWeek])         
         $script:holiday = $tuesday
         Mock Get-Holidays {
            return @{ 
              $script:holiday = "Some holiday" 
            }
         }

         Invoke-EventAnnoncements -Now $friday.AddHours(15).AddMinutes(33) -Past $friday.AddHours(15).AddMinutes(32) 

         Assert-VerifiableMocks
         Assert-MockCalled _Invoke-Speak -Exactly -Times 0 -Scope It -ParameterFilter { $message -eq "It is beer time, please proceed to Serraview fridge for a refreshing beverage." }
         Assert-MockCalled _Invoke-Mp3 -Exactly -Times 0 -Scope It -ParameterFilter { $Path -match "BeerTime.mp3" }
     } 

     It "will not annonce beer time when its holiday" { 
         $today = [DateTime]::Today
         $friday = $today.AddDays($DayOfWeek[[System.DayOfWeek]::Friday] - $DayOfWeek[$today.DayOfWeek])         
         $thursday = $today.AddDays($DayOfWeek[[System.DayOfWeek]::Friday] - $DayOfWeek[$today.DayOfWeek])         
         $script:holiday = $friday
         Mock Get-Holidays {
            return @{ 
              $script:holiday = "Some holiday" 
            }
         }

         Invoke-EventAnnoncements -Now $friday.AddHours(16).AddMinutes(01) -Past $friday.AddHours(15).AddMinutes(59) 

         Assert-VerifiableMocks
         Assert-MockCalled _Invoke-Speak -Exactly -Times 0 -Scope It -ParameterFilter { $message -eq "It is beer time, please proceed to Serraview fridge for a refreshing beverage." }
         Assert-MockCalled _Invoke-Mp3 -Exactly -Times 0 -Scope It -ParameterFilter { $Path -match "BeerTime.mp3" }
     } 

     It "will not annonce beer time when it is not last day" { 
         $today = [DateTime]::Today
         $thursday = $today.AddDays($DayOfWeek[[System.DayOfWeek]::Thursday] - $DayOfWeek[$today.DayOfWeek])         
         Mock Get-Holidays {
            return @{ 
            }
         }

         Invoke-EventAnnoncements -Now $thursday.AddHours(16).AddMinutes(01) -Past $thursday.AddHours(15).AddMinutes(59) 

         Assert-VerifiableMocks
         Assert-MockCalled _Invoke-Speak -Exactly -Times 0 -Scope It -ParameterFilter { $message -eq "It is beer time, please proceed to Serraview fridge for a refreshing beverage." }
         Assert-MockCalled _Invoke-Mp3 -Exactly -Times 0 -Scope It -ParameterFilter { $Path -match "BeerTime.mp3" }
     } 

     It "will annonce beer time when its last day" { 
         $today = [DateTime]::Today
         $friday = $today.AddDays($DayOfWeek[[System.DayOfWeek]::Friday] - $DayOfWeek[$today.DayOfWeek])         
         $thursday = $today.AddDays($DayOfWeek[[System.DayOfWeek]::Thursday] - $DayOfWeek[$today.DayOfWeek])         
         $script:holiday = $friday
         Mock Get-Holidays {
            return @{ 
              $script:holiday = "Some holiday" 
            }
         }

         Invoke-EventAnnoncements -Now $thursday.AddHours(16).AddMinutes(01) -Past $thursday.AddHours(15).AddMinutes(59) 

         Assert-VerifiableMocks
         Assert-MockCalled _Invoke-Speak -Exactly -Times 1 -Scope It -ParameterFilter { $message -eq "It is beer time, please proceed to Serraview fridge for a refreshing beverage." }
         Assert-MockCalled _Invoke-Mp3 -Exactly -Times 1 -Scope It -ParameterFilter { $Path -match "BeerTime.mp3" }
     } 
   }
}
