﻿$moduleName = "CIMonitor"
$here = Split-Path -Parent $MyInvocation.MyCommand.Path
$modulePath = (Resolve-Path "$here\..\$moduleName.psd1").ProviderPath
Import-Module $modulePath -Force

InModuleScope CIMonitor{
    Describe "Invoke-CheckBuilds" -Tags "SkipAppVeyor" {
        It "runs without timer"{
            Invoke-CheckBuilds
        }
    }


    Describe "Jenkin Service Audio Enabled" -Tags 'Audio' {        
        $testFolder = Join-Path $Script:CIMonitorRoot "Tests\JenkinService"        
        
        Mock Get-LogPath { 
            return Join-Path $TestDrive "test.log"
        }

        Mock Get-AllJobs { 
            $testObj = Get-Content (Join-Path $testFolder "Get-AllJobs.json") -Raw | ConvertFrom-Json 
            return $testObj.jobs
        } 
            
        Mock Get-Job {
            param(
                $job 
            )

            $testObj = Get-Content (Join-Path $testFolder "$($job.name).json") -Raw | ConvertFrom-Json
            return $testObj
        } 
            
        Mock Get-LastCompletedBuild {
            param(
                $job
            )

            $testObj = Get-Content (Join-Path (Join-Path $testFolder "Builds") "$($job.name).json") -Raw | ConvertFrom-Json
            return $testObj
        }   


        It "play alert sound before speaking build annoncements"{
            #{ Reset-BuildAnnoncements; Invoke-SpeakBuildAnnoncements  } | should not throw
        }
    }


   Describe "_Invoke-Mp3" {
      Mock Add-Log { }
      It "calls Invoke-Mp3 and logs to file" {
         Mock Invoke-Mp3 { return $true }
         _Invoke-Mp3 -Path "$TestDrive\Song.mp3"

         Assert-MockCalled Invoke-Mp3 -Times 1 -Exactly -Scope It { $Path -eq "$TestDrive\Song.mp3" }
         Assert-MockCalled Add-Log -Times 1 -Exactly -Scope It { $Message -match "Playing Mp3: {$([Regex]::Escape("$TestDrive\Song.mp3"))}" } 
      } 

      It "calls Invoke-Mp3 and logs to file when failed" {
         Mock Invoke-Mp3 { return $false }
         _Invoke-Mp3 -Path "$TestDrive\Song.mp3"

         Assert-MockCalled Invoke-Mp3 -Times 1 -Exactly -Scope It { $Path -eq "$TestDrive\Song.mp3" }
         Assert-MockCalled Add-Log -Times 1 -Exactly -Scope It { $Message -match "Cannot locate Mp3: {$([Regex]::Escape("$TestDrive\Song.mp3"))}" } 
      } 
   }

    Describe "Get-BuildAnnoncements" {

      It "will not run job that is not in focus list" {
         $script:SeenBuilds = $null
         $script:IsWorking = $false
         Mock Get-AllJobs { return @(New-Object psobject -Property @{ name = "abc" }) } -Verifiable
         Mock Get-Job { }
         Mock Get-JobAnnoncement { }

         Get-BuildAnnoncements -FocusJobsOnly 

         Assert-VerifiableMocks
         
         Assert-MockCalled Get-AllJobs -Exactly -Times 1 -Scope It
         Assert-MockCalled Get-Job -Exactly -Times 0 -Scope It
         Assert-MockCalled Get-JobAnnoncement -Exactly -Times 0 -Scope It

      }

      It "will run job that is in focus list" {
         $script:SeenBuilds = $null
         $script:IsWorking = $false
         Mock Get-AllJobs { return @(New-Object psobject -Property @{ name = "Wayfinder - develop" }) } -Verifiable
         Mock Get-Job { return New-Object psobject -Property @{ 
                displayName = "Wayfinder - develop"
                lastCompletedBuild = New-Object psobject @{ 
                    url = "some url"
                }
            } } -Verifiable
         Mock Get-JobAnnoncement { }

         Get-BuildAnnoncements -FocusJobsOnly 

         $script:SeenBuilds.ContainsKey("Wayfinder - develop") | should be $true

         Assert-VerifiableMocks         
         Assert-MockCalled Get-AllJobs -Exactly -Times 1 -Scope It
         Assert-MockCalled Get-Job -Exactly -Times 1 -Scope It
         Assert-MockCalled Get-JobAnnoncement -Exactly -Times 1 -Scope It
      }


    }

    Describe "Jenkin Service"{
    
        $testFolder = Join-Path $Script:CIMonitorRoot "Tests\JenkinService"

        Mock Get-AllJobs { 
            $testObj = Get-Content (Join-Path $testFolder "Get-AllJobs.json") -Raw | ConvertFrom-Json 
            return $testObj.jobs
        } 
            
        Mock Get-Job {
            param(
                $job 
            )

            $testObj = Get-Content (Join-Path $testFolder "$($job.name).json") -Raw | ConvertFrom-Json
            return $testObj
        } 
            
        Mock Get-LastCompletedBuild {
            param(
                $job
            )

            $testObj = Get-Content (Join-Path (Join-Path $testFolder "Builds") "$($job.name).json") -Raw | ConvertFrom-Json
            return $testObj
        }   

        Mock Get-LogPath { 
            return Join-Path $TestDrive "test.log"
        }

        

        Context "Get-RestService"{
            It "Get-RestService"{
                Get-RestService "http://svbuild01:8080" | should beExactly "http://svbuild01:8080/api/json"
                Get-RestService "http://svbuild01:8080" -Query "pretty=true"| should beExactly "http://svbuild01:8080/api/json?pretty=true"
            }           
        }

        Context "Get-Json"{
            It "coverts json response"{
                $json = "{ ""colors"": [ {""name"":""Yellow""}, {""name"":""Red""} ]}"

                Mock Invoke-WebRequest { return $json } -ParameterFilter { $Uri -eq "someurl/api/json?somequery" -and $UseBasicParsing -eq $true }
                $result = Get-Json "someurl" "somequery"
                $result.colors[0].name | should be "Yellow"
                $result.colors[1].name | should be "Red"
                Assert-VerifiableMocks
                Assert-MockCalled Invoke-WebRequest -Times 1 -ParameterFilter { $Uri -eq "someurl/api/json?somequery" -and $UseBasicParsing -eq $true }
            }

            It "will write error when error occurred"{
                Mock Invoke-WebRequest { throw "Booooo!" }
                Mock Write-Error {} -ParameterFilter { $Message -eq "Error processing {someotherurl/api/json?boo}`nBooooo!" }

                Get-Json "someotherurl" "boo"
                Assert-MockCalled Write-Error -Times 1 -ParameterFilter { $Message -eq "Error processing {someotherurl/api/json?boo}`nBooooo!" }
                Assert-VerifiableMocks
            }
        }

        Context "Invoke-SpeakBuildAnnoncements"{
            
            Mock -ModuleName CIMonitor Get-Mute { return $false }
            Mock -ModuleName CIMonitor Get-Volume { return 0.358 }
            Mock -ModuleName CIMonitor _Invoke-Mp3 {}
            It "can speak build annoncements"{
                $logPath = Join-Path $TestDrive "Speak.log"
                $output = Join-Path $TestDrive "Speak.wav"
                
                { Reset-BuildAnnoncements; Invoke-SpeakBuildAnnoncements -LogPath $logPath -OutputFile $output } | should not throw
                $logPath | should exist
                Assert-MockCalled _Invoke-Mp3 -Times 1
                <#
                Get-Content $logPath -Raw | should match @"
\[\d\d/\d\d/\d\d\d\d \d\d\:\d\d:\d\d] START of Invoke-SpeakBuildAnnoncements
\[\d\d/\d\d/\d\d\d\d \d\d\:\d\d:\d\d] START of Get-BuildAnnoncements
'Dashboards - master' failed [\d]+ minutes ago\.  Build was broken by Dan
'Serraview.Analytics - Master - Build' is fixed [\d]+ minutes ago\.  Thank you, Dan and Matt!
\[\d\d/\d\d/\d\d\d\d \d\d\:\d\d:\d\d] END of Get-BuildAnnoncements: took [\d]+\.[\d]+ second\(s\)
\[\d\d/\d\d/\d\d\d\d \d\d\:\d\d:\d\d] END of Invoke-SpeakBuildAnnoncements: took [\d]+\.[\d]+ second\(s\)
"@
#>
            }
            

            It "can skip speak first time"{
                $logPath = Join-Path $TestDrive "Speak.log"
                { Reset-BuildAnnoncements; Invoke-SpeakBuildAnnoncements -SkipSpeakFirstTime -LogPath $logPath} | should not throw   
                Assert-MockCalled _Invoke-Mp3 -Times 1             
            }    
            
            It "default log path"{
                { Reset-BuildAnnoncements; Invoke-SpeakBuildAnnoncements -SkipSpeakFirstTime } | should not throw     
                Assert-MockCalled _Invoke-Mp3 -Times 1           
            }       

            It "plays broken build alert"{
                { Reset-BuildAnnoncements; Invoke-SpeakBuildAnnoncements -SkipSpeakFirstTime } | should not throw     
                
                Assert-MockCalled _Invoke-Mp3 -Times 1  -ParameterFilter { $Path -eq (Get-BrokenBuildAlertPath) }
            }

            
            It "plays all builds found"{
                Mock _Invoke-Speak {}
                { Reset-BuildAnnoncements } | should not throw
                { Invoke-SpeakBuildAnnoncements -OutputFile $output } | should not throw    
                Assert-MockCalled _Invoke-Speak -Times 2
            }

            It "plays annoncement build alert"{
                
                Mock Get-BuildAnnoncements { return New-Object psobject -Property @{
                    Messages = @("world peace")
                    Results = @{$true = @("some successful job"); $false = @()}
                }}
                $output = Join-Path $TestDrive "Speak.wav"
                { Reset-BuildAnnoncements; Invoke-SpeakBuildAnnoncements -OutputFile $output } | should not throw     
                (Get-Item $output).Length | should beGreaterThan 0
                
                Assert-MockCalled _Invoke-Mp3 -Times 1  -ParameterFilter { $Path -eq (Get-BuildAnnoncementAlertPath) }
            }

            It "plays stage clear alert"{
                
                { Reset-BuildAnnoncements } | should not throw

                $Script:SeenBuilds = @{"some successful job" = "some url"}
                $Script:BuildResults = @{ $true = @("some successful job")}                
                $output = Join-Path $TestDrive "Speak.wav"
                {  Invoke-SpeakBuildAnnoncements -OutputFile $output } | should not throw     
                (Get-Item $output).Length | should beGreaterThan 0
                
                Assert-MockCalled _Invoke-Mp3 -Times 1  -ParameterFilter { $Path -eq (Get-BuildAnnoncementAlertPath) }
                Assert-MockCalled _Invoke-Mp3 -Times 1  -ParameterFilter { $Path -eq (Get-BuildAllSucceedAlertPath) }
            }


        }        
    }    
}
