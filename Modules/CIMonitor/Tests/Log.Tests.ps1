﻿$moduleName = "CIMonitor"
$here = Split-Path -Parent $MyInvocation.MyCommand.Path
$modulePath = (Resolve-Path "$here\..\$moduleName.psd1").ProviderPath
Import-Module $modulePath  -Force

InModuleScope CIMonitor {
   Describe "Log" {        
      It "creates directory before adding log" {
         Add-Log -LogPath "$TestDrive\Log\Log.log" -Message "yellow!"
         "$TestDrive\Log\Log.log" | should exist
         "$TestDrive\Log\Log.log" | should contain "yellow!" 
      }

      It "logs messages"{
      } 
      
      It "replace log messages"{
      }

      It "creates log directory if empty"{
      }

      It "logs empty or null messages"{
      }
   }
}
