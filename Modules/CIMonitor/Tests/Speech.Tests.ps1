﻿$moduleName = "CIMonitor"
$here = Split-Path -Parent $MyInvocation.MyCommand.Path
$modulePath = (Resolve-Path "$here\..\$moduleName.psd1").ProviderPath
Import-Module $modulePath -Force


InModuleScope CIMonitor {
   Describe "Speech"{
       Context "_Invoke-Speak"{
           It "can talk and log"{
               Mock -ModuleName CIMonitor Get-Mute { return $true }
               Mock -ModuleName CIMonitor Get-Volume { return 0.358 }
               Mock -ModuleName CIMonitor Get-LogPath { return Join-Path $TestDrive "Invoke-Speak.log" }
               $logPath = (Join-Path $TestDrive "Invoke-Speak.log")


               $outputFile = Join-Path $TestDrive "can talk and log.wav"
               _Invoke-Speak "I can talk" -OutputFile $outputFile
               $logPath | should exist
               Get-Content $logPath | should match "\[[\d]{4}-[\d]{2}-[\d]{2} [\d]{2}:[\d]{2}:[\d]{2}\s[\w]{3}\]\[\d\.[\d]{3}]\[Muted] I can talk"
               $outputFile | should exist
               (Get-Item $outputFile).Length | should be 83366
           }
       }

       Context "Get-InstalledVoices"{
           
           $result = Get-InstalledVoices
           $result | should not be $null
       }

   }
}
