$script:CKLAudioModuleRoot = Split-Path -Parent $MyInvocation.MyCommand.Path
Get-ChildItem (Join-Path $CKLAudioModuleRoot "Functions") -File -Recurse | Resolve-Path | ForEach-Object { . $_.ProviderPath }

