function Invoke-Mp3
{
    [CmdletBinding()]
    param(
       [Parameter(Mandatory=$true)]    
       [string] $Path
    )

    if(-not (Test-Path $Path))
    {
       return $false
    }

    $Path = (Resolve-Path $Path).ProviderPath

    Add-Type -AssemblyName presentationCore
    $filepath = $Path
    $wmplayer = New-Object System.Windows.Media.MediaPlayer
    $wmplayer.Open($filepath)
    Start-Sleep 2 # This allows the $wmplayer time to load the audio file
    $duration = $wmplayer.NaturalDuration.TimeSpan.TotalSeconds
    $wmplayer.Play()
    if($duration)
    {
        Start-Sleep $duration
    }
    $wmplayer.Stop()
    $wmplayer.Close()

    return $true
}

Export-ModuleMember -Function Invoke-Mp3
