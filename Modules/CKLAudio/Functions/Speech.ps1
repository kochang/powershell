﻿function Get-SpeechSynthesizer
{
    Add-Type -AssemblyName System.speech
    return New-Object System.Speech.Synthesis.SpeechSynthesizer
}

function Get-InstalledVoices
{
    $speak = Get-SpeechSynthesizer
    $voice = $speak.GetInstalledVoices() | where { $_.Enabled }

    return $voice.VoiceInfo
}

function Invoke-Speak
{
    param(
    [Parameter(ValueFromPipeline=$true)]
    [string]$Message,
    [string]$OutputFile
    )


    if(-not $Message -or $Message.Trim() -eq "")
    {
        return $false
    }

    $speak = Get-SpeechSynthesizer
        
    $speak.Rate = -2

    <#
    $voice = $speak.GetInstalledVoices().VoiceInfo | Where-Object {$_.Gender -eq "Female"} | Select -First 1 
    if($voice)
    {    
        $speak.SelectVoice($voice.Name)
    }
    #>    

    $speak.SelectVoiceByHints([System.Speech.Synthesis.VoiceGender]::Female, [System.Speech.Synthesis.VoiceAge]::Child)


    if($OutputFile)    
    {
        $speak.SetOutputToWaveFile($OutputFile)
    }
         
    $speak.Speak($Message)   
    $speak.Dispose()

    return $true
}

Export-ModuleMember -Function Invoke-Speak, Get-InstalledVoices
