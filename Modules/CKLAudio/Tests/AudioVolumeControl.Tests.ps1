﻿$moduleName = "CKLAudio"
$here = Split-Path -Parent $MyInvocation.MyCommand.Path
$modulePath = (Resolve-Path "$here\..\$moduleName.psm1").ProviderPath
Import-Module $modulePath -Force

Describe -Tag 'Audio' "AudioVolumeControl"{
    It "can tell volume"{
        Get-Volume | should not be $null
    }

    It "can set volume"{
        Set-Volume 0.5
        $result = Get-Volume 
        $result | should be "0.5"
    }

    It "can tell mute"{
        Get-Mute | should not be $null
    }

    It "can mute"{
        Set-Mute 
        Get-Mute | should be $true
    }

    It "can unmute"{
        Set-Mute -Unmute
        Get-Mute | should be $false
    }
}
