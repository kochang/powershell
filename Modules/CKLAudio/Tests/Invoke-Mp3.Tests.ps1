$moduleName = "CKLAudio"
$here = Split-Path -Parent $MyInvocation.MyCommand.Path
$modulePath = (Resolve-Path "$here\..\$moduleName.psm1").ProviderPath
Import-Module $modulePath -Force

Describe "Invoke-Mp3" {
   It "can play mp3" {
      Invoke-Mp3 "$here\AnnoncementAlert.mp3" | should be $true
   }

   It "will handle error gracefully" {
      Invoke-Mp3 "$here\What?.mp3" | should be $false
   }
}
