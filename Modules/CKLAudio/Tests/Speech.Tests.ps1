﻿$moduleName = "CKLAudio"
$here = Split-Path -Parent $MyInvocation.MyCommand.Path
$modulePath = (Resolve-Path "$here\..\$moduleName.psm1").ProviderPath
Import-Module $modulePath -Force

Describe "Invoke-Speak"{
     It "can talk"{
         Mock -ModuleName CKLAudio Get-Mute { return $true }

         $outputFile = Join-Path $TestDrive "can talk and log.wav"
         Invoke-Speak "I can talk" -OutputFile $outputFile
         $outputFile | should exist
         (Get-Item $outputFile).Length | should be 83366
     }
}

Describe "Get-InstalledVoices"{
   It "can get installed voices" {     
      $result = Get-InstalledVoices
      $result | should not be $null
   }
}

