$script:CKLProfileModuleRoot = Split-Path -Parent $MyInvocation.MyCommand.Path
Get-ChildItem (Join-Path $CKLProfileModuleRoot "Functions") -File -Recurse | Resolve-Path | ForEach-Object { . $_.ProviderPath }

Initialize-UserEnvironment
