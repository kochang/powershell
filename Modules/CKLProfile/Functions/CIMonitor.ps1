function Get-WallieCILog
{
   return Get-ChildItem "\\$Wallie\c$\CIMonitor\Log\"
}

function Enter-Wallie
{
   [CmdletBinding()]
   param()
   if(-not $Global:WallieSession -or $Global:WallieSession.Availability -ne "Available")
   {
      $Global:WallieSession = New-PSSession -ComputerName svws023 -SessionOption (New-PSSessionOption -Debug -IdleTimeout ([int32]::MaxValue))
   }
   $Global:Host.UI.RawUI.BackgroundColor = [System.ConsoleColor]::DarkRed
   Enter-PSSession $Global:WallieSession
}

Export-ModuleMember -Function Get-WallieCILog, Enter-Wallie
