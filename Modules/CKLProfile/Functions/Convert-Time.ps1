
function Convert-Time
{
   [CmdletBinding()]
   param(
      [DateTime]$Time,
      [ValidateSet('Eastern Standard Time', 'AUS Eastern Standard Time')]
      [string]$SourceTimeZoneID,
      [ValidateSet('Eastern Standard Time', 'AUS Eastern Standard Time')]
      [string]$ToTimeZoneID
   )
   return [System.TimeZoneInfo]::ConvertTimeBySystemTimeZoneId($Time, $SourceTimeZoneID, $ToTimeZoneID)
}

Export-ModuleMember -Function Convert-Time
