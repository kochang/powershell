
function Find-ChildProcesses
{
  [CmdletBinding()]
  Param(
     [int]$ID = $PID,
     
     [Object[]]$Property = @('ProcessID'),

     [switch]$Recurse
     )

 

  $children = @(Get-WmiObject -Class Win32_Process -Filter "ParentProcessID=$($ID)" | Select-Object -Property $Property)
  
  if($Recurse)
  {
    $children += @($children | Where-Object {$_.ProcessID -ne $null } | ForEach-Object {Find-ChildProcesses -ID $_.ProcessID})
  }
  

  return $children
}

Export-ModuleMember -Function Find-ChildProcesses
