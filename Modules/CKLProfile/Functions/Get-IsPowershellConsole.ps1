
function Get-IsPowershellConsole
{
   if($Host.Name -eq "ConsoleHost")
   {
      return $True
   }
   return $False
}

Export-ModuleMember -Function Get-IsPowershellConsole
