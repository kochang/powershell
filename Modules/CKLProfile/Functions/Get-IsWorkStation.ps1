function Get-IsWorkStation
{
   if($env:USERDOMAIN -eq "Melbourne")
   {
      return $true
   }
   return $false
}

Export-ModuleMember -Function Get-IsWorkStation
