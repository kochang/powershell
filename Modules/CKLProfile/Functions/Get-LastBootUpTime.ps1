
function Get-LastBootUpTime
{
   return Get-WmiObject win32_operatingsystem | select csname, @{LABEL=’LastBootUpTime’;EXPRESSION={$_.ConverttoDateTime($_.lastbootuptime)}}
}

Export-ModuleMember -Function Get-LastBootUpTime
