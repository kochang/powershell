
function Get-NewYorkTimeNow
{
   return [System.TimeZoneinfo]::ConvertTime([DateTime]::UtcNow,[System.TimeZoneInfo]::FindSystemTimeZoneById('Eastern Standard Time'))
}

Export-ModuleMember -Function Get-NewYorkTimeNow
