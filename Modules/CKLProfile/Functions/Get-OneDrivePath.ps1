
function Get-OneDrivePath
{
   $path = "hkcu:\Software\Microsoft\Onedrive" 
   if(Test-Path $path)
   { 
      return (Get-ItemProperty -Path $path -Name UserFolder).UserFolder
   }
   return $null
}

Export-ModuleMember -Function Get-OneDrivePath
