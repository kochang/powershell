function Send-GmailMessage
{
  param
   (
      [string]$Subject,
      [string]$Body,
      [String[]]$To = "Ko-Lin Human <kochang.chu@gmail.com>",
      [string]$SmtpServer = "smtp.gmail.com",
      [string]$From = "Ko-Lin Chang <kochang.chu@gmail.com>",
      [string[]]$Attachments,
      [string[]]$Bcc = "Ko Bot <kochang.chu@gmail.com>",
      [switch]$BodyAsHtml, 
      [string[]]$Cc,
      [pscredential]$Credential,
      [System.Net.Mail.DeliveryNotificationOptions]$DeliveryNotificationOption = @("OnSuccess", "OnFailure", "Delay"),
      [System.Text.Encoding]$Encoding,
      [Int32]$Port = 587,
      [System.Net.Mail.MailPriority]$Priority,
      [switch]$UseSsl = $true
    )
    <#
    -DeliveryNotificationOption<DeliveryNotificationOptions>
Specifies the delivery notification options for the email message. You can specify multiple values. None is the default value. The alias for this parameter is dno.
The delivery notifications are sent in an email message to the address specified in the value of the To parameter. The acceptable values for this parameter are:

-- None. No notification.
-- OnSuccess. Notify if the delivery is successful.
-- OnFailure. Notify if the delivery is unsuccessful.
-- Delay. Notify if the delivery is delayed.
-- Never. Never notify.

    -Encoding<Encoding>
Specifies the encoding used for the body and subject. The acceptable values for this parameter are:

-- ASCII
-- UTF8
-- UTF7
-- UTF32
-- Unicode
-- BigEndianUnicode
-- Default
-- OEM
ASCII is the default.
    #>

    $null = $PSBoundParameters.Remove("SmtpServer") 
    $null = $PSBoundParameters.Remove("From")
    $null = $PSBoundParameters.Remove("UseSsl")    
    $null = $PSBoundParameters.Remove("To")
    $null = $PSBoundParameters.Remove("Credential")
    $null = $PSBoundParameters.Remove("DeliveryNotificationOption")
    $null = $PSBoundParameters.Remove("Port")
    $null = $PSBoundParameters.Remove("Bcc")

    $Credential = New-Object pscredential("kochang.chu@gmail.com", ("kzktgofauaihaaxa" | ConvertTo-SecureString -AsPlainText -Force)) 

    Send-MailMessage @PSBoundParameters -From $From -SmtpServer $SmtpServer -UseSsl:$UseSsl -To $To -Credential $Credential -DeliveryNotificationOption $DeliveryNotificationOption -Port $Port -Bcc $Bcc
}


function Search-Google
{
    Add-Type -AssemblyName "System.Web"

    $queryBase = "https://www.google.com/search?q="
    $queryString = $args.ForEach({ [System.Web.HttpUtility]::UrlEncode($_) }) -join '+'

    start "$queryBase$queryString"

}


Set-Alias g    Search-Google
Set-Alias sm   Send-GmailMessage
Export-ModuleMember -Function Send-GmailMessage, Search-Google -Alias g, sm
