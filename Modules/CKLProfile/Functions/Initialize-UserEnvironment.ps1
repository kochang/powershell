function Initialize-UserEnvironment
{
   $Global:ErrorActionPreference = "Stop"
   $Global:InformationActionPreference = "Continue" 

   $Script:PSEmailServer = "smtp.gmail.com" 
   $Script:BuildServer = "CHQOPSBLD01"
   $Script:Wallie = "SVWS023"
   Export-ModuleMember -Variable PSEmailServer, BuildServer, Wallie

# Increase Buffer Size
   $b = $Host.ui.RawUI.BufferSize
   $b.Height = 9999
   $Host.ui.RawUI.BufferSize = $b


# Alias

   Set-AliasIfExists tg       "$($env:ProgramFiles)\TortoiseGit\bin\TortoiseGitProc.exe"
   Set-AliasIfExists VSCode   "$(${env:ProgramFiles(x86)})\Microsoft VS Code\Code.exe"
   Set-AliasIfExists git      "$($env:USERPROFILE)\AppData\Local\Programs\Git\bin\git.exe"    
   Set-AliasIfExists zip      "$($env:ProgramFiles)\7-Zip\7z.exe"



   if(Get-IsWorkStation)
   {
      # Go to ProdOps
      $Script:ProdOpsPath = "C:\serraview\ProdOps" 
      cd $Script:ProdOpsPath
      $Global:VerbosePreference = "Silent"
      $Global:DebugPreference = "Silent" 
      & "$Script:ProdOpsPath\Set-ModulePath.ps1"
      $Global:VerbosePreference = "Continue"
      $Global:DebugPreference = "Continue" 
      Export-ModuleMember -Variable ProdOpsPath
   }
   else
   {
      @($env:ProgramFiles, ${env:ProgramFiles(x86)}).ForEach({
         if(Test-Path "$_\Kodi\Kodi.exe")
         {
            Set-AliasIfExists kodi      "$_\Kodi\Kodi.exe"
            return # given this is a piped foreach
         }
      })
      
   }

   if($oneDrive = (Get-OneDrivePath))
   {
      $Script:OneDrive = $oneDrive
      Export-ModuleMember -Variable OneDrive
# Import Modules
      Push-ModulePathIfExistAndNotAdded "$Script:OneDrive\Code\Powershell\Modules" 
   }

   Sync-Profile
   $Global:ErrorActionPreference = "Continue"


#Web Bookmarks
   $Script:Urls = New-Object -TypeName PSObject -Property @{
         Home = "http://google.com"
         OneNote = New-Object -TypeName psobject -Property @{
           ProdOps = "https://onedrive.live.com/edit.aspx?resid=BB736AB42E76AAF4!563&cid=bb736ab42e76aaf4&app=OneNote"
         }
         Hipchat = "https://serraview.hipchat.com/chat/room/2637626"
         Jenkins = "http://svbuild01:8080/view/Prod%20Ops/job/ProdOps%20-%20Dev/lastBuild/"
         AppVeyor = "https://ci.appveyor.com/project/kolinchang/powershell"
         Nuget = "http://svdev01:8181/feeds/Default/Serraview.ProdOps.OctopusDeploy"
         Octopus = New-Object -TypeName PSObject -Property @{
            Puppy = "https://octopuppy.dev.serraview.com"
            Dev = "http://octopus.dev.serraview.com"
         } 
         Jiras = New-Object -TypeName PSObject -Property @{
           SyncFilesUpgradeBackupPurgeRestoreInstance = "https://serraview.atlassian.net/browse/PRODOPS-1253"                
           SyncFilesUpgradeUpdatePS1 = "https://serraview.atlassian.net/browse/PRODOPS-1256"        
           OctopusMigrateDeployFunctions = "https://serraview.atlassian.net/browse/PRODOPS-1349"
           OctopusCreateJenkinsBuild = "https://serraview.atlassian.net/browse/PRODOPS-1353"
         }
      }

   Export-ModuleMember -Variable Urls

}

function Prompt{
    $currentLocation = $executionContext.SessionState.Path.CurrentLocation
    $date = ([datetime]"2017-05-10")
    $countDown = ($date - ([DateTime]::Today))
    $countDownTitle = "Unemployment"
    $Host.UI.RawUI.WindowTitle = $currentLocation
    Write-Host ""
    Write-Host "[$(Get-Date -Format 'yyyy-MM-dd HH:mm')][$countDownTitle($($countDown.TotalDays)d)]"  -ForegroundColor Gray -BackgroundColor Black
    Write-Host "$currentLocation$('>'* ($nestedPromptLevel + 1))"  -ForegroundColor Green -BackgroundColor Black
    return "It's an honor to serve you, Ko: ";    
}


Export-ModuleMember -Function Initialize-UserEnvironment, Prompt 

