function Invoke-PesterFailedOnly 
{
    [CmdletBinding(DefaultParameterSetName = 'LegacyOutputXml')]
    param(
        [Parameter(Position=0,Mandatory=0,ValueFromPipeline=$true)]
        [Alias('Path', 'relative_path')]
        [object[]]$Script = '.',

        [Parameter(Position=1,Mandatory=0)]
        [Alias("Name")]
        [string[]]$TestName,

        [Parameter(Position=2,Mandatory=0)]
        [switch]$EnableExit,

        [Parameter(Position=3,Mandatory=0, ParameterSetName = 'LegacyOutputXml')]
        [string]$OutputXml,

        [Parameter(Position=4,Mandatory=0)]
        [Alias('Tags')]
        [string[]]$Tag,

        [string[]]$ExcludeTag,

        [switch]$PassThru,

        [object[]] $CodeCoverage = @(),

        [Switch]$Strict,

        [Parameter(Mandatory = $true, ParameterSetName = 'NewOutputSet')]
        [string] $OutputFile,

        [Parameter(ParameterSetName = 'NewOutputSet')]
        [ValidateSet('LegacyNUnitXml', 'NUnitXml')]
        [string] $OutputFormat = 'NUnitXml',

        [Switch]$Quiet,

        [object]$PesterOption
    )


    $global:PesterTestResults = @()
    if($null -eq (Get-Module Test-Utilities -ErrorAction Ignore) -and (Test-Path "$ProdOpsPath\Tests\Modules\Test-Utilities"))
    {
        Import-Module "$ProdOpsPath\Tests\Modules\Test-Utilities" 
        $ExcludeTag = (Get-ExcludeTagsForDeveloperMachine)
    }
    
    $null = $PSBoundParameters.Remove("PassThru")
    $null = $PSBoundParameters.Remove("ExcludeTag")
    $result = Invoke-Pester @PSBoundParameters -PassThru -ExcludeTag $ExcludeTag
    if($result.FailedCount -eq 0)
    {
       Write-Host "All good!"
       return
    }
    
    Write-Host "`a{$($result.FailedCount)} Test(s) failed!"
    foreach($r in $result.TestResult)
    {
        if($r.Result -eq "Passed")
        {
            continue
        }

        $global:PesterTestResults += $r
    }

     $e = $result.CodeCoverage.NumberOfCommandsExecuted
     $a = $result.CodeCoverage.NumberOfCommandsAnalyzed
     if($a)
     {
        $p = (($e / $a) * 100).ToString('0.00')
     }
     else 
     {
        $p = 0
     }
     $global:PesterCoverage = $p
     $global:PesterResultObject = $result 
     $global:PesterResultTime = $result.Time
}

Set-Alias ip   Invoke-PesterFailedOnly 
Export-ModuleMember -Function Invoke-PesterFailedOnly -Alias ip
