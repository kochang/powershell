
function Pop-Profile
{
   param( 
      [string]$FromLocation = "$OneDrive\Code\Powershell"
   )

   if(Test-Path $FromLocation)
   {
      $p = Get-Item $profile
      $bakFile = "$($FromLocation)\$($p.Name)"

      if(!(Test-Path $bakFile) -or $p.LastWriteTimeUtc -ge (Get-Item $bakFile).LastWriteTimeUtc)
      {
         return;
      }

      Copy $bakFile $Profile
      Write-Verbose "Profile pulled from '$($FromLocation)'"
   }
}

Export-ModuleMember -Function Pop-Profile
