
function Push-Desktop()
{ 
   Push-IfExistAndNotAtLocation("$Home\Desktop")
}


Set-Alias dt   Push-Desktop
Export-ModuleMember -Function Push-Desktop -Alias dt
