
function Push-IfExistAndNotAtLocation([string] $path)
{ 
   if((Test-Path $path) -eq $false)
   {
      return
   }

   if($MyInvocation.MyCommand.Path -eq $path)
   {
      return
   }
   Push-Location $path
}

Export-ModuleMember -Function Push-IfExistAndNotAtLocation
