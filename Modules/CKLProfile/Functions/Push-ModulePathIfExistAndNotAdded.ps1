
function Push-ModulePathIfExistAndNotAdded
{
   [CmdletBinding()]
   param(
   [Parameter(Mandatory=$true)]
   [string]$Path
   )

   END{
      if((Test-Path $Path) -and $ENV:PSModulePath -NotMatch [Regex]::Escape($Path))
      {
         $ENV:PSModulePath += ";" + $Path
         Write-Verbose "Path '$($Path)' added to module path."
      }
   }
}

Export-ModuleMember -Function Push-ModulePathIfExistAndNotAdded
