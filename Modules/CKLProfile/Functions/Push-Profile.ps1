
function Push-Profile
{
   param(
      [string]$ToLocation = "$OneDrive\Code\Powershell",
      [string]$Rename  
   )

   if(Test-Path $ToLocation)
   {
      $p = Get-Item $profile
      if($Rename)
      {
         $bakFile = Join-Path $ToLocation $Rename
      }
      else
      { 
         $bakFile = Join-Path $ToLocation $p.Name
      }

      if(!(Test-Path $bakFile) -or $p.LastWriteTimeUtc -gt (Get-Item $bakFile).LastWriteTimeUtc)
      {
         Copy $profile $bakFile
         Write-Verbose "Profile pushed to '$bakFile'"
      }
   }
}

Export-ModuleMember -Function Push-Profile
