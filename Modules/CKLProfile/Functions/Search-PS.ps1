function Search-PS
{
   [CmdletBinding()]
   param(
      [Parameter(ValueFromPipeline=$true, ValueFromPipelineByPropertyName=$true)]
      [string[]]$Pattern,
      [string[]]$Include = @("*.ps1", "*.psm1", "*.psd1", "*.ps1xml"),
      [Parameter(ValueFromPipelineByPropertyName=$true)]
      [string]$Path = (Get-Location)
   )

   BEGIN
   {
      $p = @()
      $i = @()
   }

   PROCESS
   {
      if($Pattern.Length -gt 0)
      {
         $p += $Pattern
      }
      $i += $Include
   }

   END
   {
      $Global:LastSearch = $Global:LastSearchFiles = ls -Path $Path -Recurse -Include $i -File
      if($p.Length -gt 0)
      {
         $Global:LastSearch = $Global:LastSearchFiles | Select-String -Pattern $p
         $Global:LastSearch | Format-Table -Property FileName, LineNumber, Line
         $Global:LastSearchFiles = $Global:LastSearch | Group-Object -Property Path | Select-Object -ExpandProperty Name
      }   
      else 
      { 
         $Global:LastSearchFiles
      }
    }
}

Set-Alias s  Search-PS
Export-ModuleMember -Function Search-PS -Alias s
