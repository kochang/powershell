
function Send-HipChatMessageToRoom
{ 
   [CmdletBinding()] 
   param (
      [Parameter(Mandatory=$true)]
      [string]$Message, 
      [string]$RoomId = "2637626"
      ) 
    try 
    {
        $obj = New-Object psobject -Property @{
        "message" = $Message
    }
     
    $url = "https://api.hipchat.com/v2/room/$RoomId/message?auth_token=HMPiFhfU87pA0aTjc9FJXQGis3hEgQMhM11ZxWum"
    
    $headers = @{"Content-Type" = "application/json"}
    $body = ConvertTo-Json $obj    

    Invoke-WebRequest -Headers $headers -METHOD POST -BODY $body -uri $url -UseBasicParsing | Out-Null        
    }
    catch
    {
        Write-Error $_.Exception
    }
}
Export-ModuleMember -Function Send-HipChatMessageToRoom
