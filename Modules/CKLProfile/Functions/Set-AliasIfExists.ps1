function Set-AliasIfExists
{
   param(
      [string]$Name,
      [string[]]$Path
   )

   foreach($p in $Path)
   {
      if(Test-Path $p)
      {
         Set-Alias -Name $Name -Value $p -Scope Script
         Export-ModuleMember -Alias $Name
         Write-Verbose "$Name is mapped via alias"
         break
      }
   }
}

Export-ModuleMember -Function Set-AliasIfExists
