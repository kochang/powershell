
function Set-EnvironmentPath
{
   param(
   [string]$Path,
   [string]$Message
   )

   if((Test-Path $Path) -and $ENV:Path -NotMatch [Regex]::Escape($Path))
   {
      $ENV:Path += ";" + $Path
      if($Message -ne [String]::Empty)
      {
         Write-Verbose "'$($Message)' included."
      }
      else
      {
         Write-Verbose "'$($Path)' included."
      }      
   }
}

Export-ModuleMember -Function Set-EnvironmentPath
