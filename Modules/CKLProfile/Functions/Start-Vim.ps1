
function Start-Vim
{
   [CmdletBinding()]
   param(
      [Parameter(ValueFromPipeline=$true)]
      [string[]]$Path
   )

   PROCESS
   {
      if($Path)
      {
         foreach($p in $Path)
         {
            Start-Process "$(${env:ProgramFiles(x86)})\Vim\vim74\vim.exe" """$p""" 
         }
      }
      else
      {
         Start-Process "$(${env:ProgramFiles(x86)})\Vim\vim74\vim.exe" 
      }
   }
}

Set-Alias vim  Start-Vim
Export-ModuleMember -Function Start-Vim -Alias vim
