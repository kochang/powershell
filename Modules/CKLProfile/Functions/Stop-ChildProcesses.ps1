
function Stop-ChildProcesses
{
   param(
   [int]$ID = $PID
   )

   process
   {
      Find-ChildProcesses -Recurse -ID $ID | ForEach-Object { Stop-Process $_.ProcessID }
   }
}
Export-ModuleMember -Function Stop-ChildProcesses
