
function Sync-Profile
{
   param(
      [string]$BackupLocation = "$OneDrive\Code\Powershell"
   )

   if($false -eq $(Get-IsPowershellConsole))
   { 
      return
   }

   # Sync to one drive
   Push-Profile $BackupLocation
   Pop-Profile $BackupLocation 
   #
   # Sync to Dropbox
   $dropbox = "D:\Dropbox\WindowsPowershell"
   Push-Profile $dropbox
   Pop-Profile $dropbox

   # Sync to VSCode
   $ProfileFolder = Split-Path -Parent $Profile
   Push-Profile $ProfileFolder -Rename "Microsoft.VSCode_profile.ps1"
   
}

Export-ModuleMember Sync-Profile
