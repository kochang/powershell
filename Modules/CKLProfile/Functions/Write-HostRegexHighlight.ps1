function Write-HostRegexHighlight 
{
   param(
      [Parameter(Mandatory=$true)]
      [string]$InputObject,

      [Parameter(Mandatory=$true)]
      [string]$Regex,

      [ConsoleColor]$Color = [ConsoleColor]::Red
   )

   $matches = Get-Matches -InputObject $InputObject -Regex $Regex 
   $ranges = @()
   
   foreach($m in $matches)
   { 
      $start = $m.Index
      $end = $m.Index + $m.Length

      $done = $false
      foreach($r in $ranges)
      {
         if($r.Start -le $start)
         {
            if($r.End -lt $end)
            {
               $r.End = $end
            }
            $done = $true
         }
      }

      if(-not $done)
      {
         $ranges += New-Object psobject -Property @{ 
            Start = $start
            End = $end
            }
      }
   }
   
   $ranges | Sort-Object -Property @{ Expression = { $_.Start }; Ascending = $true } | Set-Variable ranges
   $rIndex = 0;
   $range = $ranges[$rIndex]
   for($i = 0; $i -lt $InputObject.length; $i++)
   {
      $char = $InputObject[$i]
      if($range.Start -le $i -and $range.End -gt $i)
      {
         Write-Host -ForegroundColor $Color $char -NoNewLine
      }
      else 
      {
         if($i -eq $range.End)
         {
            if(($rIndex + 1) -lt $ranges.Length)
            {
               $range = $ranges[$rIndex+1]
            }
         }
         Write-Host $char -NoNewLine
      }
   }
}


function Get-Matches
{
   param(
      [Parameter(Mandatory=$true)]
      [string]$InputObject,

      [Parameter(Mandatory=$true)]
      [string]$Regex
   )

   $r = [Regex]$Regex 
   return $r.Matches($InputObject)
}

Export-ModuleMember Write-HostRegexHighlight 
