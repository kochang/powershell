<# 
https://developers.google.com/identity/protocols/OAuth2InstalledApp
#>
function Get-ApplicationToken
{
   param
   (
      [Parameter(Mandatory=$true)]
      [ValidateNotNull()]
      [string[]]$Scope,

      [ValidateNotNull()]
      [string]$ClientID = (Get-ClientID),

      [ValidateNotNull()]
      [string]$ClientSecret = (Get-ClientSecret),

      [ValidateNotNull()]
      [Uri]$RedirectUri = (Get-RedirectUri),

      [Parameter(Mandatory=$true)]
      [string]$UserID
   )
   Write-Verbose "Running $($MyInvocation.MyCommand)..."
   Write-Verbose "Loading access token in store ..."   
   $accessToken = Get-AccessTokenInStore -Scope $Scope -UserID $UserID
   if(-not $accessToken)
   {
       Write-Verbose "Loading refresh token in store ..."
       $refreshToken = Get-RefreshTokenInStore -Scope $Scope -UserID $UserID
       if(-not $refreshToken)
       { 
          Write-Verbose "Failed to load refresh token in store ..."
          Write-Verbose "Loading authentication token in store ..."
          <# 
             todo: need to find out why I can no longer cache auth code... it seems to change each time I send a request, according to 
             https://developers.google.com/identity/protocols/OAuth2
             I should be caching access token instead
          #>
          $authCode = Get-AuthenticationCode -Scope $Scope -RedirectUri $RedirectUri -ClientID $ClientID -UserID $UserID
          <#
          $authCode = Get-AuthenticationCodeInStore -UserID $UserID -Scope $Scope 
          if(-not $authCode)
          {
             Write-Verbose "Failed to load authentication token in store ..."
             $authCode = Get-AuthenticationCode -Scope $Scope -RedirectUri $RedirectUri -ClientID $ClientID -UserID $UserID
             Set-AuthenticationCodeInStore -Value $authCode -UserID $UserID -Scope $Scope
          } 
          #>

          $tokens = Get-AccessAndRefreshToken -AuthenticationCode $authCode -ClientID $ClientID -ClientSecret $ClientSecret -RedirectUri $RedirectUri -Scope $Scope
          $accessToken = $tokens.access_token

          if($tokens.refresh_token)
          {
             Set-RefreshTokenInStore -UserID $UserID -Scope $Scope -Value $tokens.refresh_token 
          }
          if($tokens.access_token)
          {
             Set-AccessTokenInStore -UserID $UserID -Scope $Scope -Value $tokens.access_token 
          }
       }
       else
       {
          $tokens = Get-AccessToken -RefreshToken $refreshToken -ClientID $ClientID -ClientSecret $ClientSecret -Scope $Scope
          $accessToken = $tokens.access_token
       } 
   }
   return $accessToken
}

function Revoke-Token
{
   param( 
      [Parameter(Mandatory=$true)]
      [ValidateNotNull()]
      $Token
   )

   Write-Verbose "Running $($MyInvocation.MyCommand)..."
   $response = Invoke-WebRequest -Uri "https://accounts.google.com/o/oauth2/revoke?token=$Token" -ContentType "application/x-www-form-urlencoded"
   Write-Verbose "Revoking token {$Token}"
   Write-Verbose $response

   return $response
}

function Get-AccessToken
{
   param(
      [Parameter(Mandatory=$true)]
      [ValidateNotNull()]
      [string]$RefreshToken,

      [Parameter(Mandatory=$true)]
      [ValidateNotNull()]
      [string]$ClientID,

      [Parameter(Mandatory=$true)]
      [ValidateNotNull()]
      [string]$ClientSecret,

      [Parameter(Mandatory=$true)]
      [ValidateNotNull()]
      [string[]]$Scope 
   )

### exchange the refresh token for an access token
    Write-Verbose "Running $($MyInvocation.MyCommand)..."
    $requestUri = "https://www.googleapis.com/oauth2/v4/token"
    $grantType = "refresh_token"
    $encodedScope = $Scope -join ' '
    $requestBody = "refresh_token=$($RefreshToken)&client_id=$($ClientID)&client_secret=$($ClientSecret)&scope=$encodedScope&grant_type=$grantType" 
    Write-Verbose "Get-AccessToken: body {$requestBody} "
    $response = Invoke-RestMethod -Method Post -Uri $requestUri -ContentType "application/x-www-form-urlencoded" -Body $requestBody 

    Write-Verbose $response
    Write-Verbose "Access token acquired: {$($response.access_token)}" 
 

    return $response
}

function Get-AccessAndRefreshToken
{
   param(
      [Parameter(Mandatory=$true)]
      [ValidateNotNull()]
      [string]$AuthenticationCode,

      [Parameter(Mandatory=$true)]
      [ValidateNotNull()]
      [string]$ClientID,

      [Parameter(Mandatory=$true)]
      [ValidateNotNull()]
      [string]$ClientSecret,

      [Parameter(Mandatory=$true)]
      [ValidateNotNull()]
      [string]$RedirectUri,

      [Parameter(Mandatory=$true)]
      [ValidateNotNull()]
      [string[]]$Scope 
   )

### exchange the refresh token for an access token

    Write-Verbose "Running $($MyInvocation.MyCommand)..."
    $grantType = "authorization_code"
    $requestUri = "https://www.googleapis.com/oauth2/v4/token"
    $encodedScope = $Scope -join ' '
    $requestBody = "code=$($AuthenticationCode)&redirect_uri=$($RedirectUri)&client_id=$($ClientID)&client_secret=$($ClientSecret)&scope=$encodedScope&grant_type=$grantType" 
 
    $response = Invoke-RestMethod -Method Post -Uri $requestUri -ContentType "application/x-www-form-urlencoded" -Body $requestBody 
 
    Write-Verbose $response
    Write-Verbose "Access token acquired: {$($response.access_token)}"
    Write-Verbose "Refresh token acquired: {$($response.refresh_token)}" 

    return $response
}


function Get-AuthenticationCode
{
   param 
   ( 
      [Parameter(Mandatory=$true)]
      [ValidateNotNull()]
      [string[]]$Scope,

      [Parameter(Mandatory=$true)]
      [ValidateNotNull()]
      [Uri]$RedirectUri,
      
      [Parameter(Mandatory=$true)]
      [ValidateNotNull()]
      [string]$ClientID,
      [Parameter(Mandatory=$true)]
      [ValidateNotNull()]
      [string]$UserID,

      [ValidateNotNull()]
      [int]$TimeOutInSeconds = 120
   )
   Write-Verbose "Running $($MyInvocation.MyCommand)..."
   $requestUri = �https://accounts.google.com/o/oauth2/token&#8221"
   $responseType = "code"
   $approvalPrompt = "force"
   $accessType = "offline"
   $scopeLine = $Scope -join ' '
   $encodedScope = $scopeLine

   $authRequestUri = "https://accounts.google.com/o/oauth2/v2/auth?scope=$encodedScope&response_type=$responseType&redirect_uri=$($RedirectUri.AbsoluteUri)&client_id=$ClientID&login_hint=$UserID&access_type=$accessType"

### Get the authorization code
   $ie = New-Object -comObject InternetExplorer.Application
   $ie.visible = $true
   $null = $ie.navigate($authRequestUri)

### Wait for user interaction in IE, manual approval

   $codeAcquired = $errorOccurred = $false
   $location = ""
   $startTime = [DateTime]::Now
   $timedOut = $false
   do 
   {
      Start-Sleep 1
      $location = $ie.LocationURL
      $codeAcquired = $ie.LocationURL -match 'code=([^&]*)'
      $errorOccurred = $ie.LocationURL -match 'error=([^&]*)'
      Write-Verbose "will time out in {$(($startTime.AddSeconds($TimeOutInSeconds) - [DateTime]::Now).TotalSeconds.ToString("00.0"))} second(s)."
      $timedOut = ([DateTime]::Now - $startTime).TotalSeconds -gt $TimeOutInSeconds
         
   } until($codeAcquired -or $errorOccurred -or $timedOut)

   $null = $ie.Quit()
   if($codeAcquired)
   {
      $authorizationCode = $matches[1]
      Write-Verbose "Authorization Code acquired: {$authorizationCode}"
      return $authorizationCode 
   }
   else
   {
      if($timedOut)
      {
         throw "Authentication Code awaiting user interaction timed out"
      }
      throw $matches[1]
   } 
}
Export-ModuleMember -Function Revoke-Token
