
$convertTime = { 
   param(
      [DateTime]$Time
   ) 

   return [Uri]::EscapeDataString("$($Time.ToString("yyyy-MM-dd"))T00:00:00+11:00") 
}


function Move-CalendarEventToALaterTime
{
   param(
         [Parameter(Mandatory=$true)]
         [ValidateNotNull()]
         [string]$CalendarID,
         [Parameter(Mandatory=$true)]
         [ValidateNotNull()]
         [string]$UserID,
         [Parameter(Mandatory=$true)]
         [ValidateNotNull()]
         [psobject]$Event,
         [Parameter(Mandatory=$true)]
         [TimeSpan]$ByTimePeriod 
      )
      #https://developers.google.com/calendar/v3/reference/events/update
      $Scope = "https://www.googleapis.com/auth/calendar" 
      $uri = "https://www.googleapis.com/calendar/v3/calendars/$CalendarID/events/$($Event.id)"

      $clone = New-Object psobject -Property @{ summary = $Event.summary }

      if($Event.start.datetime)
      {        
         $clone | Add-Member NoteProperty start (New-Object psobject -Property @{dateTime = ([DateTime]$Event.start.datetime).Add($ByTimePeriod).ToString("o") })
         
      }
     
      if($Event.start.date)
      {        
         $clone | Add-Member NoteProperty start (New-Object psobject -Property @{date = ([DateTime]$Event.start.date).Add($ByTimePeriod).ToString("yyyy-MM-dd") })
         
      }  

      if($Event.end.datetime)
      {

         $clone | Add-Member NoteProperty end (New-Object psobject -Property @{dateTime = ([DateTime]$Event.end.datetime).Add($ByTimePeriod).ToString("o") })
          
      }
      if($Event.end.date)
      {

         $clone | Add-Member NoteProperty end (New-Object psobject -Property @{date = ([DateTime]$Event.end.date).Add($ByTimePeriod).ToString("yyyy-MM-dd") })
          
      }

      $body = ConvertTo-Json -InputObject $clone
      
      $token = Get-ApplicationToken -Scope $Scope -UserID $UserID
      $result = Invoke-RestMethod -Method Put -Uri $uri -Headers @{"Authorization"="Bearer $token"} -Body $body -ContentType "application/json"
}

Export-ModuleMember -Function Move-CalendarEventToALaterTime

function Get-CalendarEventsBySearchText
{
   param(
      [Parameter(Mandatory=$true)]
      [ValidateNotNull()]
      [string]$CalendarID,
      [Parameter(Mandatory=$true)]
      [ValidateNotNull()]
      [string]$UserID,
      [Parameter(Mandatory=$true)]
      [ValidateNotNull()]
      [string]$SearchText,
      [DateTime]$TimeMin = [DateTime]::MinValue, 
      [DateTime]$TimeMax = [DateTime]::MaxValue
   )
   $Scope = "https://www.googleapis.com/auth/calendar.readonly" 
   $safeSearch = [Uri]::EscapeDataString($SearchText)

   $uri = "https://www.googleapis.com/calendar/v3/calendars/$CalendarID/events?q=$safeSearch" 

   if($TimeMin -gt $TimeMax)
   {
      throw '$TimeMin cannot be greater than $TimeMax'
   }

   if($TimeMin -ne [DateTime]::MinValue)
   {
      $safeTime = & $convertTime -Time $TimeMin
      $uri += "&timeMin=$safeTime"
   }


   if($TimeMax -ne [DateTime]::MaxValue)
   {
      $safeTime = & $convertTime -Time $TimeMax
      $uri += "&timeMax=$safeTime"
   }
   


   $token = Get-ApplicationToken -Scope $Scope -UserID $UserID
   $result = Invoke-WebRequest -Method Get -Uri $uri -Headers @{"Authorization"="Bearer $token"}

   return $result.Content | ConvertFrom-Json
}
Export-ModuleMember -Function Get-CalendarEventsBySearchText
function Get-CalendarEventsToday 
{
   param(
      [Parameter(Mandatory=$true)]
      [ValidateNotNull()]
      [string]$CalendarID,
      [Parameter(Mandatory=$true)]
      [ValidateNotNull()]
      [string]$UserID
   )
   $Scope = "https://www.googleapis.com/auth/calendar.readonly" 

   $today = [DateTime]::Today
   $tomorrow = $today.AddDays(1)


   $timeMin = & $convertTime -Time $today
   $timeMax = & $convertTime -Time $tomorrow 

   $uri = "https://www.googleapis.com/calendar/v3/calendars/$CalendarID/events?timeMin=$($timeMin)&timeMax=$($timeMax)"
   $token = Get-ApplicationToken -Scope $Scope -UserID $UserID
   $result = Invoke-WebRequest -Method Get -Uri $uri -Headers @{"Authorization"="Bearer $token"}

   return $result.Content | ConvertFrom-Json
}

Export-ModuleMember -Function Get-CalendarEventsToday
