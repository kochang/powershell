function Get-ClientID 
{
   return "847922212648-7vi77ajqb6avi4lsioj33amrn4qd6jk0.apps.googleusercontent.com"
}

function Get-ClientSecret
{
   return "NQAuXuJ4lg1baSUVkFbHZbFO" 
}

function Get-RedirectUri
{
   return [Uri]�http://kolinchang.com/gauth"    
}

function Import-GoogleClientDetails
{
   param(
      [ValidateNotNull()]
      [Parameter(Mandatory=$true)]
      [string]$Path
   )

   return Get-Content $Path -Raw | ConvertFrom-Json
}

function Export-GoogleClientDetails
{
   param(
      [Parameter(Mandatory=$true)]
      [ValidateNotNull()]
      [string]$ClientID,

      [ValidateNotNull()]
      [Parameter(Mandatory=$true)]
      [string]$ClientSecret,

      [ValidateNotNull()]
      [Parameter(Mandatory=$true)]
      [string]$RedirectUri,

      [ValidateNotNull()]
      [Parameter(Mandatory=$true)]
      [string]$Path

   )

   $obj = New-Object psobject -Property @{
         ClientID = $ClientID
         ClientSecret = $ClientSecret
         RedirectUri = $RedirectUri
      } 

   $obj | ConvertTo-Json | Out-File $Path
}

function Get-CodeInStore
{
   param(
      [string]$StorePath
   )
   if(Test-Path $StorePath)
   {
      $obj = Get-Content $StorePath -Raw | ConvertFrom-Json
      $Value = $obj.Value
   }
   else
   {
      $Value = $null
   }

   return $Value
}

function Set-CodeInStore
{
   param(
      [parameter(Mandatory=$true)]
      [string]$StorePath,
      [parameter(Mandatory=$true)]
      [string[]]$Scope,
      [string]$Value
   )

   if(-not $Value)
   {
      if(Test-Path $StorePath)
      {
         Remove-Item $StorePath
      }
   }
   else
   {
      $dir = Split-Path $StorePath
      if(-not (Test-Path $dir))
      {
         $null = New-Item -ItemType Directory $dir
      }
      $content = New-Object psobject -Property @{
            Scope = ($Scope -join ' ')
            Value = $Value
         } | ConvertTo-Json
      Out-File -InputObject $content -FilePath $StorePath
   }
}

function Get-UserScopeStorePath
{
   param(
      [Parameter(Mandatory=$true)]
      [string]$UserID,

      [Parameter(Mandatory=$true)]
      [string[]]$Scope
   )

   $joinScope = ((Sort-Object -InputObject $Scope) -join ' ').GetHashCode()
   return "$script:StorePath\$UserID\$joinScope"
}

function Get-RefreshTokenStorePath
{
   param(
      [Parameter(Mandatory=$true)]
      [string]$UserID,

      [Parameter(Mandatory=$true)]
      [string[]]$Scope
   )
   return Join-Path (Get-UserScopeStorePath @PSBoundParameters) "RefreshToken.txt" 
}

function Get-AccessTokenStorePath
{
   param(
      [Parameter(Mandatory=$true)]
      [string]$UserID,

      [Parameter(Mandatory=$true)]
      [string[]]$Scope
   )
   return Join-Path (Get-UserScopeStorePath @PSBoundParameters) "AccessToken.txt" 
}


function Set-RefreshTokenInStore
{
   param
   (
      [Parameter(Mandatory=$true)]
      [string]$UserID,

      [Parameter(Mandatory=$true)]
      [string[]]$Scope,

      [string]$Value
   )

   $store = (Get-RefreshTokenStorePath -UserID $UserID -Scope $Scope)
   Set-CodeInStore -StorePath $store -Value $Value -Scope $Scope
   Write-Verbose "Setting refresh token for scope {$Scope} in store {$store}: {$Value}"
}

function Set-AccessTokenInStore
{
   param
   (
      [Parameter(Mandatory=$true)]
      [string]$UserID,

      [Parameter(Mandatory=$true)]
      [string[]]$Scope,

      [string]$Value
   )

   $store = (Get-AccessTokenStorePath -UserID $UserID -Scope $Scope)
   Set-CodeInStore -StorePath $store -Value $Value -Scope $Scope
   Write-Verbose "Setting access token for scope {$Scope} in store {$store}: {$Value}"
}

function Get-AccessTokenInStore
{ 
   param(
      [Parameter(Mandatory=$true)]
      [string]$UserID,

      [Parameter(Mandatory=$true)]
      [string[]]$Scope
   )
   $store = (Get-AccessTokenStorePath -UserID $UserID -Scope $Scope)
   $value = Get-CodeInStore -StorePath $store 

   Write-Verbose "Getting access token for scope {$Scope }in store {$store}: {$value}"
   return $value 
}


function Get-RefreshTokenInStore
{ 
   param(
      [Parameter(Mandatory=$true)]
      [string]$UserID,

      [Parameter(Mandatory=$true)]
      [string[]]$Scope
   )
   $store = (Get-RefreshTokenStorePath -UserID $UserID -Scope $Scope)
   $value = Get-CodeInStore -StorePath $store 

   Write-Verbose "Getting refresh token for scope {$Scope }in store {$store}: {$value}"
   return $value 
}

function Get-AuthenticationCodeStorePath
{
   param(
      [Parameter(Mandatory=$true)]
      [string]$UserID,

      [Parameter(Mandatory=$true)]
      [string[]]$Scope
   )
   return Join-Path (Get-UserScopeStorePath @PSBoundParameters) "AuthenticationCode.txt" 
}

function Get-AuthenticationCodeInStore
{ 
   param(
      [Parameter(Mandatory=$true)]
      [string]$UserID,

      [Parameter(Mandatory=$true)]
      [string[]]$Scope
   )
   $store = Get-AuthenticationCodeStorePath -UserID $UserID -Scope $Scope 
   $value = Get-CodeInStore -StorePath $store

   Write-Verbose "Getting authentication code for scope {$Scope} in store {$store}: {$value}"
   return $value
} 

function Set-AuthenticationCodeInStore
{
   param
   (
      [Parameter(Mandatory=$true)]
      [string]$UserID,

      [Parameter(Mandatory=$true)]
      [string[]]$Scope,

      [string]$Value
   )

   $store = (Get-AuthenticationCodeStorePath -UserID $UserID -Scope $Scope)
   Set-CodeInStore -StorePath $store -Value $Value -Scope $Scope
   Write-Verbose "Setting authentication token for scope {$Scope} in {$store}: {$Value}"
}



Export-ModuleMember -Function Import-GoogleClientDetails, Export-GoogleClientDetails
