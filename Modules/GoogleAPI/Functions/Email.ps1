function Send-Email
{
   param (
      [string]$From,
      [String]$To,
      [string]$Subject,
      [string]$EmailBody,
      [Parameter(Mandatory=$true)]
      [string]$UserID
   )

   Write-Verbose "Running $($MyInvocation.MyCommand)..."
   $scope = "https://mail.google.com/", "https://www.googleapis.com/auth/gmail.modify", "https://www.googleapis.com/auth/gmail.compose", "https://www.googleapis.com/auth/gmail.send"
   $uri = "https://www.googleapis.com/upload/gmail/v1/users/$UserID/messages/send"
   $token = Get-ApplicationToken -Scope $Scope -UserID $UserID
   $body =  @"
Content-Type: text/plain; charset="UTF-8"
MIME-Version: 1.0
Content-Transfer-Encoding: 7bit
to: $To
from: $From
subject: $Subject

$EmailBody
"@

   Write-Verbose "Sending email {$body} for user {$UserID}"
   $result = Invoke-RestMethod -Method Post -Uri $uri -Headers @{"Authorization"="Bearer $token"; "Content-Type" =  'message/rfc822'} -Body $body 
   Write-Verbose $result

   return $result
}

<#
http://stackoverflow.com/questions/24908700/mail-attachment-wrong-media-type-gmail-api
#>
function New-DraftEmail
{
   param (
      [string]$From,
      [String]$To,
      [string]$Subject,
      [string]$EmailBody,
      [Parameter(Mandatory=$true)]
      [string]$UserID
   )

   Write-Verbose "Running $($MyInvocation.MyCommand)..."
   $Scope = "https://www.googleapis.com/auth/gmail.compose", "https://www.googleapis.com/auth/gmail.modify", "https://mail.google.com/"
   $uri = "https://www.googleapis.com/upload/gmail/v1/users/$UserID/drafts"
   $token = Get-ApplicationToken -Scope $Scope -UserID $UserID
   $body =  @"
Content-Type: text/plain; charset="UTF-8"
MIME-Version: 1.0
Content-Transfer-Encoding: 7bit
to: $To
from: $From
subject: $Subject

$EmailBody
"@

   Write-Verbose "Creating draft email {$body} for user {$UserID}"
   $result = Invoke-RestMethod -Method Post -Uri $uri -Headers @{"Authorization"="Bearer $token"; "Content-Type" =  'message/rfc822'} -Body $body 
   Write-Verbose $result

   return $result
}

Export-ModuleMember -Function New-DraftEmail, Send-Email
