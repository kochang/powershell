function Convert-ToBase64String
{
   param (
      [string]$Message
   )
   $bytes = [System.Text.Encoding]::UTF8.GetBytes($Message)
   $result = [System.Convert]::ToBase64String($bytes) 

   return $result

}

function Convert-FromBase64String
{
   param (
      [string]$Base64String
   )

   $bytes = [System.Convert]::FromBase64String($Base64String)
   $result = [System.Text.Encoding]::UTF8.GetString($bytes)

   return $result
}
