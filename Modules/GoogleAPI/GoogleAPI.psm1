param( 
   [Parameter(Position=0, Mandatory=$false)]
   [string]$StorePath 
)
$script:GoogleAPIModuleRoot = Split-Path -Parent $MyInvocation.MyCommand.Path

Get-ChildItem (Join-Path $script:GoogleAPIModuleRoot "Functions") -Recurse -File | Resolve-Path | ForEach-Object { . $_.ProviderPath }

function Set-StorePath
{
   param(
      [Parameter(Mandatory=$true)]
      [ValidateScript({ Test-Path $_ })]
      [string]$Path 
   )
   $script:StorePath = $Path

   Write-Verbose "StorePath: {$StorePath}"
}

if(-not $StorePath -or (-not (Test-Path $StorePath)))
{ 
   Set-StorePath (Join-Path $script:GoogleAPIModuleRoot "StorePath")
}
else
{
   Set-StorePath $StorePath
}

Export-ModuleMember -Function Set-StorePath


<#
#https://marckean.com/2015/09/21/use-powershell-to-make-rest-api-calls-using-json-oauth/
#https://console.developers.google.com/apis/credentials?project=monster-go-cyber
#https://developers.google.com/google-apps/calendar/v3/reference/events/list
#>
