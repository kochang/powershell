$moduleName = "GoogleAPI"
$here = Split-Path -Parent $MyInvocation.MyCommand.Path
$modulePath = (Resolve-Path "$here\..\$moduleName.psd1").ProviderPath
Import-Module $modulePath 

InModuleScope GoogleAPI {

   $script:redirect = �http://kolinchang.com/gauth" 
   $script:clientID = "847922212648-7vi77ajqb6avi4lsioj33amrn4qd6jk0.apps.googleusercontent.com"
   $script:scope = "https://www.googleapis.com/auth/calendar.readonly"
   $script:clientSecret = "NQAuXuJ4lg1baSUVkFbHZbFO"
   $script:userID = 'kochang.chu@gmail.com'

   Describe "$($PSScriptRoot): Get-AuthenticationCode" {
      mkdir "$TestDrive\StorePath" | Out-Null 
      Set-StorePath "$TestDrive\StorePath"

      It "will time out" { 
         $script:navigateCalled = $false
         $script:quitCalled = $false

         Mock New-Object { 
            $obj = New-Object psobject -Property @{
               visible = $false
               LocationURL = "" 
            } 

            Add-Member -InputObject $obj -MemberType ScriptMethod -Name navigate -Value { $script:navigateCalled = $true }
            Add-Member -InputObject $obj -MemberType ScriptMethod -Name Quit -Value { $script:quitCalled = $true } 

            return $obj
         } -ParameterFilter { $ComObject -eq "InternetExplorer.Application" } -Verifiable

         { $result = Get-AuthenticationCode -Scope $script:scope -RedirectUri $script:redirect -ClientID $script:clientID -UserID $script:userID -TimeOutInSeconds 4 } | should throw 'Authentication Code awaiting user interaction timed out'

         Assert-VerifiableMocks
         $script:navigateCalled | should be $true
         $script:quitCalled | should be $true
      }

      It "can parse authentication code" { 
         $script:navigateCalled = $false
         $script:quitCalled = $false

         Mock New-Object { 
            $obj = New-Object psobject -Property @{
               visible = $false
               LocationURL = "$($script:redirect)?code=abc#" 
            } 

            Add-Member -InputObject $obj -MemberType ScriptMethod -Name navigate -Value { $script:navigateCalled = $true }
            Add-Member -InputObject $obj -MemberType ScriptMethod -Name Quit -Value { $script:quitCalled = $true } 

            return $obj
         } -ParameterFilter { $ComObject -eq "InternetExplorer.Application" } -Verifiable

         $result = Get-AuthenticationCode -Scope $script:scope -RedirectUri $script:redirect -ClientID $script:clientID -UserID $script:userID

         Assert-VerifiableMocks
         $result | should be "abc#" 
         $script:navigateCalled | should be $true
         $script:quitCalled | should be $true
      }

      It "can parse error" {
         $script:navigateCalled = $false
         $script:quitCalled = $false

         Mock New-Object { 
            $obj = New-Object psobject -Property @{
               visible = $false
               LocationURL = "$($script:redirect)?error=access_denied#" 
            } 

            Add-Member -InputObject $obj -MemberType ScriptMethod -Name navigate -Value { $script:navigateCalled = $true }
            Add-Member -InputObject $obj -MemberType ScriptMethod -Name Quit -Value { $script:quitCalled = $true }

            return $obj
         } -ParameterFilter { $ComObject -eq "InternetExplorer.Application" } -Verifiable
              
         { Get-AuthenticationCode -Scope $script:scope -RedirectUri $script:redirect -ClientID $script:clientID -UserID $script:userID} | should throw "access_denied#"

         Assert-VerifiableMocks
         $script:navigateCalled | should be $true
         $script:quitCalled | should be $true
      }
   }

   Describe "$($PSScriptRoot): Get-AccessAndRefreshToken" { 
      mkdir "$TestDrive\StorePath" | Out-Null 
      Set-StorePath "$TestDrive\StorePath"

      It "has right call signature" { 
         Mock Invoke-RestMethod { return (New-Object psobject -Property @{
               access_token = 'fakeAccessToken'
               refresh_Token = 'fakeRefreshToken'
            })
         }
         $tokens = Get-AccessAndRefreshToken -AuthenticationCode 'fakeAuthCode' -ClientID $script:clientID -ClientSecret $script:clientSecret -RedirectUri $script:redirect -Scope $script:scope

         Assert-MockCalled Invoke-RestMethod -Exactly -Times 1 -Scope It -ParameterFilter { $Method -eq 'Post' -and $Uri -eq 'https://www.googleapis.com/oauth2/v4/token' -and $Body -eq "code=fakeAuthCode&redirect_uri=$($script:redirect)&client_id=$($script:clientID)&client_secret=$($script:clientSecret)&scope=$script:scope&grant_type=authorization_code" }
      } 
   }

   Describe "$($PSScriptRoot): Revoke-Token"{ 
      mkdir "$TestDrive\StorePath" | Out-Null 
      Set-StorePath "$TestDrive\StorePath"

      It "has right call signature" {
         Mock Invoke-WebRequest { return "" }
         Revoke-Token -Token 'fakeToken'
         Assert-MockCalled Invoke-WebRequest -Exactly -Times 1 -Scope It -ParameterFilter { $Uri -eq "https://accounts.google.com/o/oauth2/revoke?token=fakeToken" -and $ContentType -eq "application/x-www-form-urlencoded" }
      }
   }

   Describe "$($PSScriptRoot): Get-AccessTokens" { 
      mkdir "$TestDrive\StorePath" | Out-Null 
      Set-StorePath "$TestDrive\StorePath"

      It "has right call signature" {
         Mock Invoke-RestMethod { return (New-Object psobject -Property @{
               access_token = 'fakeAccessToken'
            })
         } 
         Get-AccessToken -RefreshToken 'fakeRefreshToken' -ClientID $script:clientID -ClientSecret $script:clientSecret -Scope $script:scope
         Assert-MockCalled Invoke-RestMethod -Exactly -Times 1 -Scope It -ParameterFilter { $Method -eq 'Post' -and $Uri -eq "https://www.googleapis.com/oauth2/v4/token" -and $ContentType -eq "application/x-www-form-urlencoded" -and $Body -eq "refresh_token=fakeRefreshToken&client_id=$($script:clientID)&client_secret=$($script:clientSecret)&scope=$script:scope&grant_type=refresh_token" }
      }
   }

   Describe "$($PSScriptRoot): Get-ApplicationToken" { 
      mkdir "$TestDrive\StorePath" | Out-Null 
      Set-StorePath "$TestDrive\StorePath"

      Mock Get-AuthenticationCode { return 'fakeAuthCode' }
      Mock Get-AccessAndRefreshToken { return (New-Object psobject -Property @{
            refresh_token = 'fakeRefreshToken'
            access_token = 'fakeAccessToken'
         }) }
      Mock Get-AccessToken { return (New-Object psobject -Property @{
            access_token = 'fakeAccessToken'
         }) }

      It "will gain all tokens" {
         $result = Get-ApplicationToken -Scope $script:scope -ClientID $script:clientID -ClientSecret $script:clientSecret -RedirectUri $script:redirect -UserID $script:userID
         $result | should be 'fakeAccessToken'

         Assert-MockCalled Get-AuthenticationCode -Scope It -Exactly -Times 1
         Assert-MockCalled Get-AccessAndRefreshToken -Scope It -Exactly -Times 1
         Assert-MockCalled Get-AccessToken -Scope It -Exactly -Times 0

         $hashScope = $script:scope.GetHashCode() 
         "$TestDrive\StorePath\$script:userID\$hashScope\AuthenticationCode.txt" | should exist
         "$TestDrive\StorePath\$script:userID\$hashScope\RefreshToken.txt" | should exist
      }

      It "will use default configuration" { 
         $result = Get-ApplicationToken -Scope $script:scope -UserID $script:userID
         $result | should be 'fakeAccessToken'
         Assert-MockCalled Get-AuthenticationCode -Scope It -Exactly -Times 0
         Assert-MockCalled Get-AccessAndRefreshToken -Scope It -Exactly -Times 0
         Assert-MockCalled Get-AccessToken -Scope It -Exactly -Times 1 

         $hashScope = $script:scope.GetHashCode() 
         "$TestDrive\StorePath\$script:userID\$hashScope\AuthenticationCode.txt" | should exist
         "$TestDrive\StorePath\$script:userID\$hashScope\RefreshToken.txt" | should exist
      }

      It "will retrieve access token in data store" { 
         $result = Get-ApplicationToken -Scope $script:scope -ClientID $script:clientID -ClientSecret $script:clientSecret -RedirectUri $script:redirect -UserID $script:userID
         $result | should be 'fakeAccessToken'
         Assert-MockCalled Get-AuthenticationCode -Scope It -Exactly -Times 0
         Assert-MockCalled Get-AccessAndRefreshToken -Scope It -Exactly -Times 0
         Assert-MockCalled Get-AccessToken -Scope It -Exactly -Times 1 

         $hashScope = $script:scope.GetHashCode() 
         "$TestDrive\StorePath\$script:userID\$hashScope\AuthenticationCode.txt" | should exist
         "$TestDrive\StorePath\$script:userID\$hashScope\RefreshToken.txt" | should exist
      }

      It "will refresh access token when failed to load from data store" { 
         $hashScope = $script:scope.GetHashCode() 

         $result = Get-ApplicationToken -Scope $script:scope -ClientID $script:clientID -ClientSecret $script:clientSecret -RedirectUri $script:redirect -UserID $script:userID
         $result | should be 'fakeAccessToken'
         Assert-MockCalled Get-AuthenticationCode -Scope It -Exactly -Times 0
         Assert-MockCalled Get-AccessAndRefreshToken -Scope It -Exactly -Times 0
         Assert-MockCalled Get-AccessToken -Scope It -Exactly -Times 1 
         "$TestDrive\StorePath\$script:userID\$hashScope\AuthenticationCode.txt" | should exist
         "$TestDrive\StorePath\$script:userID\$hashScope\RefreshToken.txt" | should exist
      }

      It "wil request refresh and access token when failed to load from data store" { 
         $hashScope = $script:scope.GetHashCode() 
         Remove-Item "$TestDrive\StorePath\$script:userID\$hashScope\RefreshToken.txt"

         $result = Get-ApplicationToken -Scope $script:scope -ClientID $script:clientID -ClientSecret $script:clientSecret -RedirectUri $script:redirect -UserID $script:userID
         $result | should be 'fakeAccessToken'
         Assert-MockCalled Get-AuthenticationCode -Scope It -Exactly -Times 0
         Assert-MockCalled Get-AccessAndRefreshToken -Scope It -Exactly -Times 1
         Assert-MockCalled Get-AccessToken -Scope It -Exactly -Times 0 
         "$TestDrive\StorePath\$script:userID\$hashScope\AuthenticationCode.txt" | should exist
         "$TestDrive\StorePath\$script:userID\$hashScope\RefreshToken.txt" | should exist
      }

      It "wil request refresh and access token when failed to load from data store" { 
         $hashScope = $script:scope.GetHashCode() 
         Remove-Item "$TestDrive\StorePath\$script:userID\$hashScope\RefreshToken.txt"
         Remove-Item "$TestDrive\StorePath\$script:userID\$hashScope\AuthenticationCode.txt"

         $result = Get-ApplicationToken -Scope $script:scope -ClientID $script:clientID -ClientSecret $script:clientSecret -RedirectUri $script:redirect -UserID $script:userID
         $result | should be 'fakeAccessToken'
         Assert-MockCalled Get-AuthenticationCode -Scope It -Exactly -Times 1
         Assert-MockCalled Get-AccessAndRefreshToken -Scope It -Exactly -Times 1
         Assert-MockCalled Get-AccessToken -Scope It -Exactly -Times 0 
         "$TestDrive\StorePath\$script:userID\$hashScope\AuthenticationCode.txt" | should exist
         "$TestDrive\StorePath\$script:userID\$hashScope\RefreshToken.txt" | should exist
      }
   }
}
