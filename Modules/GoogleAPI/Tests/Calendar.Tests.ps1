$moduleName = "GoogleAPI"
$here = Split-Path -Parent $MyInvocation.MyCommand.Path
$modulePath = (Resolve-Path "$here\..\$moduleName.psd1").ProviderPath
Import-Module $modulePath 

InModuleScope GoogleAPI { 
   Set-StorePath (Join-Path $script:GoogleAPIModuleRoot "StorePath")
   Describe "Calendar.ps1: Get-CalendarEventsToday" { 
      It "can get today's events" { 
         $result = Get-CalendarEventsToday -CalendarID "kochang.chu@gmail.com" -UserID 'kochang.chu@gmail.com'
         $result.summary | should be "KO CHANG"

      }
   }

   Describe "Calendar.ps1: Get-CalendarEventsBySearchText" {
      It "cannot have null search string" {
         Get-CalendarEventsBySearchText -CalendarID "kochang.chu@gmail.com" -UserID 'kochang.chu@gmail.com' -SearchText $null | should throw "Cannot bind argument to parameter 'SearchText' because it is an empty string."
      }

      It "cannot have TimeMin greater than TimeMax" {
         Get-CalendarEventsBySearchText -CalendarID "kochang.chu@gmail.com" -UserID 'kochang.chu@gmail.com' -TimeMin ([DateTime]::Today) -TimeMax ([DateTime]::Today.AddDays(-1)) | should throw '$TimeMin cannot be greater than $TimeMax' -SearchText 'x'

      }

      It "should search for text" {
      }

      It "should search with MinTime" {
      }


      It "should search with MaxTime" {
      }
   }
}

