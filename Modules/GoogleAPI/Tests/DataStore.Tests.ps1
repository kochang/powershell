$moduleName = "GoogleAPI"
$here = Split-Path -Parent $MyInvocation.MyCommand.Path
$modulePath = (Resolve-Path "$here\..\$moduleName.psd1").ProviderPath
Import-Module $modulePath 

InModuleScope GoogleAPI {

   $script:redirect = �http://kolinchang.com/gauth" 
   $script:clientID = "847922212648-7vi77ajqb6avi4lsioj33amrn4qd6jk0.apps.googleusercontent.com"
   $script:scope = "https://www.googleapis.com/auth/calendar.readonly"
   $script:clientSecret = "NQAuXuJ4lg1baSUVkFbHZbFO"
   $script:userID = 'kochang.chu@gmail.com'

   Describe "$($PSScriptRoot): Export-GoogleClientDetails and Import-GoogleClientDetails" {
      It "should be able to export and import client details" {
         Export-GoogleClientDetails -ClientID $script:clientID -ClientSecret $script:ClientSecret -RedirectUri $script:redirect -Path "$TestDrive\details.json"

         "$TestDrive\details.json" | should exist

         $result = Import-GoogleClientDetails -Path "$TestDrive\details.json"
         $result.ClientID | should be $script:clientID
         $result.ClientSecret | should be $script:clientSecret
         $result.RedirectUri | should be $script:redirect
      }
   }

   Describe "$($PSScriptRoot): Get-CodeInStore and Set-CodeInStore" {
      It "should set code in store" {
         Set-CodeInStore -Value 'hi' -StorePath "$TestDrive\CodeStore.json" -Scope 'http://scope1', 'http://scope2'
         "$TestDrive\CodeStore.json" | should exist
         $result = Get-Content -Raw "$TestDrive\CodeStore.json" | ConvertFrom-Json 
         $result.Scope | should be 'http://scope1 http://scope2'

         $result = Get-CodeInStore -StorePath "$TestDrive\CodeStore.json" 
         $result | should be 'hi' 
      }

      It "remove code in store when $null" {
         Set-CodeInStore -Value $null -StorePath "$TestDrive\CodeStore.json" -Scope 'http://scope1', 'http://scope2'
         "$TestDrive\CodeStore.json" | should not exist

         $result = Get-CodeInStore -StorePath "$TestDrive\CodeStore.json" 
         $result | should be $null
      }
   }

   Describe "$($PSScriptRoot): Get-ClientID" {
      It "should return client id" {
         Get-ClientID | should be "847922212648-7vi77ajqb6avi4lsioj33amrn4qd6jk0.apps.googleusercontent.com"
      }
   }

   Describe "$($PSScriptRoot): Get-ClientSecret" {
      It "should return client secret" {
         Get-ClientSecret | should be "NQAuXuJ4lg1baSUVkFbHZbFO" 
      }
   }

   Describe "$($PSScriptRoot): Get-RedirectUri" {
      It "should return redirect uri" {
         Get-RedirectUri | should be "http://kolinchang.com/gauth"    
      }
   }


   Describe "$($PSScriptRoot): Get-RefreshTokenInStore" {
      mkdir "$TestDrive\StorePath" | Out-Null 
      Set-StorePath "$TestDrive\StorePath"

      It "can store refresh tokens" {
         Set-RefreshTokenInStore -Value "hi" -UserID $script:userID -Scope $script:scope
         Get-RefreshTokenInStore -UserID $script:userID -Scope $script:scope | should match "hi"
      }
   }

   Describe "$($PSScriptRoot)): Get-AuthenticationCodeInStore" {
      mkdir "$TestDrive\StorePath" | Out-Null 
      Set-StorePath "$TestDrive\StorePath"

      It "can store refresh tokens" {
         Set-AuthenticationCodeInStore -Value "hi" -UserID $script:userID -Scope $script:scope
         Get-AuthenticationCodeInStore -UserID $script:userID -Scope $script:scope | should match "hi"
      }
   } 
}
