$moduleName = "GoogleAPI"
$here = Split-Path -Parent $MyInvocation.MyCommand.Path
$modulePath = (Resolve-Path "$here\..\$moduleName.psd1").ProviderPath
Import-Module $modulePath 

InModuleScope GoogleAPI { 
   Set-StorePath (Join-Path $script:GoogleAPIModuleRoot "StorePath")
   $script:draft; 
   $script:userID = 'kochang.chu@gmail.com'
   Describe "$($PSScriptRoot): New-DraftEmail" {
      It "creates a draft on the server" { 
           $script:draft = New-DraftEmail -EmailBody "good day from test runner! {$($env:ComputerName)} at $([DateTime]::Now)" -From 'kochang.chu@gmail.com' -To 'kochang.chu@gmail.com' -Subject 'New-DraftEmail' -UserID $script:userID

           $script:draft.id | should match '^\w[-]?\d+$' 
      }

      It "sends email" {
           $result = Send-Email -EmailBody "good day from test runner! {$($env:ComputerName)} at $([DateTime]::Now)" -From 'kochang.chu@gmail.com' -To 'kochang.chu@gmail.com' -Subject 'Sending Email' -UserID $script:userID
           $result.id | should match '^\w+$' 
      } 
   }
}
