$moduleName = "GoogleAPI"
$here = Split-Path -Parent $MyInvocation.MyCommand.Path
$modulePath = (Resolve-Path "$here\..\$moduleName.psd1").ProviderPath
Import-Module $modulePath 

InModuleScope GoogleAPI {
   Describe "$($PSScriptRoot): Convert-ToBase64String and Convert-FromBase64String"{
      It "Convert-ToBase64String and Convert-FromBase64String"{
         $message = "blah blah 93u .$%^&*(IU"

         $base = Convert-ToBase64String $message

         $base | should be 'YmxhaCBibGFoIDkzdSAuJCVeJiooSVU='
         Convert-FromBase64String $base | should be $message
      }
   }
}
