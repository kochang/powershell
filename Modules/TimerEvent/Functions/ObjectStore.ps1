function Set-EventTimer
{
   param(
      [Parameter(Mandatory=$true)]
      [ValidateNotNull()]
      [Timers.Timer]$Timer,

      [Parameter(Mandatory=$true)]
      [ValidateNotNull()]
      [string]$ID
   )


   if(-not $script:_TimerStore)
   {
      $script:_TimerStore = @{}
   }

   $script:_TimerStore[$ID] = $Timer
}

function Test-EventTimer
{
   param(
      [Parameter(Mandatory=$true)]
      [ValidateNotNull()]
      [string]$ID
   )

   if($script:_TimerStore)
   {
      return $script:_TimerStore.ContainsKey($ID)
   }
   return $false
}

function Get-EventTimer
{
   param(
      [Parameter(Mandatory=$true)]
      [ValidateNotNull()]
      [string]$ID
   )

   if(Test-EventTimer -ID $ID)
   {
      return $script:_TimerStore[$ID]
   }
   return $null
}

function New-Timer
{
   param( 
      [Parameter(Mandatory=$true)]
      [ValidateNotNull()]
      [double]$IntervalInMilliseconds
   )

   $obj = New-Object Timers.Timer
   $obj.Interval = $IntervalInMilliseconds

   return $obj
}

function Get-TimerIds
{
   if($script:_TimerStore)
   {
      return $script:_TimerStore.Keys
   }
   return $()
}

function Reset-EventTimers
{
   ForEach($t in Get-TimerIds)
   {
      if(-not $t)
      {
         continue
      }
      _Dispose-EventTimer -ID $t
   }
   ForEach($t in Get-TimerIds)
   {
      if(-not $t)
      {
         continue
      }
      Remove-EventTimer -ID $t
   }

   $script:_TimerStore = $null
}

function Remove-EventTimer
{
   param(
      [Parameter(Mandatory=$true)]
      [ValidateNotNull()]
      [string]$ID 
   )

   if($script:_TimerStore)
   {
      if($obj)
      {
         _Dispose-EventTimer -ID $ID
         $script:_TimerStore.Remove($ID) | Out-Null
      }
   }
}

function _Dispose-EventTimer
{
   param(
      [Parameter(Mandatory=$true)]
      [ValidateNotNull()]
      [string]$ID 
   )

   if($script:_TimerStore)
   {
      $obj = Get-EventTimer -ID $ID
      if($obj)
      {
         $obj.Enabled = $false
         $obj.Stop()
         $obj.Dispose()
      }
   }
}

Export-ModuleMember -Function Set-EventTimer, Test-EventTimer, Get-EventTimer
