function New-TimerEvent
{
   param(

      [Parameter(Mandatory=$true)]
      [ValidateNotNull()]
      [double]$IntervalInMilliseconds,

      [Parameter(Mandatory=$true)]
      [ValidateNotNull()]
      [ValidateScript({ (Test-EventTimer -ID $_) -eq $false })]
      [string]$ID,

      [Parameter(Mandatory=$true)]
      [ValidateNotNull()]
      [ScriptBlock]$Action
   )

   $timerObj = New-Timer -IntervalInMilliseconds $IntervalInMilliseconds
   Set-EventTimer -Timer $timerObj -ID $ID
   return Register-ObjectEvent -InputObject $timerObj -EventName Elapsed -SourceIdentifier $ID -Action $Action }


function Start-TimerEvent
{
   param(
      [Parameter(Mandatory=$true)]
      [ValidateNotNull()]
      [string]$ID
   )
   
   if(Test-EventTimer -ID $ID)
   {
      $obj = Get-EventTimer -ID $ID
      if(-not $obj.Enabled)
      {
         $obj.Enabled = $true
         return $true
      }
   }
   return $false 
}

function Stop-TimerEvent
{
   param(
      [Parameter(Mandatory=$true)]
      [ValidateNotNull()]
      [string]$ID
   )

   $obj = Get-EventTimer -ID $ID
   if($obj)
   {
      Remove-EventTimer -ID $ID 
      Unregister-Event -SourceIdentifier $ID
      return $true
   }

   return $false 
}

Export-ModuleMember -Function New-TimerEvent, Start-TimerEvent, Stop-TimerEvent
