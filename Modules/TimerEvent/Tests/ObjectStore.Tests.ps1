$moduleName = "TimerEvent"
$here = Split-Path -Parent $MyInvocation.MyCommand.Path
$modulePath = (Resolve-Path "$here\..\$moduleName.psd1").ProviderPath
Import-Module $modulePath 

InModuleScope TimerEvent {
   Describe "ObjectStore" {
      AfterEach {
         Reset-EventTimers
      }


      It "should be able to get/set/test event timers" {
         Test-EventTimer -ID "Test" | should be $false

         $obj = New-Timer -IntervalInMilliseconds 1000
         $toBeDisposed = New-Timer -IntervalInMilliseconds 1000

         $obj | should not be $null
         $obj.Interval | should be 1000

         Set-EventTimer -ID "Test" -Timer $obj
         Set-EventTimer -ID "ToBeDisposed" -Timer $toBeDisposed

         Test-EventTimer -ID "Test" | should be $true
         Test-EventTimer -ID "SomeOtherTest" | should be $false

         Get-EventTimer -ID "Test" | should be $obj
         Get-EVentTimer -ID "SomeOtherStuff" | should be $null

         Remove-EventTimer -ID "Test" 
         Test-EventTimer -ID "Test" | should be $false

         Reset-EventTimers

         $Script:_TimerStore | should be $null
      }
   }
}
