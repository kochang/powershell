$moduleName = "TimerEvent"
$here = Split-Path -Parent $MyInvocation.MyCommand.Path
$modulePath = (Resolve-Path "$here\..\$moduleName.psd1").ProviderPath
Import-Module $modulePath 

InModuleScope $moduleName {
   Describe "can create a new timer event" {

      AfterEach { 
         Get-EventSubscriber | Unregister-Event
      }

      It "can create a timer event" {
         
         $outputFile = "$TestDrive\timerEvent.test"
         $ID = New-Guid
         $Action = [ScriptBlock]::Create("""$([DateTime]::Now)"" | Out-File ""$outputFile""")
         
         $result = New-TimerEvent -IntervalInMilliseconds 10 -ID $ID -Action $Action

         Start-TimerEvent -ID $ID | should be $true
         Start-TimerEvent -ID "SomeOtherStuff" | should be $false
         Start-Sleep 1

         $outputFile | should exist
         $fileContent = Get-Content "$outputFile" -Head 1 
         ([DateTime]::Now - ([DateTime]$fileContent)).TotalSeconds | should belessthan 2
         $result.State | should be "Running" 
         $result.HasMoreData | should be $true

         Stop-TimerEvent -ID $ID | should be $true
         Stop-TimerEvent -ID "SomeOtherTest" | should be $false
      }
   }
}
