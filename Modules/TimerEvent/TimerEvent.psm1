$script:TimeEventModuleRoot = Split-Path -Parent $MyInvocation.MyCommand.Path
Get-ChildItem (Join-Path $script:TimeEventModuleRoot "Functions") -File -Recurse | Resolve-Path | ForEach-Object { . $_.ProviderPath }

