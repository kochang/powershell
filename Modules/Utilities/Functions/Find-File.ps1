﻿function TraverseTree
{
    param(
        [array]$InputCollection,
        [ScriptBlock]$HasNext,
        [ScriptBlock]$GoNext,
        [ScriptBlock]$ProcessLeaf
    )

    foreach($i in $InputCollection)
    {
        if($HasNext.Invoke($i))
        {
            TraverseTree -InputCollection $GoNext.Invoke($i) -HasNext $HasNext -GoNext $GoNext -ProcessLeaf $ProcessLeaf
        }
        else
        {
            $ProcessLeaf.Invoke($i)
        }        
    }    
}

function ExcludeFileItem
{
    param(
        $Item,
        [string[]] $Exclude
    )

    if($Exclude)
    {
        foreach($e in $Exclude)
        {
            if($Item.Name -like $e)
            {
                return $true
            }
        }
    }
    return $false
}

function TraverseFileStructure
{
    param(
        [string[]]$Path,
        [string[]]$Exclude,
        [ScriptBlock]$Action
    )
    
    foreach($p in (Resolve-Path $Path))
    {    
        TraverseTree -InputCollection (Get-ChildItem $p)`
        -HasNext {
            param ($item)
            
            if($item -is [System.IO.DirectoryInfo] -and -not (ExcludeFileItem -Item $item -Exclude $Exclude))
            {
                return $true
            }
           
            return $false
            
    
        }`
        -GoNext {
            param ($item)
            return Get-ChildItem $item.FullName
        }`
        -ProcessLeaf {
            param ($item)
                if(ExcludeFileItem -Item $item -Exclude $Exclude)
                {
                    return 
                }

                $Action.Invoke($item)
            }
    }
}

function Find-File
{
    param(
        [string[]]$Pattern,
        [string]$Path = (Get-Location),
        [string[]]$Exclude
    )
    TraverseFileStructure $Path -Exclude $Exclude -Action {`
        param($item)

        Select-String -Path $item.FullName -Pattern $Pattern |`
            Select FileName, Path, Line, LineNumber
        #return $item
    }`
        | group Path `
        | Select Count -ExpandProperty Group `
        | ft
}

Export-ModuleMember -Function Find-File
