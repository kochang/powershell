﻿$here = Split-Path -Parent $MyInvocation.MyCommand.Path
$sut = (Split-Path -Leaf $MyInvocation.MyCommand.Path) -replace '\.Tests\.', '.'
#. "$here\$sut"

Import-Module (Resolve-Path "$here/../*.psm1")

Describe $sut {
    Context "give a context"{
            It "finds file by file name" {
                Add-Content "$TestDrive\FindMe.txt" "Hello"

                $result = Find-File -Pattern "FindMe" $TestDrive 
                $result | should be $null
            }
        }
}
