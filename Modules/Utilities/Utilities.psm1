﻿$moduleRoot = Split-Path $MyInvocation.MyCommand.Path

"$moduleRoot\Functions\*.ps1" | Resolve-Path | foreach { . $_.ProviderPath}

$Script:UtilitiesModulePath = $moduleRoot