﻿


$apiKey = "API-YHN6VGADTSAIZOVDM7WLCAHPA"
$server = [uri]"https://octopuppy.dev.serraview.com"
$name = "Hello World"

$Header = @{
    "X-Octopus-ApiKey" = $apiKey
}

# get existing step template
$request = New-Object uri($server, "/api/actiontemplates")
$obj = Invoke-WebRequest -Uri $request -Headers $Header -Method Get -UseBasicParsing | Select-Object -ExpandProperty Content | ConvertFrom-Json

$target = $obj.Items | where { $_.NAme -eq $name} 


$skipProperties = @(
    "Id"
    "Version", 
    @("Parameters", "Id")
    "Links"
)

Enter-Object $target $skipProperties
<#
$target.Description = "REN"


$body = $target | ConvertTo-Json -Depth 10
$request = New-Object uri($server, "/api/actiontemplates/$($target.Id)")
#>
#$x = Invoke-RestMethod -Uri $request -Headers $Header -Method Put -Body ($target | ConvertTo-Json -Depth 10) -UseBasicParsing
<#
{
    "ActionType":  "Octopus.Script",
    "Properties":  {
                       "Octopus.Action.Script.ScriptSource":  "Package",
                       "Octopus.Action.Package.PackageId":  "Serraview.ProdOps.OctopusDeploy",
                       "Octopus.Action.RunOnServer":  "false",
                       "Octopus.Action.Script.ScriptName":  "HelloWorld.ps1",
                       "Octopus.Action.Package.FeedId":  "feeds-111"
                   },
    "Description":  null,
    "Name":  "HelloWorld.ps1",
    "Parameters":  [
                       {
                           "Label":  "YourName",
                           "HelpText":  "Enter your name here",
                           "Name":  "YourName",
                           "DefaultValue":  "Ko",
                           "DisplaySettings":  "@{Octopus.ControlType=SingleLineText}"
                       },
                       {
                           "Label":  "YourStory",
                           "HelpText":  "What\u0027s your story?",
                           "Name":  "YourStory",
                           "DefaultValue":  "Cat ate my mouse",
                           "DisplaySettings":  "@{Octopus.ControlType=MultiLineText}"
                       },
                       {
                           "Label":  "YourTitle",
                           "HelpText":  "Select your title",
                           "Name":  "YourTitle",
                           "DefaultValue":  "Ms",
                           "DisplaySettings":  "@{Octopus.ControlType=Select; Octopus.SelectOptions=Mr|Mr\r\nMrs|Mrs\r\nMs|Ms\r\nMiss|Miss\r\nDr|Doctor}"
                       },
                       {
                           "Label":  "HasPets",
                           "HelpText":  "Do you have pets?",
                           "Name":  "HasPets",
                           "DefaultValue":  "True",
                           "DisplaySettings":  "@{Octopus.ControlType=Checkbox}"
                       },
                       {
                           "Label":  "Secret",
                           "HelpText":  "What\u0027s your secret?",
                           "Name":  "Secret",
                           "DefaultValue":  "coffee",
                           "DisplaySettings":  "@{Octopus.ControlType=Sensitive}"
                       },
                       {
                           "Label":  "YourStep",
                           "HelpText":  "What\u0027s your favorite step template?",
                           "Name":  "YourStep",
                           "DefaultValue":  "XYZ",
                           "DisplaySettings":  "@{Octopus.ControlType=StepName}"
                       }
                   ]
}
#>
<#
{
    "Id":  "ActionTemplates-4",
    "Name":  "Hello World",
    "Description":  "REN",
    "ActionType":  "Octopus.Script",
    "Version":  12,
    "Properties":  {
                       "Octopus.Action.Script.ScriptSource":  "Package",
                       "Octopus.Action.RunOnServer":  "false",
                       "Octopus.Action.Script.ScriptFileName":  "HelloWorld.ps1",
                       "Octopus.Action.Package.FeedId":  "feeds-111",
                       "Octopus.Action.Package.PackageId":  "Serraview.ProdOps.OctopusDeploy"
                   },
    "Parameters":  [
                       {
                           "Id":  "5537a0c2-da5a-4270-9faa-a76121adb3b9",
                           "Name":  "YourName",
                           "Label":  "YourName",
                           "HelpText":  "Type your name here",
                           "DefaultValue":  "Awesome Ko",
                           "DisplaySettings":  "@{Octopus.ControlType=SingleLineText}"
                       },
                       {
                           "Id":  "7dfaa9f7-1b59-4851-8bcb-5488e6e092d0",
                           "Name":  "YourStory",
                           "Label":  "Your Story",
                           "HelpText":  "",
                           "DefaultValue":  null,
                           "DisplaySettings":  "@{Octopus.ControlType=MultiLineText}"
                       },
                       {
                           "Id":  "f962f644-272b-4a45-aa51-e7fff3ddaa40",
                           "Name":  "YourTitle",
                           "Label":  "Your Title",
                           "HelpText":  "",
                           "DefaultValue":  null,
                           "DisplaySettings":  "@{Octopus.ControlType=Select; Octopus.SelectOptions=Mr|Mr\nMrs|Mrs\nMs|Ms\nMiss|Miss\nDr|Doctor}"
                       },
                       {
                           "Id":  "adfda0c4-7175-428c-bd8e-e59f0ecc0228",
                           "Name":  "HasPets",
                           "Label":  "Has Pets",
                           "HelpText":  null,
                           "DefaultValue":  null,
                           "DisplaySettings":  "@{Octopus.ControlType=Checkbox}"
                       },
                       {
                           "Id":  "3619cae1-cc79-40c7-bb78-2493a1a63543",
                           "Name":  "Secret",
                           "Label":  "Secret",
                           "HelpText":  "",
                           "DefaultValue":  null,
                           "DisplaySettings":  "@{Octopus.ControlType=Sensitive}"
                       },
                       {
                           "Id":  "710ead9c-099f-42ce-ac46-5d9a67fa9e14",
                           "Name":  "YourStep",
                           "Label":  "Your Step",
                           "HelpText":  "",
                           "DefaultValue":  null,
                           "DisplaySettings":  "@{Octopus.ControlType=StepName}"
                       }
                   ],
    "Links":  {
                  "Self":  "/api/actiontemplates/ActionTemplates-4",
                  "Usage":  "/api/actiontemplates/ActionTemplates-4/usage"
              }
}
#>