﻿$path = "\\svbuild01\c$\Program Files (x86)\Jenkins\jobs\ProdOps - Dev\1859613777.csv"

$coverage = Get-Content $path -Raw | ConvertFrom-Csv -Delimiter ',' -Header "Title","Code Coverage","Value","Series Label","Build Number","Build Date","URL"


function Convert-TimeStamp
{
    param(
    $timestamp
    )
    
    $seconds = $timestamp / 1000
    $baseTime = ([DateTime]'1/1/1970').AddSeconds($seconds)
    $currentTime = [System.TimeZone]::CurrentTimeZone.ToLocalTime($baseTime)
    return $currentTime
}


$prev = $null
$drop = [Ordered]@{}
foreach($c in $coverage)
{
   
   $value = $c.Title
   $buildID = $c.Value

   if($value -notmatch '^[\d]+')
   {
      continue
   }

   if($null -eq $prev)
   {
      $prev = $value
      continue
   }

   $diff = $value - $prev

   $request = Invoke-WebRequest -Uri "http://svbuild01:8080/view/Prod%20Ops/job/ProdOps%20-%20Dev/$buildID/api/json?pretty&tree=culprits[fullName],timestamp" -UseBasicParsing     
   $obj = $request.Content | ConvertFrom-Json
   $time = Convert-TimeStamp $obj.timestamp
   $culprits = ($obj.culprits | Select-Object -ExpandProperty fullname) -join ', '

   if($diff -lt 0)
   {
      $drop[[Math]::Abs($diff)] = $buildID  
      Write-Output "[-] $([Math]::Abs($diff).ToString("0.000"))% coverage loss for build number {$buildID} on $time"
      Write-Output "Potential culprits: $culprits"
   }
   elseif($diff -eq 0)
   { 
      #Write-Output "[ ] draw for build number {$buildID} on $time"
   }
   else
   {
      #Write-Output "[+] $($diff.ToString("0.000"))% coverage gain for build number {$buildID} on $time"
      #Write-Output "Contributors: $culprits"
   }
   
   $prev = $value 
}
<#
foreach ($d in $drop.Keys)
{
    $buildID = $drop[$d]
    $request = Invoke-WebRequest -Uri "http://svbuild01:8080/view/Prod%20Ops/job/ProdOps%20-%20Dev/$buildID/api/json?pretty&tree=culprits[fullName],timestamp" -UseBasicParsing     
    $obj = $request.Content | ConvertFrom-Json
    $time = Convert-TimeStamp $obj.timestamp
    $culprits = ($obj.culprits | Select-Object -ExpandProperty fullname) -join ', '
    
    Write-Output "Coverage loss at $($d.ToString("0.000"))% for build number {$buildID} on $time"    
    Write-Output "Potential culprits: $culprits"
    Write-Output
}
#>
