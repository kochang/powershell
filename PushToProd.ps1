param (
  [string]$Path, 
  [string]$DestinationBranch,
  [string]$SourceBranch
)

function PushToProd
{
   param (
     [string]$Path = "C:\serraview\bitbucket\DataManagement",
     [string]$DestinationBranch = "prod",
     [string]$SourceBranch = "staging" 
   )

   $me = $Script:MyInvocation.MyCommand.Path
   $myDir = Split-Path $me
   $ShortcutFile = Join-Path $myDir "PushToProd.lnk"
   if(-not (Test-Path $ShortcutFile))
   {
      $shell = New-Object -ComObject WScript.Shell
      $sc = $shell.CreateShortcut($ShortcutFile)      
      $sc.TargetPath = "$PSHOME\powershell.exe" 
      $sc.Arguments = "-NoProfile -NoExit -NoLogo -Command ""& '$me' -Path '$Path'"""
      $sc.Save() 
      Write-Host "You can now double click on {$ShortcutFile} to run this script."
      return
   }
      
   if(-not (Test-Path $Path))
   {
      throw "Invalid Path {$Path}"
   } 

   if(-not (Test-Path "$Path\.git"))
   {
      throw "Path {$Path} is not a git repository"
   } 

   Push-Location $Path
   Write-Host "Working on directory {$Path}"

   $stash = $false 
   if($null -ne (git status --short))
   {
      if((Read-Host "There are local pending changes to commit on branch {$(git rev-parse --abbrev-ref HEAD)}, would you like to continue without them?") -notmatch "y")
      {
         return
      }

      Write-Verbose "Stashing your local changes"
      git stash
      $stash = $true
   } 

   if($(git rev-parse --abbrev-ref HEAD) -ne $SourceBranch)
   {
       Write-Verbose "Checking out branch {$SourceBranch}"
       git checkout $SourceBranch
   }
   git fetch
   git rebase

   if($null -ne (git diff --name-only origin/$SourceBranch))
   {
      if((Read-Host "There are local commits on branch {$(git rev-parse --abbrev-ref HEAD)} that are not pushed to remote server, would you like to continue without them?") -notmatch "y")
      {
         return
      }
   }

   if($(git rev-parse --abbrev-ref HEAD) -ne $DestinationBranch)
   {
       Write-Verbose "Checking out branch {$DestinationBranch}"
       git checkout $DestinationBranch
   }
   git rebase
   git merge origin/$SourceBranch
   git push 

   Write-Verbose "Switching back to branch {$SourceBranch}"
   git checkout $SourceBranch
   if($stash)
   {
      Write-Verbose "Popping your local changes back"
      git stash pop
   }
   Write-Host "Script complete" 
   Pop-Location 
}

PushToProd @PSBoundParameters
