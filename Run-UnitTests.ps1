Import-Module $PSScriptRoot\ThirdPartyModules\Pester\Pester.psm1 
$modulePath = (Resolve-Path "$PSScriptRoot\Modules").ProviderPath
if($ENV:PSModulePath -notmatch [Regex]::Escape($modulePath))
{
   Write-Host "Adding {$modulePath} to `$ENV:PSModulePath"
   $ENV:PSModulePath += ";$modulePath"
   Write-Host "`$ENV:PSModulePath: {$ENV:PSModulePath}"
}

Write-Host "************************************************************************"
Write-Host "* START of Unit Tests                                             *"
Write-Host "************************************************************************"

$output = "$PSScriptRoot\TestResults.xml"
$result = Invoke-Pester "$PSScriptRoot\Modules\*\Tests\*" -CodeCoverage "$PSScriptRoot\Modules\*\Functions\*" -PassThru -OutputFormat NUnitXml -OutputFile $output -ExcludeTag 'Audio','SkipAppVeyor'



$analyzed = $result.CodeCoverage.NumberOfCommandsAnalyzed 
$executed = $result.CodeCoverage.NumberOfCommandsExecuted 

$coverage = 0
if($analyzed -gt 0)
{
  $coverage = $executed / $analyzed
}
$codeCoverageMsg = "Code coverage is $($coverage.ToString(`"0.00%`"))"
$failedMsg = "$($result.FailedCount) tests failed."
Write-Host $codeCoverageMsg

if($env:APPVEYOR_JOB_ID)
{
  (New-Object 'System.Net.WebClient').UploadFile("https://ci.appveyor.com/api/testresults/nunit/$($env:APPVEYOR_JOB_ID)", (Resolve-Path $output))

  Add-AppveyorMessage -Message $codeCoverageMsg
  Add-AppveyorMessage -Message $failedMsg
}
if ($result.FailedCount -gt 0) { 
    throw $failedMsg
} 


Write-Host "************************************************************************"
Write-Host "* END of Unit Tests                                               *"
Write-Host "************************************************************************"

