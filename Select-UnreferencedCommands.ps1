﻿<#

    This script locates function/command call that possibly does not exist.

    Commands listed could also be call referencing modules that is not imported or only available on a server.

#>

function Get-Useage
{
    param(
        [string[]]$Path
    )

    $results = @()
    foreach ($p in $Path)
    {
        $code = Get-Content $p
        if(-not $code)
        {
            Write-Warning "{$p} has no content!"
            continue
        }

        $tokens = [System.Management.Automation.PSParser]::Tokenize($code, [ref]$null) 
        
        foreach($token in $tokens)
        {
            if($token.Type -eq 'Command')
            {
                $results += $token.Content
            }
        }
    }   

    return $results | Group-Object | Select-Object -ExpandProperty Name
}

function Get-Functions
{
    param(
        [string[]]$Path
    )

    $result = @()
    foreach($p in $Path)
    {
        $code = Get-Content $p
        if(-not $code)
        {
            Write-Warning "{$p} has no content!"
            continue
        }
        $tokens = [System.Management.Automation.PSParser]::Tokenize($code, [ref]$null)        
        $keywordMatch = $false
        foreach($token in $tokens)
        {
            if($keywordMatch)
            {
                $keywordMatch = $false
                if($token.Type -eq 'CommandArgument')
                {
                    $result += $token.Content
                }
            }
            $keywordMatch = $token.Type -eq 'Keyword' -and $token.Content -match 'function'
        }
    }

    return $result | Group-Object | Select-Object -ExpandProperty Name
}

function Unreferenced
{
    Get-ChildItem C:\serraview\ProdOps\ServerRoles\ -Recurse -File -Include *.ps1 | Set-Variable files
    $files += Get-ChildItem C:\serraview\ProdOps\Tests\Modules -Recurse -File -Include *.ps1

    $functions = Get-Functions -Path $files

    $usage = Get-Useage -Path $files
    $warnings = @()
    foreach($u in $usage)
    {
        if($functions -notcontains $u)
        {        
            if(($u -match '\\') -or (($u -notmatch '\\') -and (-not (Get-Command -Name $u -ErrorAction Ignore))))
            {            
                $warnings += $u
            }
        }
    }

    $group = $warnings | Group-Object
    foreach($g in $group)
    {        
        Write-Host "{$($g.Name)} is not a valid function call"
    }
}

clear
Unreferenced

<#
function OtherDependencies
{
    $path = ls C:\serraview\ProdOps\ServerRoles -Recurse -File -Include *.ps1 | Where { $_.FullName -notmatch [Regex]::Escape("C:\serraview\ProdOps\ServerRoles\Common\Modules") }
    $modulePaths = ls C:\serraview\ProdOps\ServerRoles\Common\Modules -Directory 

    foreach($m in $modulePaths)
    {
        $commands = Get-Functions (Get-ChildItem "$($m.FullName)\Library" -Recurse -File -Include *.ps1 -Exclude *.Tests.ps1 | Select-Object -ExpandProperty FullName)
        
        foreach($p in $path)
        {
            $selfCommands = Get-Functions -Path $p.FullName
            foreach($sc in $selfCommands)
            {
                $commands = $commands -ne $sc
            }
            $uses = Get-Useage -Path $p.FullName
            foreach($cmd in $commands)
            {
                if($uses -eq $cmd)
                {                    
                    if(-not (Select-String -Path $p.FullName -Pattern "Import-Module.*$($m.Name)"))
                    {
                        Write-Host "{$($m.Name)} <- {$($p.FullName)} base on {$cmd}" -BackgroundColor DarkMagenta
                        Select-String -Path $p.FullName -Pattern "Import-Module"
                        $p.FullName | Set-Clipboard
                        Write-Host
                    }
                    
                }
            }             
        }
    }

}


function ModuleDependencies
{
    $modulePaths = ls C:\serraview\ProdOps\ServerRoles\Common\Modules -Directory 

    foreach($m in $modulePaths)
    {
        $commands = Get-Functions (Get-ChildItem "$($m.FullName)\Library" -Recurse -File -Include *.ps1 -Exclude *.Tests.ps1 | Select-Object -ExpandProperty FullName)
        $otherModules = $modulePaths -ne $m 

        foreach($o in $otherModules)
        {
            $otherCommands = Get-Functions (Get-ChildItem "$($o.FullName)\Library" -Recurse -File -Include *.ps1 -Exclude *.Tests.ps1 | Select-Object -ExpandProperty FullName)
            foreach($oc in $otherCommands)
            {
                $commands = $commands -ne $oc
            }
                            
            $targets = Get-ChildItem $o.FullName -Recurse -File -Include *.ps1
            foreach ($t in $targets)
            {
                $uses = Get-Useage -Path $t.FullName
                foreach($cmd in $commands)
                {
                    if($uses -eq $cmd)
                    {
                        Write-Host "{$($m.Name)} <- {$($o.Name)} base on {$cmd} useage in {$($t.FullName)}"
                    }
                }
            }            
        
        
        }
    }
}

#>