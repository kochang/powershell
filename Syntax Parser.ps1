﻿Import-Module D:\OneDrive\Code\Powershell\Modules\CKLProfile\CKLProfile.psd1


$hash = @{}

function Get-TokenTypeHash
{
    param(
        [System.Collections.ObjectModel.Collection``1[System.Management.Automation.PSToken]]$InputObject
    )

    $hash = @{}
    foreach($o in $InputObject)
    {
        if(-not $hash.ContainsKey($o.Type))
        {
            $hash[$o.Type] = @()
        }

        $hash[$o.Type] += $o.Content
    }
    
    return $hash
}

function Get-Commands
{
    param(
        [System.Collections.ObjectModel.Collection``1[System.Management.Automation.PSToken]]$InputObject
    )

    $results = @()
    foreach($o in $InputObject)
    {
        if($o.Type -eq "Command")
        {
            $results += $o.Content
        }
    }

    return $results
}

function Get-Functions
{
    param(
        [System.Collections.ObjectModel.Collection``1[System.Management.Automation.PSToken]]$InputObject
    )

    $results = @()
    foreach($o in $InputObject)
    {
        if($o.Type -eq "Keyword" -and $o.Content -eq 'function')
        {
            $results += $o
        }
    }

    return $results
}
foreach ($file in (ls $ProdOpsPath -Include *.ps1 -Exclude *.Tests.ps1 -Recurse))
{
    $code = Get-Content $file    
    $errors = $null

    if(-not $code)
    {
        Write-Warning $file.FullName
        continue
    }
    $parse = [System.Management.Automation.PSParser]::Tokenize($code, [ref]$errors)
    
    $commands = (Get-TokenTypeHash -InputObject $parse)["Command"]
    if($commands)
    {
        $hash[$file.FullName] = @{}
        
        foreach($c in $commands)
        {
            if(-not $hash[$file.FullName].ContainsKey($c))
            {
                $hash[$file.FullName][$c] = 1
            }
            else
            {
                ++$hash[$file.FullName][$c]
            }
        }           
    }

}

foreach ($k in $hash.Keys)
{
    
    Write-Host "File: {$k}" -BackgroundColor Red
    foreach ($cmd in $hash[$k].Keys)
    {
        Write-Host "Cmd: {$cmd}" -BackgroundColor Black
        if($a = Get-Alias -Name $cmd -ErrorAction Ignore)
        {
            $a.Name
            $a.Definition
            $p.Start
            $p.Length
        }
    }
}

