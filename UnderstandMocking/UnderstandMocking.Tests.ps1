﻿$here = Split-Path -Parent $MyInvocation.MyCommand.Path
$sut = (Split-Path -Leaf $MyInvocation.MyCommand.Path).Replace(".Tests.", ".")
. "$here\$sut"

Describe "UnderstandMocking" {

    Mock UnderstandMocking { return "Mocking within describe"} -ParameterFilter { $p -eq ""}
    Mock UnderstandMocking { return "Mocking within describe 2"} -ParameterFilter { $p -eq "describe"}


    
    It "calling function as it is" {
        UnderstandMocking "as it is" | should be "as it is"
    }

    It "calling mocking from describe"{
        UnderstandMocking "" | should be "Mocking within describe"
        UnderstandMocking "describe" | should be "Mocking within describe 2"
    }

     It "calling mocking from describe"{
        
        Mock UnderstandMocking { return "Mocking within it"} -ParameterFilter { $p -eq ""}        
        UnderstandMocking "" | should be "Mocking within it"

        Mock UnderstandMocking { return "Mocking unconditionally"}

        UnderstandMocking "abc" | should be "Mocking unconditionally"
    }

    It "another mock"{
       UnderstandMocking "abc" | should be "Mocking unconditionally"
    }
}
