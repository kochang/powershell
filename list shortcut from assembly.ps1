﻿function Get-ResourceSet
{
    param
    (
        [object]$obj
    )

    $assembly = $obj.GetType().Assembly
    $resourceNames = $assembly.GetManifestResourceNames()

    foreach($r in $resourceNames)
    {        
        if($r -match '^(?<name>[\w\W]+)\.resources$')
        {
            $rm = New-Object System.Resources.ResourceManager $Matches.name,$assembly
            $rs = $rm.GetResourceSet((Get-Culture), $true, $true) 
            $rs.Where({ $_.Name -match 'Shortcut\d?$|^F\d+Keyboard' }) | Select-Object -Property Name,Value | Sort-Object Value #| ConvertTo-Html > C:\Users\ko-lin.chang\Desktop\output.htm
        }

    }


}

Get-ResourceSet $psISE